<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

$this->setTitle('Producteurs') ;
$this->setMeta('description', 'Retrouvez les producteurs présents sur Opendistrib.') ;

?>

<h1 class="title-system-order"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h1>

<?= 

GridView::widget([
    'dataProvider' => $dataProviderProducers,
    'summary' => '',
    'columns' => [
        [
            'attribute' => 'name',
        ],
        [
            'attribute' => 'type',
        ],
        [
            'label' => 'Localisation',
            'value' => function($model) {
                return $model->city. ' ('.$model->postcode.')' ;
            }
        ],
        [
            'label' => 'Lien',
            'format' => 'raw',
            'value' => function($model) {
                $iconLock = (strlen($model->code)) ? ' <span class="glyphicon glyphicon-lock"></span>' : '' ;
                $html = Html::a('Visiter',Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index', 'slug_producer' => $model->slug]), ['class'=>'btn btn-primary']) ;
                $html .= $iconLock ;
                return $html ;
            }
        ],
    ]
]);
                    
?>