<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

$this->setTitle('Mentions légales');
$this->setMeta('description', 'Prenez connaissance de nos mentions légales.') ;


?>

<div class="container content-text">
    <div class="content">
        <h1>Mentions légales</h1>

        <h2>Éditeur</h2>
        <p><strong>distrib</strong><br />
        42 Grande Rue<br />
        25320 Vorges-les-Pins<br />
        Responsable de publication : Guillaume BOURGEOIS</p>
        
        <p>Société hébergée par Cap'Entreprendre située 12 PLACE ROBERT SCHUMAN 57600 FORBACH<br />
        Siret : 47784404700019.</p>
        
        <h2>Développement</h2>
        <p>Guillaume Bourgeois<br />
            <a href="http://www.guillaume-bourgeois.fr/">www.guillaume-bourgeois.fr</a><br />
            42 Grande Rue, 25320 Vorges-les-Pins
        </p>
        
        <h2>Graphisme</h2>
        <p>Sébastien Bourgeois<br />
            25290 Ornans
        </p>

        <h2>Hébergement</h2>
        <p>La société <strong>Alwaysdata</strong>, SARL au capital de 5.000 € immatriculée au RCS de 
            Paris sous le numéro 492 893 490 dont le siège social se trouve 62 rue Tiquetonne – 75002 Paris.</p>

        <h2>Propriété intellectuelle et Copyright</h2>
        <p>Le site est protégé par les lois en vigueur sur la propriété intellectuelle et le droit d’auteur au niveau national et international.<br />
            Toutes les informations disponibles sur le Site sont protégées par un copyright et sont propriété de <strong>distrib</strong>, sous réserve de droits appartenant à des tiers. Les présentes informations ne devront pas être interprétées comme constituant une licence ou un droit d’utilisation portant sur toute image, marque déposée, marque de service ou logo de <strong>distrib</strong>. Le téléchargement ou la copie de tout matériel à partir du Site ne vous confère aucun droit sur les éléments téléchargés ou copiés. <strong>distrib</strong> réserve tous ses droits sur le copyright et la propriété de toute information disponible sur le Site et les fera valoir dans toute l’extension de la loi applicable.</p>

        <h2>Limitation de la responsabilité</h2>
        <p>Les informations publiées sur ce site sont publiées à titre d’informations générales uniquement. <strong>distrib</strong> est soucieux de diffuser des informations complètes et exactes mais ne peut garantir l’exactitude, la précision ou l’exhaustivité des informations mises à disposition sur le Site et ne saurait en aucun cas être tenu responsable en cas d’erreur ou d’inexactitudes contenues sur son Site et ne saurait garantir que l’utilisation de son Site en porte pas atteinte aux droits de tiers. Toute utilisation du Site est faite aux risques et périls de l’utilisateur.<br />
            Dans les limites autorisées par la loi, notamment en cas de négligence, <strong>distrib</strong> ne pourra être tenu responsable de pertes ou de dommages de quelque nature que ce soit, qu’il s’agisse, notamment mais non exclusivement, de dommages directs, indirects ou consécutifs, liés à l’accès au Site ou à tout autre site ayant un lien avec le Site, à son utilisation, à son exploration ou à son téléchargement.</p>

        <h2>Lien hypertextes</h2>		
        <p>La mise en place d’un lien hypertexte vers le site http://www.opendistrib.net nécessite une autorisation préalable écrite de <strong>distrib</strong>. Si vous souhaitez mettre en place un lien hypertexte vers ce site, vous devez en conséquence prendre contact avec le responsable du site. <strong>distrib</strong> ne peut en aucun cas être tenu pour responsable de la mise à disposition des sites qui font l’objet d’un lien hypertexte à partir du site http://www.opendistrib.net et ne peut supporter aucune responsabilité sur le contenu, les produits, les services, etc. disponibles sur ces sites ou à partir de ces sites.</p>

        <h2>Informatique et libertés – Données personnelles</h2>
        <p>La collecte et le traitement des données personnelles de l'Internaute par <strong>distrib</strong> ont fait l'objet d'une déclaration auprès de la CNIL (numéro 2018856).<br />
        La finalité du traitement des données personnelles fournies volontairement par l'Internaute sur le Site au travers du module « Contact », est de permettre à l'Internaute d'être contacté rapidement par <strong>distrib</strong>.<br />
        L'Internaute est informé que les données de connexion ou relatives à la navigation de l'Internaute depuis le Site sont collectées (informations de navigation) de manière automatique par un logiciel de statistiques et de mesure d'audience. Ces informations ne sont collectées qu'à des fins d'analyse statistique et d'optimisation interne du Site.<br />
        L'Internaute est également informé qu'en application des articles 39 et 40 de la loi dite « Informatique et Libertés » du 6 janvier 1978, modifiée par la loi 2004-801 du 6 août 2004, l'Internaute dispose d'un droit d'accès, de rectification, de modification et de suppression concernant ses données personnelles.</p>
    </div>
</div>