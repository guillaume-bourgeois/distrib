<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->setTitle('Inscription') ;
$this->setMeta('description', 'Inscrivez-vous afin de profiter des fonctionnalités de la plateforme.');

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1 class="title-system-order"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h1>
    
    <div class="row">
        <div class="col-lg-5">
            <?php if(YII_ENV == 'demo'): ?>
                <div class="alert alert-warning">
                    Les inscriptions sont désactivées sur l'espace de démonstration. Utilisez 
                    les identifiants indiqués sur la page de <?= Html::a('connexion', Yii::$app->urlManager->createUrl(['site/login'])); ?> pour vous identifier.
                </div>
            <?php else: ?>
                <?php $form = ActiveForm::begin(['id' => 'form-signup','enableClientValidation'=> false]); ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'lastname') ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'phone') ?>                    

                    <div id="user-producer" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default <?php if($model->option_user_producer == 'user' || !$model->option_user_producer): ?>active<?php endif; ?>">
                          <input type="radio" name="SignupForm[option_user_producer]" value="user" id="option-user" autocomplete="off" <?php if($model->option_user_producer == 'user' || !$model->option_user_producer): ?>checked<?php endif; ?>> Je suis client
                        </label>
                        <label class="btn btn-default <?php if($model->option_user_producer == 'producer'): ?>active<?php endif; ?>">
                          <input type="radio" name="SignupForm[option_user_producer]" value="producer" id="option-producer" autocomplete="off" <?php if($model->option_user_producer == 'producer'): ?>checked<?php endif; ?>> Je suis producteur
                        </label>
                    </div>

                    <div id="fields-producer">
                        <?= $form->field($model, 'name_producer') ?>
                        <?= $form->field($model, 'type')->textInput(['placeholder' => 'Boulangerie, brasserie, ferme ...']); ?>
                        <?= $form->field($model, 'postcode') ?>
                        <?= $form->field($model, 'city') ?>
                        <?= $form->field($model, 'id_tax_rate_default')
                                ->dropDownList(ArrayHelper::map(TaxRate::find()->all(), 'id', function($model) { return $model->name; }), [
                                    'prompt' => '--',
                                ])
                                ->label('TVA à appliquer par défaut'); ?>
                        <?= $form->field($model, 'free_price',[
                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span></div>',
                                ])
                                ->label('Prix libre HT / mois')
                                ->hint('Laissez ce champs vide si vous souhaitez dans un premier temps simplement tester la plateforme. Une fois l\'outil mis en place dans votre structure, vous pourrez modifier ce coût dans la section "Mon abonnement" de votre panneau d\'administration.') ;  ?>
                        <?= $form->field($model, 'cgv')->checkbox()->label('J\'accepte les <a class="btn btn-xs btn-default" data-toggle="modal" data-target="#modal-cgv" href="javascript:void(0);">conditions générales de service</a> et les <a class="btn btn-xs btn-default" data-toggle="modal" data-target="#modal-prices" href="javascript:void(0);">conditions tarifaires</a> (prix libre).') ?>
                    </div>
                    <div id="fields-user">
                        <?= $form->field($model, 'id_producer')->dropDownList($dataProducers, ['prompt' => '--','encode' => false,'options' => $optionsProducers]) ?>
                        <div id="bloc-code-acces">
                        <?= $form->field($model, 'code',[
                                    'inputTemplate' => '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>{input}</div>',

                                ])
                                ->label('Code')
                                ->hint('Renseignez-vous auprès de votre producteur pour qu\'il vous fournisse le code d\'accès') ;  ?>
                        </div>
                    </div>

                    <div class="form-group" id="buttons-signup">
                        <?= Html::submitButton("S'inscrire", ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            <?php endif; ?>
            
            
        </div>
    </div>
</div>

<!-- CGV -->
<div class="modal fade" id="modal-cgv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Conditions générales de service</h4>
      </div>
      <div class="modal-body">
        <?= $this->render('_cgv_content.php'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- Tarifs -->
<?= $this->render('../site/_prices_producer'); ?>