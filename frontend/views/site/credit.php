<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

$this->setTitle('Crédit') ;
$this->setMeta('description','Présentation de la fonctionnalité "Crédit" de la plateforme.') ;

?>

<div class="container content-text">
    <div class="content">
        <h1>Le Crédit</h1>
        <p>Le <em>Crédit</em> est un compte prépayé qui vous permet de régler
            vos commandes sur le site <strong>distrib</strong>. Ainsi vous
            n'avez pas à payer chez votre producteur à chaque fois que vous y 
            récupérez une commande.</p>
        <p>Pour créditer votre compte, rien de plus simple. Il vous suffit de vous
        rendre chez votre producteur et de régler la somme que vous souhaitez créditer.
        Le producteur se chargera de mettre à jour votre <em>Crédit</em>
        en ligne.</p>
        <p>Lors de vos prochaines commandes, vous aurez juste à indiquer que vous souhaitez
            payer avec votre <em>Crédit</em> et votre compte sera automatiquement débité.</p>
    </div>
</div>