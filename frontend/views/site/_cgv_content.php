<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

?>

<p>Les présentes Conditions Générales de Service ( ci-après les « <strong>Conditions</strong> ») 
    font partie intégrante du contrat conclu (le « <strong>Contrat</strong> ») entre <strong>Guillaume BOURGEOIS - Cap'Entreprendre</strong> inscrit en tant que <strong>SCOP SARL</strong> au Registre du Commerce et des Sociétés 
    de <strong>Sarreguemines</strong>, sous le numéro <strong>47784404700019</strong>, dont le siège social est situé au 
    <strong>12 PLACE ROBERT SCHUMAN 57600 FORBACH</strong> (ci-après la « <strong>Société</strong> ») et le producteur 
    souhaitant être référencé sur le site internet de la Société (le « <strong>Producteur</strong> »).</p>

<h2>1. Objet</h2>
<p>La Société édite le site internet <a href="http://www.opendistrib.net">http://www.opendistrib.net</a>   
    (le « <strong>Site</strong> »)  permettant la mise en relation des utilisateurs 
    du Site avec les Producteurs inscrits sur le Site afin de faciliter leur commande 
    auprès de leur producteur. Les présentes Conditions ont pour objet de définir 
    les droits et obligations du Producteur dans le cadre de son référencement sur le Site. </p>

<h2>2. Mise en relation</h2>
<p>Le rôle de la Société dans le cadre de la mise en relation du Producteur avec 
    les utilisateurs du Site est limité à un simple rôle d’intermédiaire technique. 
    Tout Producteur mis en relation avec un utilisateur du Site exerce en tant que 
    professionnel indépendant et demeure entièrement responsable des produits vendus 
    aux utilisateurs du Site notamment au titre des conditions d'exécution du travail 
    de tout personnel fabriquant les produits vendus, et également au regard du respect 
    de la durée du travail, de l'hygiène et de la sécurité, au sein des locaux du Producteur.</p>

<h2>3. Accès au Site</h2>
<p>Au titre du Contrat, la Société concède un droit d’utilisation personnel, temporaire 
    et non cessible d’accès à la partie réservée aux Producteurs de son Site selon 
    les termes des présentes Conditions et sous réserve du paiement des redevances 
    d’accès au Site prévues à l’article 8 des présentes Conditions. Avant d’utiliser 
    le Site, le Producteur sera invité à créer un compte et choisir un identifiant et 
    un mot de passe. Le Producteur est seul responsable de l’utilisation et de la 
    gestion de son droit d’accès au Site et du maintien de la confidentialité et 
    sécurité de ses identifiants. Le Producteur devra informer immédiatement la Société 
    de toute utilisation non autorisée de ses identifiants. La Société ne pourra être 
    tenue pour responsable de toute perte ou dommage résultant de l'incapacité du Producteur 
    à protéger ses identifiants. </p>

<h2>4. Utilisation du Site</h2>
<p>Une fois son compte créé sur le Site, le Producteur s’engage à fournir des informations 
    sincères et exactes sur notamment ses points de vente, ses produits disponibles 
    à la vente et les tarifs proposés à la clientèle et à tenir à jour ces informations. 
    La Société se réserve le droit de résilier le Contrat et suspendre l’accès au Site sans 
    préavis en cas de fausses informations fournies par le Producteur.</p>

<h2>5. Commande de produits</h2>
<p>Toute nouvelle commande  d’un utilisateur apparaitra sur le compte du Producteur 
    sur le Site. La commande comporte systématiquement la date pour laquelle les 
    produits sont demandés par l’utilisateur. Les nouvelles commandes seront communiquées 
    au Producteur de manière quotidienne par mail. </p>

<h2>6. Annulations</h2>
<p>Toute commande devra être honorée par le Producteur. Le Producteur dispose toutefois 
    de la faculté de refuser ou d’annuler une nouvelle commande. Si le nombre de commandes 
    refusées par le Producteur est considéré comme trop important, notamment en cas 
    de réclamations des utilisateurs, la Société se réserve le droit de mettre fin 
    au Contrat et suspendre l’accès du Producteur au Site sans préavis ni indemnité.</p>

<h2>7. Paiement des commandes</h2>
<p>Le Site permet, si le Producteur active cette option, de réaliser des transactions 
    monétaires entre les utilisateurs et les Producteurs. Cette transaction vient 
    alimenter un crédit disponible sur le Site qui sera ensuite débité à chaque
    commande.<br />
    Pour toutes les commandes non payées via le système de crédit, elles seront à 
    régler directement auprès du Producteur lors de la réception des produits commandés 
    par l’Utilisateur. 
</p>

<h2>8. Redevances d'utilisation du Site</h2>
<p>La création d’un compte et l’utilisation de la partie du Site réservée aux Producteurs 
    implique le paiement mensuel par les Producteurs d'un montant (HT) défini par eux-même dans
    la section <em>Mon abonnement</em>. Si ce montant est supérieur à 0€, la Société émettra à l’issue 
    de chaque mois une facture au Producteur, payable 
    par virement bancaire ou par chèque dans les trente (30) jours à compter 
    de la date d’émission de la facture. Tout défaut ou retard de paiement emportera 
    immédiatement application de pénalités de retard à un taux qui est de trois fois 
    le taux d’intérêt légal appliqué par la Banque Centrale Européenne (B.C.E) à 
    son opération de refinancement la plus récente, majoré de dix (10) points de pourcentage 
    et paiement d’une indemnité forfaitaire pour frais de recouvrement d’un montant 
    de quarante (40) euros en application de l’article D441-5 du Code de Commerce. 
    La Société se réserve également le droit de résilier le Contrat et suspendre l’accès 
    au Site sans préavis en cas de retard ou défaut de paiement du Producteur.</p>

<h2>9. Durée</h2>
<p>Le présent Contrat est conclu pour une durée indéterminée. Le Contrat est résiliable 
    par l’une ou l’autre des parties, sans indemnité, par notification écrite adressée 
    par email à l’autre partie. En cas de résiliation du Contrat, le Producteur devra 
    s’acquitter des redevances dues et non réglées à la date de résiliation. </p>

<h2>10. Responsabilité / Garantie</h2>
<p>La Société n’est en aucun cas responsable si, en cas d’indisponibilité liée à 
    des opérations de maintenance, ou à la défaillance du réseau Internet ou pour 
    toute autre raison qui lui sont indépendantes, le Producteur ne peut accéder au Site. 
    Le Producteur reconnaît que pour l’ensemble des contenus qu’il fournit au moyen du Site, 
    le rôle de la Société se limite à celui d’un simple hébergeur. En outre, en sa 
    qualité de simple intermédiaire, la responsabilité de la Société ne peut être 
    engagée pour des actes découlant de sa prestation de mise en relation. Enfin, 
    dans le cadre de la simple fourniture d’un accès au Site, la Société agit en simple 
    qualité de prestataire technique et ne pourra en aucun cas voir sa responsabilité 
    engagée pour les actes d’usage ou d’exploitation du Site par le Producteur ou réalisé 
    à partir des identifiants du Producteur. Le Producteur s’engage à utiliser le Site 
    dans le respect de la législation française en vigueur, notamment en matière de 
    droit de la consommation (prohibition de la publicité trompeuse et des pratiques 
    commerciales déloyales ou agressives), droit des données personnelles, droit de
    la propriété intellectuelle, droit à l’image et droit du commerce électronique. 
    En conséquence, le Producteur s’engage à garantir la Société, contre toute réclamation, 
    action ou demande d’indemnisation d’un utilisateur du Site, d’un tiers ou d’une 
    autorité publique portant sur une information, un contenu ou un acte du Producteur 
    pris en violation des stipulations du présent Contrat ou des dispositions législatives 
    en vigueur. Conformément à l’article L.111-7 du Code de la Consommation, le Producteur 
    reconnaît que la Société lui a mis à disposition un espace lui permettant de communiquer 
    aux consommateurs les obligations légales d’information précontractuelle concernant 
    les produits qu’il propose à la vente sur le Site et ne pourra donc être reconnue 
    responsable en cas de de défaut de communication du Producteur desdites informations.</p>

<h2>11. Propriété intelectuelle</h2>
<p>Le contenu fourni par le Producteur ou créé par ce dernier demeure sa propriété et 
    reste sous sa responsabilité exclusive. Le Producteur autorise toutefois la Société 
    à utiliser les différents éléments visuels ou textuels fournis au moyen du Site 
    sur ses différents supports de communication tels que les newsletters envoyés 
    aux utilisateurs du Site. Le Producteur reconnaît que les contenus qu’il édite 
    et qui seront accessibles via le Site ne porteront pas atteinte aux droits des 
    tiers, et qu’il est autorisé à les reproduire et/ou les diffuser au public sur 
    Internet. Le Producteur garantit à ce titre la Société contre toutes réclamations, 
    revendications de propriété et actions judiciaires ou extrajudiciaires relatives 
    aux contenus susvisés et s’engage à dédommager intégralement la Société de frais 
    et pertes qui pourraient causés à la Société de ce fait. La Société demeure exclusivement 
    propriétaire de l’ensemble des marques, photographies et logos et de tout autre 
    matériel reproduit sur le Site lui appartenant et ne pouvant  être utilisé par le 
    Producteur sans le consentement préalable et écrit de la Société.</p>

<h2>12. Données personnelles</h2>
<p> La Société est responsable de traitement de l’ensemble des données personnelles 
    des utilisateurs du Site. A ce titre, le Producteur reconnaît qu’aucune donnée 
    personnelle ne lui est transférée par le Présent Contrat, les données des utilisateurs 
    n’étant accessibles que via le Site. Le Producteur s’interdit tout acte d’extraction 
    ou reproduction des données personnelles collectées sur le Site. En cas de collecte 
    de données des utilisateurs par le Producteur suite à une mise en relation, ce dernier 
    s’engage à respecter les droits reconnus aux utilisateurs par la loi informatiques 
    et libertés et garantir la Société contre toute réclamation d’un utilisateur résultant 
    d’une violation par le Producteur des dispositions législatives en vigueur en matière 
    de protection des données à caractère personnel. </p>

<h2>13. Force majeure</h2>
<p>Aucune des parties ne pourra être tenue responsable de l'inexécution de l'une de 
    ses obligations contractuelles (à l’exception des obligations de paiement) du 
    fait de la survenance d'un cas de force majeure. Sont considérées comme des cas 
    de force majeure -outre ceux habituellement retenus par la jurisprudence des cours 
    et tribunaux français- les grèves, les ruptures d’approvisionnement, les incendies, 
    les tremblements de terre, les inondations, les guerres, les actes de terrorisme 
    ou tout autre élément imprévisible, irrésistible et externe. La partie qui souhaite
    invoquer un cas de force majeure notifiera immédiatement l’autre partie de la 
    survenance d’un tel cas, en précisant la nature de l’événement, son effet, ainsi 
    que sa durée prévisible. Il est expressément convenu que cette partie fera ses 
    meilleurs efforts pour surmonter l’effet du cas de force majeure, ainsi que sa 
    durée.</p>

<h2>14. Stipulations générales</h2>

<h3>14.1 Cession</h3>
<p>Le Contrat est conclu en considération de la personne du Producteur et ne peut 
    donc faire l’objet d’une cession, à titre gratuit ou onéreux, au profit d’un 
    tiers sans l’accord préalable et écrit de La Société. Toute cession ou transfert 
    réalisé en violation des stipulations susvisées sera nulle. </p>

<h3>14.2 Notifications</h3>
<p>Toute notification ou communication au titre du Contrat devra être établie par 
    écrit et adressée par courrier, par télécopie, par e-mail ou par lettre. Toute 
    notification ou communication sera réputée avoir été valablement délivrée à 
    compter de sa réception ou après un délai de trois jours suivant son envoi, 
    selon l’événement qui se produit en premier. Tout changement de domiciliation 
    de l’une des parties devra faire l’objet d’une notification écrite à l’autre 
    partie.</p>

<h3>14.3 Non renonciation</h3>
<p>Le fait pour l’une ou l’autre des parties de ne pas se prévaloir d’un manquement 
    de l’autre partie à l’une quelconque de ses obligations au titre du Contrat ne 
    saurait être interprété comme une renonciation à l’obligation en cause.</p>

<h3>14.4 Non validité partielle</h3>
<p>Si une ou plusieurs stipulations du Contrat sont tenues pour illégales ou inapplicables 
    ou considérées comme telles en application d’une loi, d’un règlement ou d’une 
    décision de justice devenue définitive, elles seront réputées non écrites et les 
    autres stipulations demeureront en vigueur.</p>

<h3>14.5 Titres</h3>
<p>Les titres des articles du Contrat n’ont qu’une valeur indicative et ne permettent 
    pas d’interpréter la teneur des stipulations y afférentes au titre du Contrat. </p>

<h3>14.6 Indépendance des parties</h3>
<p>Chacune des parties conclut le Contrat en tant qu’entrepreneur indépendant et 
    non en tant qu’agent ou Producteur de l’autre partie. Aucune des parties ne pourra 
    prétendre vis-à-vis des tiers qu’elle a le pouvoir de représenter l’autre partie.</p>

<h3>14.7 Droit applicable et attribution de compétence</h3>
<p>Le contrat est soumis à la loi française. En cas de litige entre les Parties 
    découlant de la validité, l’interprétation ou l’exécution du Contrat et à défaut 
    d’accord amiable entre les parties ci-avant, compétence exclusive est attribuée 
    aux tribunaux de Paris, nonobstant pluralité de défendeurs ou appels en garanties,
    même pour les procédures d’urgence ou les procédures conservatoires.  </p>


