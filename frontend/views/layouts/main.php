<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\helpers\Url ;

/* @var $this \yii\web\View */
/* @var $content string */

$isHome = (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ;

\common\assets\CommonAsset::register($this);
\frontend\assets\AppAsset::register($this);

$producer = null ;
if(!Yii::$app->user->isGuest && Yii::$app->user->identity->id_producer > 0) {
    $producer = Producer::searchOne(['id' => Yii::$app->user->identity->id_producer]) ;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
    <title><?php if($isHome): ?>Opendistrib | <?= Html::encode($this->title) ?> <?php else: ?><?= Html::encode($this->title) ?> | Opendistrib<?php endif; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="icon" type="image/png" href="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/favicon-distrib.png" />
    <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/favicon.ico" /><![endif]-->
    <?php $this->head() ?>
    <!--[if lt IE 9]>
    <script src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/js/html5shiv.min.js"></script>
    <link href="<?= Yii::$app->urlManager->getBaseUrl(); ?>/css/ie.css" rel="stylesheet">
    <![endif]-->
    <!--[if IE 7]>
            <link href="<?= Yii::$app->urlManager->getBaseUrl(); ?>/css/ie7.css" rel="stylesheet">
    <![endif]-->
</head>
<body class="<?php if($isHome): echo 'home' ; endif; ?><?php if(!Yii::$app->user->isGuest): ?> connected<?php endif; ?>">
	<?php $this->beginBody() ?>
		<div id="back"></div>  
                <?php if(YII_ENV == 'demo'): ?>
                    <div id="block-demo">
                        <div class="container">
                            <span class="glyphicon glyphicon-eye-open"></span> <strong>Espace de démonstration</strong>  :  
                            Testez la plateforme sans avoir à vous inscrire. Les données sont réinitialisées quotidiennement &bull; <?= Html::a('Retour', Url::env('prod', 'frontend')) ?>
                        </div>
                    </div>
                <?php endif; ?>
		<header id="header">
                    <nav class="navbar navbar-default">
                        <div id="the-header" class="container">
                            <div class="navbar-header">
                                <a id="link-home" href="<?= Yii::$app->urlManager->createUrl('site/index') ; ?>">
                                    <img src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-distrib.png" alt="" />
                                </a>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse"><span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>    
                                <div id="w0-collapse" class="collapse navbar-collapse">
                                <?php
                                    echo Nav::widget([
                                        'encodeLabels' => false,
                                        'options' => ['class' =>'nav nav-pills navbar-nav navbar-right'],
                                        'items' => [
                                            [
                                                'label' => '<span class="glyphicon glyphicon-home"></span> Accueil',
                                                'url' => Yii::$app->urlManager->createUrl(['site/index']),
                                                'active' => $this->getControllerAction() == 'site/index',
                                                'options' => ['id' => 'li-home']
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-search"></span> Producteurs',
                                                'url' => Yii::$app->urlManager->createUrl(['site/producers']),
                                                'active' => $this->getControllerAction() == 'site/producers',
                                                'options' => ['id' => 'li-producteurs']
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-envelope"></span> Contact',
                                                'url' => Yii::$app->urlManager->createUrl(['site/contact']),
                                                'active' => $this->getControllerAction() == 'site/contact',
                                                'options' => ['id' => 'li-contact']
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-flag"></span> '.($producer ? Html::encode($producer->name) : ''),
                                                'url' => '#',
                                                'items' => [
                                                    [
                                                        'label' => '<span class="glyphicon glyphicon-th-large"></span> Mon espace',
                                                        'url' => Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index', 'slug_producer' => $producer ? $producer->slug : '']),
                                                    ],
                                                    [
                                                        'label' => '<span class="glyphicon glyphicon-cog"></span> Administration',
                                                        'url' => Yii::$app->urlManagerBackend->createAbsoluteUrl(['site/index']),
                                                    ]
                                                ],
                                                'visible' => !Yii::$app->user->isGuest && $producer
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-log-in"></span> Connexion',
                                                'url' => Yii::$app->urlManager->createUrl(['site/login']),
                                                'visible' => Yii::$app->user->isGuest,
                                                'active' => $this->getControllerAction() == 'site/login'
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-user"></span> Inscription',
                                                'url' => Yii::$app->urlManager->createUrl(['site/signup']),
                                                'visible' => Yii::$app->user->isGuest,
                                                'active' => $this->getControllerAction() == 'site/signup'
                                            ],
                                            [
                                                'label' => '<span class="glyphicon glyphicon-user"></span> '.((!Yii::$app->user->isGuest) ? Html::encode(Yii::$app->user->identity->name .' '.strtoupper(substr(Yii::$app->user->identity->lastname, 0, 1))) : '').'. ',
                                                'options' => ['id' => 'label1'],
                                                'url' => '#',
                                                'items' => [
                                                    [
                                                        'label' => '<span class="glyphicon glyphicon-user"></span> Profil',
                                                        'url' => Yii::$app->urlManager->createUrl(['user/update']),
                                                    ],
                                                    [
                                                        'label' => '<span class="glyphicon glyphicon-off"></span> Déconnexion',
                                                        'url' => Yii::$app->urlManager->createUrl(['site/logout']),
                                                    ]
                                                ],
                                                'visible' => !Yii::$app->user->isGuest
                                            ],
                                        ]
                                    ]);
                                ?>  
                            </div>
                        </div>
                    </nav>
		</header>
                
                <?php if(!Yii::$app->user->isGuest): ?>
                <section id="bookmarked-producers">
                    <div class="container">
                        <?php 
                        $producersArray = Producer::find()
                            ->joinWith(['userProducer user_producer'])
                            ->where([
                                'user_producer.id_user' => User::getCurrentId(), 
                                'user_producer.bookmark' => 1, 
                            ])
                            ->all() ;
                        ?>
                        <?php if(count($producersArray)): ?>
                            <h2>Mes producteurs &gt;</h2>
                            <div id="producers">
                                <?php foreach($producersArray as $producer): ?>
                                    <a class="btn btn-xs btn-default" href="<?= Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index','slug_producer' => $producer->slug]); ?>"><?= Html::encode($producer->name); ?></a>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <div id="discover">
                                <p>Vous n'avez aucun producteur dans vos favoris : </p>
                                <a class="btn btn-default btn-lg" href="<?= Yii::$app->urlManager->createUrl(['site/producers']); ?>"><span class="glyphicon glyphicon-grain"></span> Découvrez les producteurs</a>
                            </div>
                        <?php endif; ?>
                        <div class="clr"></div>
                    </div>
                </section>
                <?php endif; ?>
                
		<div id="main">
                    
                    <section class="container" id="content">
                        <?php if(Yii::$app->session->hasFlash('error')): ?>
                            <div class="alert alert-danger" role="alert">
                                <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php endif; ?>
                        <?php if(Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert alert-success" role="alert">
                                <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php endif; ?>

                        <?= $content ?>
                    </section>
		</div>
		<footer id="footer">
			<div class="container">
                            <a href="<?php echo Yii::$app->urlManager->createUrl('site/contact') ; ?>">Contact</a> &bull;
                            <a href="<?php echo Yii::$app->urlManager->createUrl('site/mentions') ; ?>">Mentions légales</a> &bull;
                            <a href="<?php echo Yii::$app->urlManager->createUrl('site/cgv') ; ?>">CGS</a> &bull;
                            <a id="code-source" href="https://framagit.org/guillaume-bourgeois/distrib">Code source <img src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-framagit.png" alt="Hébergé par Framasoft" /> <img src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-gitlab.png" alt="Propulsé par Gitlab" /></a> &bull;
                            <a id="social-mastodon" href="https://mastodon.social/@opendistrib">Mastodon <img src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-mastodon.png" alt="Mastodon" /></a> &bull;
                            <a id="social-diaspora" href="https://framasphere.org/people/db12d640c64c0137f1d52a0000053625">Diaspora <img src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-diaspora.png" alt="Diaspora" /></a>
                        </div>
		</footer>
	
	<?php $this->endBody() ?>
        
        <?= $this->render('@common/views/_analytics.php'); ?>
                
</body>
</html>
<?php $this->endPage() ?>
