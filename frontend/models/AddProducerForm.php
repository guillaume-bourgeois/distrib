<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\UserProducer;
use common\models\Producer;
use yii\helpers\Html;

/**
 * ContactForm is the model behind the contact form.
 */
class AddProducerForm extends Model 
{

    public $id_producer;
    public $code;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            ['id_producer', 'integer'],
            ['id_producer', 'required'],
            ['id_producer', function($attribute, $params) {

                $producer = Producer::findOne($this->id_producer);
                if (!$producer) {
                    $this->addError($attribute, 'Ce producteur n\'existe pas.');
                }
                
                $userProducerExist = UserProducer::searchOne([
                    'id_user' => Yii::$app->user->identity->id,
                    'active' => 1
                ]) ;

                if ($userProducerExist) {
                    $this->addError($attribute, 'Ce producteur est déjà sur votre tableau de bord.');
                }
            }],
            ['code', 'required', 'message' => 'Champs obligatoire', 'when' => function($model) {
                    $producer = Producer::findOne($this->id_producer);
                    if ($producer) {
                        return strlen($producer->code);
                    } else {
                        return false;
                    }
                }],
            ['code', function($attribute, $params) {
                    $code = $this->$attribute;
                    $producer = Producer::findOne($this->id_producer);

                    if ($producer && strtolower(trim($code)) != strtolower(trim($producer->code))) {
                        $this->addError($attribute, 'Code incorrect');
                    }
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id_producer' => 'Producteur',
            'code' => 'Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function add() 
    {
        $producer = Producer::findOne($this->id_producer);

        $userProducerExist = UserProducer::searchOne([
            'id_user' => User::getCurrentId(),
            'active' => 0
        ]) ;

        if ($userProducerExist) {
            $userProducerExist->active = 1;
            $userProducerExist->save();
        } else {
            $userProducer = new UserProducer();
            $userProducer->id_user = User::getCurrentId();
            $userProducer->id_producer = $this->id_producer;
            $userProducer->credit = 0;
            $userProducer->actif = 1;
            $userProducer->save();
        }

        Yii::$app->session->setFlash('success', 'Le producteur <strong>' . Html::encode($producer->name) . '</strong> a bien été ajoutée à votre tableau de bord.');
    }

}
        