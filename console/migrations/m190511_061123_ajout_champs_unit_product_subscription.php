<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190511_061123_ajout_champs_unit_product_subscription extends Migration {

    public function up() {
        $this->addColumn('product_subscription', 'unit', Schema::TYPE_STRING . ' DEFAULT \'piece\'');
        $this->addColumn('product_subscription', 'step', Schema::TYPE_FLOAT . ' DEFAULT 1');
    }

    public function down() {
        $this->dropColumn('product_subscription', 'unit');
        $this->dropColumn('product_subscription', 'step');
    }
}
