<?php

use yii\db\Migration;
use yii\db\Schema;

class m200106_143017_module_devis_bl_factures_modification_champs_id_status_order extends Migration
{
        public function safeUp()
        {
                $this->dropColumn('order', 'id_status');
        }

        public function safeDown()
        {
                $this->addColumn('order', 'id_status', Schema::TYPE_INTEGER);
        }
}
