<?php

use yii\db\Migration;

class m181220_080234_translate_database extends Migration
{
    public function up()
    {
        $this->renameTable('commande', 'order') ;
        $this->renameColumn('order', 'id_point_vente', 'id_point_sale') ;
        $this->renameColumn('order', 'id_production', 'id_distribution') ;
        $this->renameColumn('order', 'commentaire', 'comment') ;
        $this->renameColumn('order', 'commentaire_point_vente', 'comment_point_sale') ;
        $this->renameColumn('order', 'paiement_automatique', 'auto_payment') ;
        
        $this->renameTable('commande_auto', 'subscription') ;
        $this->renameColumn('subscription', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('subscription', 'date_debut', 'date_debin') ;
        $this->renameColumn('subscription', 'date_fin', 'date_end') ;
        $this->renameColumn('subscription', 'lundi', 'monday') ;
        $this->renameColumn('subscription', 'mardi', 'tuesday') ;
        $this->renameColumn('subscription', 'mercredi', 'wednesday') ;
        $this->renameColumn('subscription', 'jeudi', 'thursday') ;
        $this->renameColumn('subscription', 'vendredi', 'friday') ;
        $this->renameColumn('subscription', 'samedi', 'saterday') ;
        $this->renameColumn('subscription', 'dimanche', 'sunday') ;
        $this->renameColumn('subscription', 'periodicite_semaine', 'week_frequency') ;
        $this->renameColumn('subscription', 'id_point_vente', 'id_point_sale') ;
        $this->renameColumn('subscription', 'paiement_automatique', 'auto_payment') ;
        
        $this->renameTable('commande_auto_produit', 'product_subscription') ;
        $this->renameColumn('product_subscription', 'id_commande_auto', 'id_subscription') ;
        $this->renameColumn('product_subscription', 'id_produit', 'id_product') ;
        $this->renameColumn('product_subscription', 'quantite', 'quantity') ;
        
        $this->renameTable('commande_produit', 'product_order') ;
        $this->renameColumn('product_order', 'id_commande', 'id_order') ;
        $this->renameColumn('product_order', 'id_produit', 'id_product') ;
        $this->renameColumn('product_order', 'quantite', 'quantity') ;
        $this->renameColumn('product_order', 'prix', 'price') ;
        $this->renameColumn('product_order', 'mode_vente', 'sale_mode') ;
        
        $this->renameTable('credit_historique', 'credit_history') ;
        $this->renameColumn('credit_history', 'id_commande', 'id_order') ;
        $this->renameColumn('credit_history', 'montant', 'amount') ;
        $this->renameColumn('credit_history', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('credit_history', 'moyen_paiement', 'mean_payment') ;
        $this->renameColumn('credit_history', 'commentaire', 'comment') ;
        
        $this->renameTable('developpement', 'development') ;
        $this->renameColumn('development', 'objet', 'subject') ;
        $this->renameColumn('development', 'avancement', 'progress') ;
        $this->renameColumn('development', 'statut', 'status') ;
        $this->renameColumn('development', 'estimation_temps', 'time_estimate') ;
        $this->renameColumn('development', 'date_livraison', 'date_delivery') ;
        
        $this->renameTable('developpement_priorite', 'development_priority') ;
        $this->renameColumn('development_priority', 'id_developpement', 'id_development') ;
        $this->renameColumn('development_priority', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('development_priority', 'priorite', 'priority') ;
        
        $this->renameTable('etablissement', 'producer') ;
        $this->dropColumn('producer', 'gratuit') ;
        $this->renameColumn('producer', 'nom', 'name') ;
        $this->renameColumn('producer', 'code_postal', 'postcode') ;
        $this->renameColumn('producer', 'ville', 'city') ;
        $this->renameColumn('producer', 'date_paiement', 'date_payment') ;
        $this->renameColumn('producer', 'heure_limite_commande', 'order_deadline') ;
        $this->renameColumn('producer', 'delai_commande', 'order_delay') ;
        $this->renameColumn('producer', 'solde_negatif', 'negative_balance') ;
        $this->renameColumn('producer', 'credit_pain', 'credit') ;
        $this->renameColumn('producer', 'actif', 'active') ;
        $this->renameColumn('producer', 'prix_libre', 'free_price') ;
        $this->renameColumn('producer', 'infos_commande', 'order_infos') ;
        
        $this->renameTable('facture', 'invoice') ;
        $this->renameColumn('invoice', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('invoice', 'libelle', 'wording') ;
        $this->renameColumn('invoice', 'texte', 'text') ;
        $this->renameColumn('invoice', 'montant_ht', 'amount_without_tax') ;
        $this->renameColumn('invoice', 'paye', 'paid') ;
        $this->renameColumn('invoice', 'date_paiement', 'date_payment') ;
        $this->renameColumn('invoice', 'methode_paiement', 'mean_payment') ;
        $this->renameColumn('invoice', 'periode', 'period') ;
        $this->renameColumn('invoice', 'ca', 'turnover') ;
        
        $this->renameTable('point_vente', 'point_sale') ;
        $this->dropColumn('point_sale', 'pain') ;
        $this->dropColumn('point_sale', 'vrac') ;
        $this->renameColumn('point_sale', 'nom', 'name') ;
        $this->renameColumn('point_sale', 'adresse', 'address') ;
        $this->renameColumn('point_sale', 'horaires_lundi', 'infos_monday') ;
        $this->renameColumn('point_sale', 'horaires_mardi', 'infos_tuesday') ;
        $this->renameColumn('point_sale', 'horaires_mercredi', 'infos_wednesday') ;
        $this->renameColumn('point_sale', 'horaires_jeudi', 'infos_thursday') ;
        $this->renameColumn('point_sale', 'horaires_vendredi', 'infos_friday') ;
        $this->renameColumn('point_sale', 'horaires_samedi', 'infos_saterday') ;
        $this->renameColumn('point_sale', 'horaires_dimanche', 'infos_sunday') ;
        $this->renameColumn('point_sale', 'localite', 'locality') ;
        $this->renameColumn('point_sale', 'point_fabrication', 'point_production') ;
        $this->renameColumn('point_sale', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('point_sale', 'acces_restreint', 'restricted_access') ;
        $this->renameColumn('point_sale', 'credit_pain', 'credit') ;
        $this->renameColumn('point_sale', 'livraison_lundi', 'delivery_monday') ;
        $this->renameColumn('point_sale', 'livraison_mardi', 'delivery_tuesday') ;
        $this->renameColumn('point_sale', 'livraison_mercredi', 'delivery_wednesday') ;
        $this->renameColumn('point_sale', 'livraison_jeudi', 'delivery_thursday') ;
        $this->renameColumn('point_sale', 'livraison_vendredi', 'delivery_friday') ;
        $this->renameColumn('point_sale', 'livraison_samedi', 'delivery_saterday') ;
        $this->renameColumn('point_sale', 'livraison_dimanche', 'delivery_sunday') ;
        
        $this->renameTable('point_vente_user', 'user_point_sale') ;
        $this->renameColumn('user_point_sale', 'id_point_vente', 'id_point_sale') ;
        $this->renameColumn('user_point_sale', 'commentaire', 'comment') ;
        
        $this->renameTable('production', 'distribution') ;
        $this->renameColumn('distribution', 'actif', 'active') ;
        $this->renameColumn('distribution', 'livraison', 'delivery') ;
        $this->renameColumn('distribution', 'id_etablissement', 'id_producer') ;
        
        $this->renameTable('production_point_vente', 'point_sale_distribution') ;
        $this->renameColumn('point_sale_distribution', 'id_production', 'id_distribution') ;
        $this->renameColumn('point_sale_distribution', 'id_point_vente', 'id_point_sale') ;
        $this->renameColumn('point_sale_distribution', 'livraison', 'delivery') ;
        
        $this->renameTable('production_produit', 'product_distribution') ;
        $this->renameColumn('product_distribution', 'id_production', 'id_distribution') ;
        $this->renameColumn('product_distribution', 'id_produit', 'id_product') ;
        $this->renameColumn('product_distribution', 'actif', 'active') ;
        $this->renameColumn('product_distribution', 'quantite_max', 'quantity_max') ;
        
        $this->renameTable('produit', 'product') ;
        $this->dropColumn('product', 'illustration') ;
        $this->dropColumn('product', 'saison') ;
        $this->dropColumn('product', 'diminutif') ;
        $this->dropColumn('product', 'vrac') ;
        $this->renameColumn('product', 'nom', 'name') ;
        $this->renameColumn('product', 'actif', 'active') ;
        $this->renameColumn('product', 'prix', 'price') ;
        $this->renameColumn('product', 'poids', 'weight') ;
        $this->renameColumn('product', 'recette', 'recipe') ;
        $this->renameColumn('product', 'lundi', 'monday') ;
        $this->renameColumn('product', 'mardi', 'tuesday') ;
        $this->renameColumn('product', 'mercredi', 'wednesday') ;
        $this->renameColumn('product', 'jeudi', 'thursday') ;
        $this->renameColumn('product', 'vendredi', 'friday') ;
        $this->renameColumn('product', 'samedi', 'saterday') ;
        $this->renameColumn('product', 'dimanche', 'sunday') ;
        $this->renameColumn('product', 'quantite_max', 'quantity_max') ;
        $this->renameColumn('product', 'epuise', 'unavailable') ;
        $this->renameColumn('product', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('product', 'mode_vente', 'sale_mode') ;
        
        $this->dropColumn('user', 'confiance') ;
        $this->renameColumn('user', 'nom', 'lastname') ;
        $this->renameColumn('user', 'prenom', 'name') ;
        $this->renameColumn('user', 'telephone', 'phone') ;
        $this->renameColumn('user', 'adresse', 'address') ;
        $this->renameColumn('user', 'mail_prod_lundi', 'mail_distribution_monday') ;
        $this->renameColumn('user', 'mail_prod_mardi', 'mail_distribution_tuesday') ;
        $this->renameColumn('user', 'mail_prod_mercredi', 'mail_distribution_wednesday') ;
        $this->renameColumn('user', 'mail_prod_jeudi', 'mail_distribution_thursday') ;
        $this->renameColumn('user', 'mail_prod_vendredi', 'mail_distribution_friday') ;
        $this->renameColumn('user', 'mail_prod_samedi', 'mail_distribution_saterday') ;
        $this->renameColumn('user', 'mail_prod_dimanche', 'mail_distribution_sunday') ;
        $this->renameColumn('user', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('user', 'date_derniere_connexion', 'date_last_connection') ;

        $this->renameTable('user_etablissement', 'user_producer') ;
        $this->renameColumn('user_producer', 'id_etablissement', 'id_producer') ;
        $this->renameColumn('user_producer', 'actif', 'active') ;
        $this->renameColumn('user_producer', 'favoris', 'bookmark') ;
        
    }

    public function down()
    {
        $this->renameTable('order', 'commande') ;
        $this->renameColumn('commande', 'id_point_sale', 'id_point_vente') ;
        $this->renameColumn('commande', 'id_distribution', 'id_production') ;
        $this->renameColumn('commande', 'comment', 'commentaire') ;
        $this->renameColumn('commande', 'comment_point_sale', 'commentaire_point_vente') ;
        $this->renameColumn('commande', 'auto_payment', 'paiement_automatique') ;
        
        $this->renameTable('subscription', 'commande_auto') ;
        $this->renameColumn('commande_auto', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('commande_auto', 'date_debin', 'date_debut') ;
        $this->renameColumn('commande_auto', 'date_end', 'date_fin') ;
        $this->renameColumn('commande_auto', 'monday', 'lundi') ;
        $this->renameColumn('commande_auto', 'tuesday', 'mardi') ;
        $this->renameColumn('commande_auto', 'wednesday', 'mercredi') ;
        $this->renameColumn('commande_auto', 'thursday', 'jeudi') ;
        $this->renameColumn('commande_auto', 'friday', 'vendredi') ;
        $this->renameColumn('commande_auto', 'saterday', 'samedi') ;
        $this->renameColumn('commande_auto', 'sunday', 'dimanche') ;
        $this->renameColumn('commande_auto', 'week_frequency', 'periodicite_semaine') ;
        $this->renameColumn('commande_auto', 'id_point_sale', 'id_point_vente') ;
        $this->renameColumn('commande_auto', 'auto_payment', 'paiement_automatique') ;
        
        $this->renameTable('product_subscription', 'commande_auto_produit') ;
        $this->renameColumn('commande_auto_produit', 'id_subscription', 'id_commande_auto') ;
        $this->renameColumn('commande_auto_produit', 'id_product', 'id_produit') ;
        $this->renameColumn('commande_auto_produit', 'quantity', 'quantite') ;
        
        $this->renameTable('product_order', 'commande_produit') ;
        $this->renameColumn('commande_produit', 'id_order', 'id_commande') ;
        $this->renameColumn('commande_produit', 'id_product', 'id_produit') ;
        $this->renameColumn('commande_produit', 'quantity', 'quantite') ;
        $this->renameColumn('commande_produit', 'price', 'prix') ;
        $this->renameColumn('commande_produit', 'sale_mode', 'mode_vente') ;
        
        $this->renameTable('credit_history', 'credit_historique') ;
        $this->renameColumn('credit_historique', 'id_order', 'id_commande') ;
        $this->renameColumn('credit_historique', 'amount', 'montant') ;
        $this->renameColumn('credit_historique', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('credit_historique', 'mean_payment', 'moyen_paiement') ;
        $this->renameColumn('credit_historique', 'comment', 'commentaire') ;
        
        $this->renameTable('development', 'developpement') ;
        $this->renameColumn('developpement', 'subject', 'objet') ;
        $this->renameColumn('developpement', 'progress', 'avancement') ;
        $this->renameColumn('developpement', 'status', 'statut') ;
        $this->renameColumn('developpement', 'time_estimate', 'estimation_temps') ;
        $this->renameColumn('developpement', 'date_delivery', 'date_livraison') ;
        
        $this->renameTable('development_priority', 'developpement_priorite') ;
        $this->renameColumn('developpement_priorite', 'id_development', 'id_developpement') ;
        $this->renameColumn('developpement_priorite', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('developpement_priorite', 'priority', 'priorite') ;
        
        $this->renameTable('producer', 'etablissement') ;
        $this->addColumn('etablissement', 'gratuit', 'boolean') ;
        $this->renameColumn('etablissement', 'name', 'nom') ;
        $this->renameColumn('etablissement', 'postcode', 'code_postal') ;
        $this->renameColumn('etablissement', 'city', 'ville') ;
        $this->renameColumn('etablissement', 'date_payment', 'date_paiement') ;
        $this->renameColumn('etablissement', 'order_deadline', 'heure_limite_commande') ;
        $this->renameColumn('etablissement', 'order_delay', 'delai_commande') ;
        $this->renameColumn('etablissement', 'negative_balance', 'solde_negatif') ;
        $this->renameColumn('etablissement', 'credit', 'credit_pain') ;
        $this->renameColumn('etablissement', 'active', 'actif') ;
        $this->renameColumn('etablissement', 'free_price', 'prix_libre') ;
        $this->renameColumn('etablissement', 'order_infos', 'infos_commande') ;
        
        $this->renameTable('invoice', 'facture') ;
        $this->renameColumn('facture', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('facture', 'wording', 'libelle') ;
        $this->renameColumn('facture', 'text', 'texte') ;
        $this->renameColumn('facture', 'amount_without_tax', 'montant_ht') ;
        $this->renameColumn('facture', 'paid', 'paye') ;
        $this->renameColumn('facture', 'date_payment', 'date_paiement') ;
        $this->renameColumn('facture', 'mean_payment', 'methode_paiement') ;
        $this->renameColumn('facture', 'period', 'periode') ;
        $this->renameColumn('facture', 'turnover', 'ca') ;
        
        $this->renameTable('point_sale', 'point_vente') ;
        $this->addColumn('point_vente', 'pain', 'boolean') ;
        $this->addColumn('point_vente', 'vrac', 'boolean') ;
        $this->renameColumn('point_vente', 'name', 'nom') ;
        $this->renameColumn('point_vente', 'address', 'adresse') ;
        $this->renameColumn('point_vente', 'infos_monday', 'horaires_lundi') ;
        $this->renameColumn('point_vente', 'infos_tuesday', 'horaires_mardi') ;
        $this->renameColumn('point_vente', 'infos_wednesday', 'horaires_mercredi') ;
        $this->renameColumn('point_vente', 'infos_thursday', 'horaires_jeudi') ;
        $this->renameColumn('point_vente', 'infos_friday', 'horaires_vendredi') ;
        $this->renameColumn('point_vente', 'infos_saterday', 'horaires_samedi') ;
        $this->renameColumn('point_vente', 'infos_sunday', 'horaires_dimanche') ;
        $this->renameColumn('point_vente', 'locality', 'localite') ;
        $this->renameColumn('point_vente', 'point_production', 'point_fabrication') ;
        $this->renameColumn('point_vente', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('point_vente', 'restricted_access', 'access_restreint') ;
        $this->renameColumn('point_vente', 'credit', 'credit_pain') ;
        $this->renameColumn('point_vente', 'delivery_monday', 'livraison_lundi') ;
        $this->renameColumn('point_vente', 'delivery_tuesday', 'livraison_mardi') ;
        $this->renameColumn('point_vente', 'delivery_wednesday', 'livraison_mercredi') ;
        $this->renameColumn('point_vente', 'delivery_thursday', 'livraison_jeudi') ;
        $this->renameColumn('point_vente', 'delivery_friday', 'livraison_vendredi') ;
        $this->renameColumn('point_vente', 'delivery_saterday', 'livraison_samedi') ;
        $this->renameColumn('point_vente', 'delivery_sunday', 'livraison_dimanche') ;
        
        $this->renameTable('user_point_sale', 'point_vente_user') ;
        $this->renameColumn('point_vente_user', 'id_point_sale', 'id_point_vente') ;
        $this->renameColumn('point_vente_user', 'comment', 'commentaire') ;
        
        $this->renameTable('distribution', 'production') ;
        $this->renameColumn('production', 'active', 'actif') ;
        $this->renameColumn('production', 'delivery', 'livraison') ;
        $this->renameColumn('production', 'id_producer', 'id_etablissement') ;
        
        $this->renameTable('point_sale_distribution', 'production_point_vente') ;
        $this->renameColumn('production_point_vente', 'id_distribution', 'id_production') ;
        $this->renameColumn('production_point_vente', 'id_point_sale', 'id_point_vente') ;
        $this->renameColumn('production_point_vente', 'delivery', 'livraison') ;
        
        $this->renameTable('product_distribution', 'production_produit') ;
        $this->renameColumn('production_produit', 'id_distribution', 'id_production') ;
        $this->renameColumn('production_produit', 'id_product', 'id_produit') ;
        $this->renameColumn('production_produit', 'active', 'actif') ;
        $this->renameColumn('production_produit', 'quantity_max', 'quantite_max') ;
        
        $this->renameTable('product', 'produit') ;
        $this->addColumn('produit', 'illustration', 'varchar(255)') ;
        $this->addColumn('produit', 'saison', 'varchar(255)') ;
        $this->addColumn('produit', 'diminutif', 'varchar(255)') ;
        $this->addColumn('produit', 'vrac', 'boolean') ;
        $this->renameColumn('produit', 'name', 'nom') ;
        $this->renameColumn('produit', 'active', 'actif') ;
        $this->renameColumn('produit', 'price', 'prix') ;
        $this->renameColumn('produit', 'weight', 'poids') ;
        $this->renameColumn('produit', 'recipe', 'recette') ;
        $this->renameColumn('produit', 'monday', 'lundi') ;
        $this->renameColumn('produit', 'tuesday', 'mardi') ;
        $this->renameColumn('produit', 'wednesday', 'mercredi') ;
        $this->renameColumn('produit', 'thursday', 'jeudi') ;
        $this->renameColumn('produit', 'friday', 'vendredi') ;
        $this->renameColumn('produit', 'saterday', 'samedi') ;
        $this->renameColumn('produit', 'sunday', 'dimanche') ;
        $this->renameColumn('produit', 'quantity_max', 'quantite_max') ;
        $this->renameColumn('produit', 'unavailable', 'epuise') ;
        $this->renameColumn('produit', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('produit', 'sale_mode', 'mode_vente') ;
        
        $this->addColumn('user', 'confiance', 'boolean') ;
        $this->renameColumn('user', 'lastname', 'nom') ;
        $this->renameColumn('user', 'name', 'prenom') ;
        $this->renameColumn('user', 'phone', 'telephone') ;
        $this->renameColumn('user', 'address', 'adresse') ;
        $this->renameColumn('user', 'mail_distribution_monday', 'mail_prod_lundi') ;
        $this->renameColumn('user', 'mail_distribution_tuesday', 'mail_prod_mardi') ;
        $this->renameColumn('user', 'mail_distribution_wednesday', 'mail_prod_mercredi') ;
        $this->renameColumn('user', 'mail_distribution_thursday', 'mail_prod_jeudi') ;
        $this->renameColumn('user', 'mail_distribution_friday', 'mail_prod_vendredi') ;
        $this->renameColumn('user', 'mail_distribution_saterday', 'mail_prod_samedi') ;
        $this->renameColumn('user', 'mail_distribution_sunday', 'mail_prod_dimanche') ;
        $this->renameColumn('user', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('user', 'date_last_connection', 'date_derniere_connexion') ;

        $this->renameTable('user_producer', 'user_etablissement') ;
        $this->renameColumn('user_etablissement', 'id_producer', 'id_etablissement') ;
        $this->renameColumn('user_etablissement', 'active', 'actif') ;
        $this->renameColumn('user_etablissement', 'bookmark', 'favoris') ;
    }
}
