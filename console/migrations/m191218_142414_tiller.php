<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m191218_142414_tiller extends Migration
{
    public function up()
    {
        $this->addColumn('producer', 'tiller', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('producer', 'tiller_provider_token', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'tiller_restaurant_token', Schema::TYPE_STRING) ;
    }

    public function down()
    {
        $this->dropColumn('producer', 'tiller') ;
        $this->dropColumn('producer', 'tiller_provider_token') ;
        $this->dropColumn('producer', 'tiller_restaurant_token') ;
    }
}
