<?php

use yii\db\Migration;
use yii\db\Schema;

class m200602_073010_add_producer_option_payment_text extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_payment_info', Schema::TYPE_TEXT);
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_payment_info');

                return false;
        }
}
