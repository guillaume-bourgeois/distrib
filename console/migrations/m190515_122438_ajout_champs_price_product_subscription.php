<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190515_122438_ajout_champs_price_product_subscription extends Migration {

    public function up() {
        $this->addColumn('product_subscription', 'price', Schema::TYPE_FLOAT. ' DEFAULT 0') ;
        
        $productsSubscriptionsArray = common\models\ProductSubscription::find()->all() ;
        $productsArray = common\models\Product::find()->all() ;
        
        foreach($productsSubscriptionsArray as $productSubscription) {
            foreach($productsArray as $product) {
                if($productSubscription->id_product == $product->id) {
                    $productSubscription->price = $product->price ;
                    $productSubscription->save() ;
                }
            }
        }
    }

    public function down() {
        $this->dropColumn('product_subscription', 'price') ;
    }
}
