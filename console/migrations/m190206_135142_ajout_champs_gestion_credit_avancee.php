<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

use common\models\Producer ;

class m190206_135142_ajout_champs_gestion_credit_avancee extends Migration {

    public function up() {
        $this->addColumn('producer', 'credit_functioning', Schema::TYPE_STRING.' DEFAULT \''.Producer::CREDIT_FUNCTIONING_OPTIONAL.'\'') ;
        $this->addColumn('point_sale', 'credit_functioning', Schema::TYPE_STRING) ;
    }

    public function down() {
        $this->dropColumn('producer', 'credit_functioning') ;
        $this->dropColumn('point_sale', 'credit_functioning') ;
    }

}
