<?php

use yii\db\Migration;

class m201218_071628_add_option_export_csv extends Migration
{
    public function safeUp()
    {
            $this->addColumn('producer', 'option_csv_export_all_products', Schema::TYPE_BOOLEAN.' DEFAULT 0');
    }

    public function safeDown()
    {
            $this->dropColumn('producer', 'option_csv_export_all_products');

        return false;
    }
}
