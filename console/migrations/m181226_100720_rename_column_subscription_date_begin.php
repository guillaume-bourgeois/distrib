<?php

use yii\db\Migration;

class m181226_100720_rename_column_subscription_date_begin extends Migration
{
    public function up()
    {
        $this->renameColumn('subscription', 'date_debin', 'date_begin');
    }

    public function down()
    {
        $this->renameColumn('subscription', 'date_begin', 'date_debin') ;
    }
}
