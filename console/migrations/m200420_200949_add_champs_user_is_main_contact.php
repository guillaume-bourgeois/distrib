<?php

use yii\db\Migration;
use yii\db\Schema;

class m200420_200949_add_champs_user_is_main_contact extends Migration
{
        public function safeUp()
        {
                $this->addColumn('user', 'is_main_contact', Schema::TYPE_BOOLEAN . ' DEFAULT 1');
        }

        public function safeDown()
        {
                $this->dropColumn('user', 'is_main_contact');
        }
}
