<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190220_073434_ajout_champs_point_vente_defaut extends Migration {

    public function up() {
        $this->addColumn('point_sale', 'default', Schema::TYPE_BOOLEAN) ;
    }

    public function down() {
        $this->dropColumn('point_sale', 'default') ;
    }
}
