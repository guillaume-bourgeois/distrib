<?php

use yii\db\Migration;
use yii\db\Schema;

class m200107_140622_module_bl_devis_factures_modification_champ_taxrate extends Migration
{
        public function safeUp()
        {
                $this->renameColumn('tax_rate', 'pourcent', 'value');

        }

        public function safeDown()
        {
                $this->renameColumn('tax_rate', 'value', 'pourcent');

        }

}
