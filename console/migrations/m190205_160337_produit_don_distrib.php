<?php

use yii\db\Migration;

class m190205_160337_produit_don_distrib extends Migration {

    public function up() {
        $this->insert('product', [
            'name' => 'Don',
            'description' => 'Don à la plateforme distrib',
            'recipe' => '',
            'active' => 1,
            'photo' => null,
            'price' => 1,
            'weight' => null,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'quantity_max' => null,
            'id_producer' => 0,
            'sale_mode' => 'unit',
            'order' => 1000,
        ]) ;
    }

    public function down() {
        $this->delete('product', 'name = \'Don\' AND id_producer = 0') ;
    }
}
