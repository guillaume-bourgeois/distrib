<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190204_151019_add_champs_infos_legales extends Migration {

    public function up() {
        $this->addColumn('producer', 'mentions', Schema::TYPE_TEXT) ;
        $this->addColumn('producer', 'gcs', Schema::TYPE_TEXT) ;
    }

    public function down() {
        $this->dropColumn('producer', 'mentions') ;
        $this->dropColumn('producer', 'gcs') ;
    }
}
