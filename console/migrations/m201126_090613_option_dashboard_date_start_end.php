<?php

use yii\db\Migration;
use yii\db\Schema;

class m201126_090613_option_dashboard_date_start_end extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_dashboard_date_start', Schema::TYPE_DATE);
                $this->addColumn('producer', 'option_dashboard_date_end', Schema::TYPE_DATE);
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_dashboard_date_start');
                $this->dropColumn('producer', 'option_dashboard_date_end');

                return false;
        }
}
