<?php

use yii\db\Migration;
use yii\db\Schema;

class m200108_091551_module_bl_devis_factures_suppression_champs_documents extends Migration
{
        public function safeUp()
        {
                $this->dropColumn('invoice', 'city');
                $this->dropColumn('invoice', 'postcode');
                $this->dropColumn('quotation', 'city');
                $this->dropColumn('quotation', 'postcode');
                $this->dropColumn('delivery_note', 'city');
                $this->dropColumn('delivery_note', 'postcode');
        }

        public function safeDown()
        {
                $this->addColumn('invoice', 'city', Schema::TYPE_STRING);
                $this->addColumn('invoice', 'postcode', Schema::TYPE_STRING);
                $this->addColumn('quotation', 'city', Schema::TYPE_STRING);
                $this->addColumn('quotation', 'postcode', Schema::TYPE_STRING);
                $this->addColumn('delivery_note', 'city', Schema::TYPE_STRING);
                $this->addColumn('delivery_note', 'postcode', Schema::TYPE_STRING);
        }
}
