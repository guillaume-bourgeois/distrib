<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\db\Schema;
use yii\db\Migration;

class m150408_140526_add_champs_divers extends Migration
{
    public function up()
    {

    	// user de confiance ou non
    	$this->addColumn('user', 'confiance', Schema::TYPE_BOOLEAN) ;
    	
    	// points de vente
    	$this->addColumn('point_vente', 'horaires_lundi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_mardi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_mercredi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_jeudi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_vendredi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_samedi', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'horaires_dimanche', Schema::TYPE_TEXT) ;
    	$this->addColumn('point_vente', 'localite', Schema::TYPE_STRING) ;
    	$this->addColumn('point_vente', 'point_fabrication', Schema::TYPE_BOOLEAN) ;
    	
    	// production
    	$this->addColumn('production', 'livraison', Schema::TYPE_BOOLEAN) ;
    	
    	// table production_produit
    	$this->createTable('production_produit', [
    		'id' => 'pk',
    		'id_production' => Schema::TYPE_INTEGER,
    		'id_produit' => Schema::TYPE_INTEGER,
    		'actif' => Schema::TYPE_BOOLEAN
    	]) ; 
    	
    	// produit
    	$this->addColumn('produit', 'lundi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'mardi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'mercredi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'jeudi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'vendredi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'samedi', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'dimanche', Schema::TYPE_BOOLEAN) ;
    	$this->addColumn('produit', 'order', Schema::TYPE_INTEGER) ;
    	
    	
    }

    public function down()
    {
       
    	// user de confiance ou non
    	$this->dropColumn('user', 'confiance') ;
    	
    	// points de vente
    	$this->dropColumn('point_vente', 'horaires_lundi') ;
    	$this->dropColumn('point_vente', 'horaires_mardi') ;
    	$this->dropColumn('point_vente', 'horaires_mercredi') ;
    	$this->dropColumn('point_vente', 'horaires_jeudi') ;
    	$this->dropColumn('point_vente', 'horaires_vendredi') ;
    	$this->dropColumn('point_vente', 'horaires_samedi') ;
    	$this->dropColumn('point_vente', 'horaires_dimanche') ;
    	$this->dropColumn('point_vente', 'localite') ;
    	$this->dropColumn('point_vente', 'point_fabrication') ;
    	
    	// production
    	$this->dropColumn('production', 'livraison') ;
    	 
    	// table production_produit
    	$this->dropTable('production_produit');
    	
    	// produit
    	$this->dropColumn('produit', 'lundi') ;
    	$this->dropColumn('produit', 'mardi') ;
    	$this->dropColumn('produit', 'mercredi') ;
    	$this->dropColumn('produit', 'jeudi') ;
    	$this->dropColumn('produit', 'vendredi') ;
    	$this->dropColumn('produit', 'samedi') ;
    	$this->dropColumn('produit', 'dimanche') ;
    	$this->dropColumn('produit', 'order') ;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
