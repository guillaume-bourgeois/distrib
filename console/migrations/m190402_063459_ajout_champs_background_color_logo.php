<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190402_063459_ajout_champs_background_color_logo extends Migration {

    public function up() {
        $this->addColumn('producer', 'background_color_logo', Schema::TYPE_STRING);
    }

    public function down() {
        $this->dropColumn('producer', 'background_color_logo') ;
    }
}
