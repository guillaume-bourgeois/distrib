<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190121_101211_ajout_config_option_gestion_abonnement_par_l_utilisateur extends Migration {

    public function up() {
        $this->addColumn('producer', 'user_manage_subscription', Schema::TYPE_BOOLEAN) ;
    }

    public function down() {
        $this->dropColumn('producer', 'user_manage_subscription') ;
    }
}
