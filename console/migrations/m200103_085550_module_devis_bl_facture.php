<?php

use yii\db\Migration;
use yii\db\Schema;

class m200103_085550_module_devis_bl_facture extends Migration
{
    public function up()
    {
        // invoice
        $this->dropTable('invoice') ;
        $this->createTable('invoice', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
            'reference' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_DATETIME,
            'comment' => Schema::TYPE_TEXT,
            'id_user' => Schema::TYPE_INTEGER,
            'address' => Schema::TYPE_TEXT,
            'city' => Schema::TYPE_STRING,
            'postcode' => Schema::TYPE_STRING,
    	]);
        
        // quotation
        $this->createTable('quotation', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
            'reference' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_DATETIME,
            'comment' => Schema::TYPE_TEXT,
            'id_user' => Schema::TYPE_INTEGER,
            'address' => Schema::TYPE_TEXT,
            'city' => Schema::TYPE_STRING,
            'postcode' => Schema::TYPE_STRING,
    	]);
        
        // delivery_note
        $this->createTable('delivery_note', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
            'reference' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_DATETIME,
            'comment' => Schema::TYPE_TEXT,
            'id_point_sale_distribution' => Schema::TYPE_INTEGER,
            'id_user' => Schema::TYPE_INTEGER,
            'address' => Schema::TYPE_TEXT,
            'city' => Schema::TYPE_STRING,
            'postcode' => Schema::TYPE_STRING,
    	]);
        
        // order
        $this->addColumn('order', 'id_status', Schema::TYPE_INTEGER) ;
        $this->addColumn('order', 'id_invoice', Schema::TYPE_INTEGER) ;
        $this->addColumn('order', 'id_quotation', Schema::TYPE_INTEGER) ;
        $this->addColumn('order', 'id_delivery_note', Schema::TYPE_INTEGER) ;
        
        // order_status
        $this->createTable('order_status', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
    	]);
        
        // order_order_status
        $this->createTable('order_order_status', [
            'id' => 'pk',
            'id_order' => Schema::TYPE_INTEGER,
            'id_order_status' => Schema::TYPE_INTEGER,
            'date' => Schema::TYPE_DATETIME,
    	]);
        
        // product
        $this->addColumn('product', 'id_tax_rate', Schema::TYPE_INTEGER) ;
        
        // product_order
        $this->addColumn('product_order', 'id_tax_rate', Schema::TYPE_INTEGER) ;
        $this->addColumn('product_order', 'description', Schema::TYPE_TEXT) ;
        
        // tax_rate
        $this->createTable('tax_rate', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
            'pourcent' => Schema::TYPE_FLOAT,
    	]);
        
        // producer
        $this->addColumn('producer', 'status', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'id_tax_rate_default', Schema::TYPE_INTEGER) ;
        $this->addColumn('producer', 'document_quotation_prefix', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_quotation_first_reference', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_quotation_duration', Schema::TYPE_INTEGER) ;
        $this->addColumn('producer', 'document_invoice_prefix', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_invoice_first_reference', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_delivery_note_prefix', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_delivery_note_first_reference', Schema::TYPE_STRING) ;
        $this->addColumn('producer', 'document_infos_bottom', Schema::TYPE_TEXT) ;
        $this->addColumn('producer', 'document_infos_quotation', Schema::TYPE_TEXT) ;
        $this->addColumn('producer', 'document_infos_invoice', Schema::TYPE_TEXT) ;
        $this->addColumn('producer', 'document_infos_delivery_note', Schema::TYPE_TEXT) ;
    }

    public function down()
    {
        // invoice
        $this->dropTable('invoice') ;
        
        // quotation
        $this->dropTable('quotation') ;
        
        // delivery_note
        $this->dropTable('delivery_note') ;
        
        // order
        $this->dropColumn('order', 'id_status') ;
        $this->dropColumn('order', 'id_invoice') ;
        $this->dropColumn('order', 'id_quotation') ;
        $this->dropColumn('order', 'id_delivery_note') ;
        
        // order_status
        $this->dropTable('order_status') ;
        
        // order_order_status
        $this->dropTable('order_order_status') ;
        
        // product
        $this->dropColumn('product', 'id_tax_rate') ;
        
        // product_order
        $this->dropColumn('product_order', 'id_tax_rate') ;
        $this->dropColumn('product_order', 'description') ;
        
        // tax_rate
        $this->dropTable('tax_rate') ;
        
        // producer
        $this->dropColumn('producer', 'status') ;
        $this->dropColumn('producer', 'id_tax_rate_default') ;
        $this->dropColumn('producer', 'document_quotation_prefix') ;
        $this->dropColumn('producer', 'document_quotation_first_reference') ;
        $this->dropColumn('producer', 'document_quotation_duration') ;
        $this->dropColumn('producer', 'document_invoice_prefix') ;
        $this->dropColumn('producer', 'document_invoice_first_reference') ;
        $this->dropColumn('producer', 'document_delivery_note_prefix') ;
        $this->dropColumn('producer', 'document_delivery_note_first_reference') ;
        $this->dropColumn('producer', 'document_infos_bottom') ;
        $this->dropColumn('producer', 'document_infos_quotation') ;
        $this->dropColumn('producer', 'document_infos_invoice') ;
        $this->dropColumn('producer', 'document_infos_delivery_note') ;
    }
}
