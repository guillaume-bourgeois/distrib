<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190806_084324_ajout_champs_comment_subscription extends Migration
{
    public function up()
    {
        $this->addColumn('subscription', 'comment', Schema::TYPE_TEXT) ;
    }

    public function down()
    {
       $this->dropColumn('subscription', 'comment') ;
    }
}
