<?php

use yii\db\Migration;

class m181226_131915_rename_columns_saturday_point_sale extends Migration {

    public function up() {
        $this->renameColumn('point_sale', 'delivery_saterday', 'delivery_saturday') ;
        $this->renameColumn('point_sale', 'infos_saterday', 'infos_saturday') ;
        $this->renameColumn('subscription', 'saterday', 'saturday') ;
        $this->renameColumn('product', 'saterday', 'saturday') ;
    }

    public function down() {
        $this->renameColumn('point_sale', 'delivery_saturday', 'delivery_saterday') ;
        $this->renameColumn('point_sale', 'infos_saturday', 'infos_saterday') ;
        $this->renameColumn('subscription', 'saturday', 'saterday') ;
        $this->renameColumn('product', 'saturday', 'saterday') ;
    }
}
