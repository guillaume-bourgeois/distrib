<?php

use yii\db\Migration;
use yii\db\Schema;

class m200117_155246_point_sale_ajout_champs_id_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('point_sale', 'id_user', Schema::TYPE_INTEGER) ;
    }

    public function safeDown()
    {
        $this->dropColumn('point_sale', 'id_point_sale') ;
    }
}
