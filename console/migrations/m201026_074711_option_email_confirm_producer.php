<?php

use yii\db\Migration;

class m201026_074711_option_email_confirm_producer extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_email_confirm_producer', Schema::TYPE_BOOLEAN.' DEFAULT 0');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_email_confirm_producer');

                return false;
        }
}
