<?php

use yii\db\Migration;
use yii\db\Schema;

class m201116_104905_add_option_dashboard_number_distributions extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_dashboard_number_distributions', Schema::TYPE_INTEGER.' DEFAULT 3');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_dashboard_number_distributions');

                return false;
        }
}
