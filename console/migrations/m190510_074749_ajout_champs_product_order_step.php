<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190510_074749_ajout_champs_product_order_step extends Migration {

    public function up() {
        $this->addColumn('product_order', 'step', Schema::TYPE_FLOAT.' DEFAULT 1') ;
    }

    public function down() {
        $this->dropColumn('product_order','step') ;
    }
}
