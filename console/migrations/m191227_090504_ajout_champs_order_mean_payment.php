<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m191227_090504_ajout_champs_order_mean_payment extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'mean_payment', Schema::TYPE_STRING) ;
    }

    public function down()
    {
        $this->dropColumn('order', 'mean_payment') ;
    }
}
