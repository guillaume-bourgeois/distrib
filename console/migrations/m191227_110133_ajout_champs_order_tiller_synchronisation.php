<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m191227_110133_ajout_champs_order_tiller_synchronisation extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'tiller_synchronization', Schema::TYPE_BOOLEAN.' DEFAULT 1') ;
    }

    public function down()
    {
        $this->dropColumn('order', 'tiller_synchronization') ;
    }
}
