<?php

use yii\db\Migration;
use yii\db\Schema;

class m201207_164410_specific_prices_percent extends Migration
{
    public function safeUp()
    {
            $this->addColumn('product_price', 'percent', Schema::TYPE_INTEGER) ;
    }

    public function safeDown()
    {
            $this->dropColumn('product_price', 'percent') ;

                return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201207_164410_specific_prices_percent cannot be reverted.\n";

        return false;
    }
    */
}
