<?php

use yii\db\Migration;
use yii\db\Schema;

class m200107_101930_module_devis_bl_facture_suppression_champs_delivery_note extends Migration
{
        public function safeUp()
        {
                $this->dropColumn('delivery_note', 'id_point_sale');
                $this->dropColumn('delivery_note', 'id_distribution');
        }

        public function safeDown()
        {
                $this->addColumn('delivery_note', 'id_point_sale', Schema::TYPE_INTEGER);
                $this->addColumn('delivery_note', 'id_distribution', Schema::TYPE_INTEGER);
        }
}
