<?php

use yii\db\Migration;
use yii\db\Schema;

class m210318_095733_add_product_reference extends Migration
{
        public function safeUp()
        {
                $this->addColumn('product', 'reference', Schema::TYPE_STRING . ' DEFAULT NULL') ;
                $this->addColumn('producer', 'option_export_display_product_reference', Schema::TYPE_BOOLEAN.' DEFAULT 0') ;
        }

        public function safeDown()
        {
                $this->dropColumn('product', 'reference') ;
                $this->dropColumn('producer', 'option_export_display_product_reference') ;
        }
}
