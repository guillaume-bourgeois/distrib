<?php

use yii\db\Migration;
use yii\db\Schema;

class m210419_100507_add_option_display_export_grid extends Migration
{
    public function safeUp()
    {
            $this->addColumn('producer', 'option_display_export_grid', Schema::TYPE_BOOLEAN. ' DEFAULT 0');
    }

    public function safeDown()
    {
            $this->dropColumn('producer', 'option_display_export_grid');
    }

}
