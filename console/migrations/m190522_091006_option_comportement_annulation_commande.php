<?php

use yii\db\Migration;
use yii\db\mysql\Schema;
use common\models\Producer ;

class m190522_091006_option_comportement_annulation_commande extends Migration {

    public function up() {
        $this->addColumn('producer', 'option_behavior_cancel_order', Schema::TYPE_STRING.' DEFAULT \''.Producer::BEHAVIOR_DELETE_ORDER_STATUS.'\'') ;
    }

    public function down() {
        $this->dropColumn('producer', 'option_behavior_cancel_order') ;
    }
}
