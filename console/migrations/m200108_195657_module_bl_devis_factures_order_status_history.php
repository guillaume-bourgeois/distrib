<?php

use yii\db\Migration;
use yii\db\Schema;

class m200108_195657_module_bl_devis_factures_order_status_history extends Migration
{
        public function safeUp()
        {
                $this->delete('order_status');
                $this->renameTable('order_order_status', 'order_status_history');
                $this->addColumn('order_status_history', 'id_user', Schema::TYPE_INTEGER);
                $this->addColumn('order', 'status', Schema::TYPE_STRING);
                $this->renameColumn('order_status_history', 'id_order_status', 'status');
                $this->alterColumn('order_status_history', 'status', Schema::TYPE_STRING);
        }

        public function safeDown()
        {
                echo "m200108_195657_module_bl_devis_factures_order_status_history cannot be reverted.\n";

                return 'false';
        }

        /*
        // Use up()/down() to run migration code without a transaction.
        public function up()
        {

        }

        public function down()
        {
            echo "m200108_195657_module_bl_devis_factures_order_status_history cannot be reverted.\n";

            return false;
        }
        */
}
