<?php

use yii\db\Migration;
use yii\db\Schema;
use common\models\Producer ;

class m200601_073309_add_producer_options extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'behavior_home_point_sale_day_list', Schema::TYPE_STRING.' DEFAULT \''.Producer::BEHAVIOR_HOME_POINT_SALE_DAY_LIST_WEEK.'\'');
                $this->addColumn('producer', 'behavior_order_select_distribution', Schema::TYPE_STRING.' DEFAULT \''.Producer::BEHAVIOR_ORDER_SELECT_DISTRIBUTION_CALENDAR.'\'');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'behavior_home_point_sale_day_list');
                $this->dropColumn('producer', 'behavior_order_select_distribution');

                return false;
        }
}
