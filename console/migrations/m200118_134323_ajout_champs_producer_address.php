<?php

use yii\db\Migration;
use yii\db\Schema;

class m200118_134323_ajout_champs_producer_address extends Migration
{
    public function safeUp()
    {
        $this->addColumn('producer', 'address', Schema::TYPE_TEXT) ;
    }

    public function safeDown()
    {
        $this->dropColumn('producer', 'address') ;
    }
}
