<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190205_170712_ajout_option_autoriser_don_plateforme extends Migration {

    public function up() {
        $this->addColumn('producer', 'option_allow_user_gift', Schema::TYPE_BOOLEAN.' DEFAULT 1') ;
    }

    public function down() {
        $this->dropColumn('producer', 'option_allow_user_gift') ;
    }
}
