<?php

use yii\db\Migration;
use yii\db\Schema;

class m200624_083903_product_distribution_change_column_quantity_max extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product_distribution', 'quantity_max', Schema::TYPE_FLOAT) ;
    }

    public function safeDown()
    {
        echo "m200624_083903_product_distribution_change_column_quantity_max cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200624_083903_product_distribution_change_column_quantity_max cannot be reverted.\n";

        return false;
    }
    */
}
