<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190508_155647_ajout_champs_vente_kilo extends Migration {

    public function up() {
        $this->dropColumn('product', 'sale_mode') ;
        $this->addColumn('product', 'unit', Schema::TYPE_STRING.' DEFAULT \'piece\'') ;
        $this->addColumn('product', 'step', Schema::TYPE_FLOAT.' DEFAULT 1') ;
        
        $this->dropColumn('product_order', 'sale_mode') ;
        $this->addColumn('product_order', 'unit', Schema::TYPE_STRING.' DEFAULT \'piece\'') ;
    }

    public function down() {
        $this->addColumn('product', 'sale_mode', Schema::TYPE_STRING.' DEFAULT \'unit\'') ;
        $this->dropColumn('product', 'unit') ;
        $this->dropColumn('product', 'step') ;
        
        $this->addColumn('product_order', 'sale_mode', Schema::TYPE_STRING.' DEFAULT \'unit\'') ;
        $this->dropColumn('product_order', 'unit') ;
    }

}
