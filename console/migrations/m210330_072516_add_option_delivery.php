<?php

use yii\db\Migration;
use yii\db\Schema;

class m210330_072516_add_option_delivery extends Migration
{

        public function safeUp()
        {
                $this->addColumn('producer', 'option_delivery', Schema::TYPE_BOOLEAN. ' DEFAULT 0');
                $this->addColumn('order', 'delivery_home', Schema::TYPE_BOOLEAN.' DEFAULT 0');
                $this->addColumn('order', 'delivery_address', Schema::TYPE_TEXT);
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_delivery');
                $this->dropColumn('order', 'delivery_home');
                $this->dropColumn('order', 'delivery_address');
        }

}

