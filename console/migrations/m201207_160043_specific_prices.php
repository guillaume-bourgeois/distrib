<?php

use yii\db\Migration;
use yii\db\Schema;

class m201207_160043_specific_prices extends Migration
{
    public function safeUp()
    {
            $this->createTable('product_price', [
                    'id' => 'pk',
                    'id_product' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_user' => Schema::TYPE_INTEGER,
                    'id_point_sale' => Schema::TYPE_INTEGER,
                    'price' => Schema::TYPE_FLOAT,
            ]);

            $this->addColumn('user_producer', 'product_price_percent', Schema::TYPE_INTEGER) ;
            $this->addColumn('point_sale', 'product_price_percent', Schema::TYPE_INTEGER) ;
    }

    public function safeDown()
    {
            $this->dropTable('product_price') ;
            $this->dropColumn('user_producer', 'product_price_percent') ;
            $this->dropColumn('point_sale', 'product_price_percent') ;

            return false;
    }
}
