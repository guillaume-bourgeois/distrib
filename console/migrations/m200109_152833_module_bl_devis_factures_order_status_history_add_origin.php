<?php

use yii\db\Migration;
use yii\db\Schema;

class m200109_152833_module_bl_devis_factures_order_status_history_add_origin extends Migration
{
    public function safeUp()
    {
            $this->addColumn('order_status_history', 'origin', Schema::TYPE_STRING);

    }

    public function safeDown()
    {
            $this->dropColumn('order_status_history', 'origin');
    }

}
