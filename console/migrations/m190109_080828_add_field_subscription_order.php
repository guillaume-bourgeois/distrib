<?php

use yii\db\Migration;

class m190109_080828_add_field_subscription_order extends Migration {

    public function up() {
        $this->addColumn('order', 'id_subscription', Schema::TYPE_INTEGER) ;
    }

    public function down() {
        $this->dropColumn('order', 'id_subscription') ;
    }
}
