<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190225_093245_ajout_champs_gestion_credit_utilisateur extends Migration {

    public function up() {
        $this->addColumn('user_producer', 'credit_active', Schema::TYPE_BOOLEAN.' DEFAULT 0') ;
    }

    public function down() {
        $this->dropColumn('user_producer', 'credit_active') ;
    }
}
