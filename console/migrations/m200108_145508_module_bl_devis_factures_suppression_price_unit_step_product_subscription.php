<?php

use yii\db\Migration;

class m200108_145508_module_bl_devis_factures_suppression_price_unit_step_product_subscription extends Migration
{
        public function safeUp()
        {
                $this->dropColumn('product_subscription', 'unit');
                $this->dropColumn('product_subscription', 'step');
                $this->dropColumn('product_subscription', 'price');
        }

        public function safeDown()
        {
                echo "m200108_145508_module_bl_devis_factures_suppression_price_unit_step_product_subscription cannot be reverted.\n";

                return false;
        }

        /*
        // Use up()/down() to run migration code without a transaction.
        public function up()
        {

        }

        public function down()
        {
            echo "m200108_145508_module_bl_devis_factures_suppression_price_unit_step_product_subscription cannot be reverted.\n";

            return false;
        }
        */
}
