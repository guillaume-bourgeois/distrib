<?php

use yii\db\Migration;
use yii\db\Schema;

class m210303_095445_add_product_order_invoice_price extends Migration
{
        public function safeUp()
        {
                $this->addColumn('product_order', 'invoice_price', Schema::TYPE_FLOAT. ' DEFAULT NULL') ;
        }

        public function safeDown()
        {
                $this->dropColumn('product_order', 'invoice_price') ;
        }
}
