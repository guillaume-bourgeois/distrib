<?php

use yii\db\Migration;
use yii\db\Schema;

class m210303_081449_add_table_group_user extends Migration
{
        public function up()
        {
                $this->createTable('user_group', [
                        'id' => 'pk',
                        'id_producer' => Schema::TYPE_INTEGER. ' NOT NULL',
                        'name' => Schema::TYPE_STRING . ' NOT NULL',
                ]);

                $this->createTable('user_user_group', [
                        'id' => 'pk',
                        'id_user' => Schema::TYPE_INTEGER. ' NOT NULL',
                        'id_user_group' => Schema::TYPE_INTEGER. ' NOT NULL',
                ]);

        }

        public function down()
        {
                $this->dropTable('user_group');
                $this->dropTable('user_user_group');
        }

}
