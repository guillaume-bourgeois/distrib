#!/usr/bin/env bash
sudo chown fab:www-data frontend/runtime/ -R
chmod 775 frontend/runtime/

sudo chown fab:www-data frontend/web/assets/ -R
sudo chmod 775 frontend/web/assets/ -R

sudo chown fab:www-data backend/runtime/ -R
sudo chmod 775 backend/runtime/ -R

sudo chown fab:www-data backend/web/assets/ -R
sudo chmod 775 backend/web/assets/ -R
