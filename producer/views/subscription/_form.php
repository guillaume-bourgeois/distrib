<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper ;
use common\models\User ;
use common\models\PointSale ;

\producer\assets\VuejsSubscriptionFormAsset::register($this) ;

?>

<div class="subscription-form" id="app-subscription-form" :class="{'loaded': !loading}">
    
    <form @submit.prevent="formSubmit()" v-show="loading == false">
        
    <input type="hidden" id="subscription-id" value="<?= $idSubscription; ?>" />
        
    <div class="alert alert-danger" v-if="errors.length">
        <ul>
            <li v-for="error in errors">
                {{ error }}
            </li>
        </ul>
    </div>
    <div class="points-sale">
        <h3><span>Point de vente</span></h3>
        
        <table class="table table-bordered table-hovered" v-if="pointsSale.length">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Localité</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="pointSale in pointsSale" v-if="pointSale" :class="(pointSaleActive && pointSale.id == pointSaleActive.id) ? 'selected' : ''">
                    <td class="name">
                        <span class="the-name">{{ pointSale.name }}</span>
                        <div class="comment" v-if="pointSale.userPointSale">
                            {{ pointSale.userPointSale.comment }}
                        </div>
                    </td>
                    <td class="locality">{{ pointSale.locality }}</td>
                    <td class="actions">
                        <div :class="'form-group' + (pointSale.invalid_code ? ' has-error' : '')">
                            <div class="input-group" v-if="pointSale.code && pointSale.code.length > 0">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input v-model="pointsSaleCodes[pointSale.id]" type="password" placeholder="Code" class="form-control input-code" />
                            </div>
                        </div>
                        <button class="btn btn-primary" @click.prevent="pointSaleClick" :data-code="pointSale.code && pointSale.code.length > 0" :data-id-point-sale="pointSale.id">
                            <span class="glyphicon glyphicon-map-marker"></span>
                            Choisir 
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <h3 id="step-date"><span>Dates</span></h3>
    <div class="col-md-4">
        <div class="form-group">
            <label>Date de début</label>
            <v-date-picker 
                mode="single" 
                v-model="dateBegin"
                :input-props='{class: "form-control", placeholder: "" }'>
            </v-date-picker>
        </div>
    </div>
    <div class="clr"></div>    
    <div class="col-md-4">
        <div class="form-group">
            <label>Date de fin</label>
            <v-date-picker 
                mode="single" 
                v-model="dateEnd"
                :min-date="dateBegin"
                :input-props='{class: "form-control", placeholder: ""}'>
            </v-date-picker>
            <div class="hint-block">Laisser vide pour une durée indéterminée</div> 
        </div>
    </div>
    <div class="clr"></div>
    <div class="col-md-4">
        <label for="subscriptionform-week_frequency">Périodicité</label>
        <select id="subscriptionform-week_frequency" class="form-control" v-model="weekFrequency">
            <option value="1">Toutes les semaines</option> 
            <option value="2">Toutes les 2 semaines</option> 
            <option value="3">Toutes les 3 semaines</option> 
            <option value="4">Tous les mois</option>
        </select>
    </div>
    <div class="clr"></div>
    
    <h3><span>Paiement</span></h3>

    <?php if(Producer::getConfig('credit')): ?>
    <div class="form-group field-subscriptionform-auto_payment">
        <label><input type="checkbox" id="subscriptionform-auto_payment" name="SubscriptionForm[auto_payment]" v-model="autoPayment"> Paiement automatique</label> 
        <div class="hint-block">Cochez cette case si vous souhaitez que votre Crédit soit automatiquement débité.</div> 
        <div class="help-block"></div>
    </div>
    <?php endif; ?>
    
    <div class="days" v-if="pointSaleActive">
        <h3 id="step-days"><span>Jours</span></h3>
        <div v-if="pointSaleActive.delivery_monday == true">
            <div class="form-group field-subscriptionform-monday">
                <label><input type="checkbox" id="subscriptionform-monday" v-model="monday" @change="dayChange"> Lundi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_tuesday == true">
            <div class="form-group field-subscriptionform-monday">
                <label><input type="checkbox" id="subscriptionform-tuesday" v-model="tuesday" @change="dayChange"> Mardi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_wednesday == true">
            <div class="form-group field-subscriptionform-wednesday">
                <label><input type="checkbox" id="subscriptionform-wednesday" v-model="wednesday" @change="dayChange"> Mercredi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_thursday == true">
            <div class="form-group field-subscriptionform-thursday">
                <label><input type="checkbox" id="subscriptionform-thursday" v-model="thursday" @change="dayChange"> Jeudi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_friday == true">
            <div class="form-group field-subscriptionform-friday">
                <label><input type="checkbox" id="subscriptionform-friday" v-model="friday" @change="dayChange"> Vendredi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_saturday == true">
            <div class="form-group field-subscriptionform-saturday">
                <label><input type="checkbox" id="subscriptionform-saturday" v-model="saturday" @change="dayChange"> Samedi</label> 
            </div>
        </div>
        <div v-if="pointSaleActive.delivery_sunday == true">
            <div class="form-group field-subscriptionform-sunday">
                <label><input type="checkbox" id="subscriptionform-sunday" v-model="sunday" @change="dayChange"> Dimanche</label> 
            </div>
        </div>

        <div class="alert alert-warning" v-if="!pointSaleActive.delivery_monday && !pointSaleActive.delivery_tuesday && !pointSaleActive.delivery_wednesday && !pointSaleActive.delivery_thursday && !pointSaleActive.delivery_friday && !pointSaleActive.delivery_saturday && !pointSaleActive.delivery_sunday">
            Aucun jour de distribution disponible pour ce point de vente.
        </div>
    </div>
    
    <div class="clr"></div>
    
    <div class="products" v-if="(monday || tuesday || wednesday || thursday || friday || saturday || sunday) && checkOneProductAvailable()">
        <h3><span>Produits</span></h3>
        <?php if(isset($model->errors['products']) && count($model->errors['products']))
        {
            echo '<div class="alert alert-danger">'.$model->errors['products'][0].'</div>' ;
        }
        ?>
        <table :class="'table table-bordered table-condensed table-hover' + (monday ? ' monday-active' : '') + (tuesday ? ' tuesday-active' : '') + (wednesday ? ' wednesday-active' : '') + (thursday ? ' thursday-active' : '') + (friday ? ' friday-active' : '') + (saturday ? ' saturday-active' : '') + (sunday ? ' sunday-active' : '')">
             <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prix unitaire</th>
                    <th>Quantité</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="product in products" v-if="checkProductAvailable(product)">
                    <td>
                        <span class="name">{{ product.name }}</span> 
                        <span class="other">
                            <span v-if="product.description.length">/</span> 
                            <span class="description">{{ product.description }}</span> 
                            <span v-if="product.weight">({{ product.weight }}g)</span>
                        </span>
                        <div class="recipe" v-if="product.recipe.length">{{ product.recipe }}</div>
                    </td>
                    <td class="price-unit">
                        {{ formatPrice(product.price_with_tax) }}<br /><span class="unit">{{ product.wording_unit }}</span>

                    </td>
                    <td class="quantity">
                        <div class="input-group">
                            <input type="hidden" :value="product.step" :name="'product_step_'+product.step" />
                            <input type="hidden" :value="product.unit" :name="'product_unit_'+product.unit" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" @click="productQuantityClick(product, -product.step)"><span class="glyphicon glyphicon-minus"></span></button>
                            </span>
                            <input type="text" v-model="product.quantity_form" :class="'form-control '+((product.quantity_form > 0) ? 'has-quantity' : '')">
                            <span class="input-group-addon">{{ product.wording_short }}</span>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" @click="productQuantityClick(product, product.step)"><span class="glyphicon glyphicon-plus"></span></button>
                            </span>
                        </div>
                    </td>
                    <td class="price-total">
                        {{ formatPrice(product.price_with_tax * (product.quantity_form / product.coefficient_unit )) }}
                    </td>
                </tr>
                <tr class="total">
                    <td colspan="3"></td>
                    <td class="price-total">{{ priceTotal(true) }}</td>
                </tr>
            </tbody>
        </table>
        <div v-if="!checkOneProductAvailable()" class="alert alert-warning">
            Aucun produit n'est disponible pour les jours de distribution sélectionnés.
        </div>
    </div>    
    
    <div class="clr"></div>

    <div>
        <h3><span>Commentaire</span></h3>
        <div class="form-group">
            <textarea id="subscriptionform-comment" class="form-control comment-textarea" v-model="comment"></textarea>
        </div>
    </div>
    
    <button class="btn btn-primary" disabled="disabled" v-if="disableSubmitButton">Enregistrer</button>
    <button class="btn btn-primary" v-else>Enregistrer</button>
</form>
</div>