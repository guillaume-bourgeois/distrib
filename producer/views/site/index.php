<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use common\helpers\GlobalParam ;
use common\models\Distribution;
use common\models\PointSaleDistribution;
use common\models\Producer ;

$producer = $this->context->getProducer() ;
$this->setTitle('Accueil');
$this->setPageTitle(Html::encode($producer->type.' à '.$producer->city)) ;
?>

<section id="presentation">
    <?php if(strlen($producer->description)): ?>
    <div class="description">
        <?= nl2br(Html::encode($producer->description));  ?>
    </div>
    <?php endif; ?>
    <div class="clr"></div>
</section>


<section id="points-sale">
    <h3><span>Points de vente</span></h3>
    <?= GridView::widget([
        'dataProvider' => $dataProviderPointsSale,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'contentOptions' => ['class' => 'name'],
                'value' => function($model) {
                    $html = '<span class="the-name">'.Html::encode($model->name).'</span>' ;
                    if(strlen($model->locality)) {
                        $html .= '<br />à '.Html::encode($model->locality) ;
                    }
                    return $html ;
                }
            ],
            [
                'label' => 'Jours de livraison',
                'format' => 'raw',
                 'contentOptions' => ['class' => 'days'],
                'value' => function($model) {

                        $producer = \Yii::$app->controller->getProducer() ;

                        if($producer->behavior_home_point_sale_day_list == Producer::BEHAVIOR_HOME_POINT_SALE_DAY_LIST_WEEK) {
                                $arrayDays = [
                                    'monday' => 'Lundi',
                                    'tuesday' => 'Mardi',
                                    'wednesday' => 'Mercredi',
                                    'thursday' => 'Jeudi',
                                    'friday' => 'Vendredi',
                                    'saturday' => 'Samedi',
                                    'sunday' => 'Dimanche'
                                ] ;

                                $html = '' ;
                                foreach($arrayDays as $dayEn => $dayFr) {
                                        $fieldDelivery = 'delivery_'.$dayEn ;
                                        $fieldInfos = 'infos_'.$dayEn ;

                                        if ($model->$fieldDelivery) {
                                                $html .= '<strong>'.$dayFr.'</strong>';
                                                if(strlen($model->$fieldInfos)) {
                                                        $html .= ' / <small>'.nl2br(str_replace(['[select_previous_day]', '[/select_previous_day]'], '', $model->$fieldInfos)).'</small>' ;
                                                }
                                                $html .= '<br />' ;
                                        }
                                }

                                return $html  ;
                        }
                        elseif($producer->behavior_home_point_sale_day_list == Producer::BEHAVIOR_HOME_POINT_SALE_DAY_LIST_INCOMING_DISTRIBUTIONS) {
                                $html = '' ;
                                $incomingDistributions = Distribution::getIncomingDistributions();
                                $cpt = 0 ;
                                foreach($incomingDistributions as $distribution) {
                                        $countPointSaleDistribution = PointSaleDistribution::searchCount([
                                                'id_distribution' => $distribution->id,
                                                'id_point_sale' => $model->id,
                                                'delivery' => 1
                                        ]);
                                        if($countPointSaleDistribution) {
                                                $html .= strftime('%A %d %B', strtotime($distribution->date)).'<br />';
                                        }

                                        $cpt ++ ;
                                }
                                return $html ;
                        }
                }
            ]
        ],
    ]); ?>
</section>


<section id="products">
    <h3><span>Produits</span></h3>

        <?php

        $columnsProducts  = [] ;
        if($hasProductPhoto) {
                $columnsProducts [] = [
                        'attribute' => 'photo',
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'photo'],
                        'value' => function($model) {
                                if(strlen($model->photo)) {
                                        return '<img class="photo-product" src="'.Yii::$app->urlManagerProducer->baseUrl.'/uploads/'.$model->photo.'" />' ;
                                }
                                return '' ;
                        }
                ] ;
        }

        $columnsProducts[] = [
                'attribute' => 'name',
                'format' => 'raw',
                'contentOptions' => ['class' => 'name'],
                'value' => function($model) {
                        $html = '<span class="the-name">'.Html::encode($model->name).'</span>' ;
                        if(strlen($model->description)) {
                                $html .= ' / '.Html::encode($model->description) ;
                        }
                        if(strlen($model->recipe)) {
                                $html .= '<br />'.nl2br(Html::encode($model->recipe)) ;
                        }
                        return $html ;
                }
        ] ;

        if($hasProductWeight) {
                $columnsProducts[] = [
                        'attribute' => 'weight',
                        'format' => 'raw',
                        'value' => function($model) {
                                if(strlen($model->weight)) {
                                        return $model->weight.'&nbsp;g' ;
                                }
                                return '' ;
                        }
                ] ;
        }

        $columnsProducts[] = [
                'attribute' => 'price',
                'value' => function($model) {
                        if($model->price) {
                                return Price::format($model->getPriceWithTax()).' ('.Product::strUnit($model->unit, 'wording_unit', true).')' ;
                        }
                        return '' ;
                }
        ] ;
        ?>

        <?php if($dataProviderProducts->query->count()) : ?>
            <?= GridView::widget([
                    'dataProvider' => $dataProviderProducts,
                    'summary' => '',
                    'columns' => $columnsProducts
            ]); ?>
        <?php endif; ?>

        <?php foreach($categories as $category): ?>
            <?php if($dataProviderProductsByCategories[$category->id]->query->count()): ?>
                <h4><?= Html::encode($category->name) ?></h4>
                <?= GridView::widget([
                        'dataProvider' => $dataProviderProductsByCategories[$category->id],
                        'summary' => '',
                        'columns' => $columnsProducts
                ]); ?>
            <?php endif; ?>
        <?php endforeach; ?>
</section>