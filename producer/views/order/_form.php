<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\widgets\ActiveForm;
use common\models\Product;

?>
<div class="order-form">
    
    <?php
    $form = ActiveForm::begin([
        'enableClientScript' => false
    ]);
    ?>
    
    <?php 
    if(count($distributionDaysArray) <= 1) :
    ?>
    <div class="alert alert-warning">Aucun jour de production n'a été programmé par le producteur.</div>
    <?php endif; ?>
    
        
    <?php if($idProducer && count($distributionDaysArray) > 1): ?>   
        
    <div id="step-date" class="col-md-6">
        <?= $form->field($model, 'id_distribution')->label('')->hiddenInput(); ?>

        <?php if (isset($model->id)): ?>
            <div class="date-order"><span><?php echo date('d/m/Y', strtotime($distribution->date)); ?></span></div>
            <?= Html::hiddenInput('id_order', $model->id,['id'=>'id-order']); ?>
            <?= Html::hiddenInput('paid_amount', $model->getAmount(Order::AMOUNT_PAID),['id'=>'paid-amount']); ?>
        <?php endif; ?> 

        <div id="datepicker-distribution" <?php if (isset($model->id)): ?>style="display:none"<?php endif; ?>>
        </div>

        <?php if (!isset($model->id)): ?>    
            <br />
        <?php endif; ?>

        <div id="dates" style="display:none;">
            <?php
            foreach ($distributionDaysArray as $idDistribution => $day) {
                if ($day != '--') {
                    echo '<div><span class="date">' . $day . '</span><span class="id_distribution">' . $idDistribution. '</span></div>';
                }
            }
            ?>
        </div>

        <div class="clr"></div>
        <div id="orders-in-progress" style="display:none;">
            <?php foreach ($ordersArray as $order): ?>
                <?php echo '<div class="order" data-iddistribution="' . $order->id_distribution. '" data-id="' . $order->id . '" data-href="' . Yii::$app->urlManager->createUrl(['order/update', 'id' => $order->id, 'id_producer' => $order->distribution->id_producer]) . '"></div>'; ?>
            <?php endforeach; ?>
        </div>
        <div id="has-order-in-progress" style="display:none;" class="alert alert-danger">Vous avez déjà une commande en cours pour cette date. <a href="#">Cliquez ici</a> pour la modifier.</div>

    </div>
    <div class="col-md-6">
        <?php if(strlen($producer->order_infos)): ?>
            <div id="order-infos">
                <?= nl2br(Html::encode($producer->order_infos)) ?>
            </div>    
        <?php endif; ?>
    </div>
    

    <div class="clr"></div>

    <div id="block-points-sale">
        <h3 id="step-point-sale"><span>Points de vente</span></h3>
    <?=
            $form->field($model, 'id_point_sale')
            ->label('')
            ->hiddenInput();
    ?>

    <input type="hidden" id="livraison" value="<?php if (!is_null($distribution) && $distribution->delivery): ?>1<?php else: ?>0<?php endif; ?>" />

    <ul id="points-sale" class="blocks">
        <?php
        
        foreach ($pointsSaleArray as $pointSale) {
            
            $comment = '' ;
            if(isset($pointSale->userPointSale) && is_array($pointSale->userPointSale) && count($pointSale->userPointSale))
            {
                foreach($pointSale->userPointSale as $userPointSale)
                {
                    if($userPointSale->id_user == User::getCurrentId() && strlen($userPointSale->comment))
                    {
                        $comment = '<div class="comment"><span>'.Html::encode($userPointSale->comment).'</span></div>' ;
                    }
                }
            }
            
            $htmlCode = '' ; 
            $dataCode = '0' ;
            $code = '' ;
            if(strlen($pointSale->code))
            {
                if(!isset($model->id_point_sale) || $model->id_point_sale != $pointSale->id)
                {
                    $htmlCode .= '<span class="glyphicon glyphicon-lock"></span> ' ;
                    $dataCode = '1' ;
                }
                else {
                    $code = $pointSale->code ;
                }
            }
            
            echo '<li class="block point-sale point-sale-' . $pointSale->id . '" data-code="'.$dataCode.'" data-credit="'.(int) $pointSale->credit.'"><div class="contenu">' .
            '<span style="display:none;" class="id">' . $pointSale->id . '</span>' .
            '<div class="name">' .$htmlCode. Html::encode($pointSale->name) . '</div>' .
            '<div class="address">à ' . Html::encode($pointSale->locality) . '</div>' .
            $comment .
            '<input type="hidden" name="code_point_sale_'.$pointSale->id.'" value="'.$code.'" />'.
            '</div></li>';
            
        }
        ?>
    </ul>
    
    <div class="clr"></div>
    
    <div id="step-infos-point-sale">
    <?php 
        foreach ($pointsSaleArray as $pointSale) {
            echo '<div class="alert alert-warning infos-point-sale infos-point-sale-'.$pointSale->id.'"><h4>Infos : <span>'.Html::encode($pointSale->name).'</span></h4>' .
                '<div class="jour jour-1">' . $pointSale->getStrInfos('monday') . '</div>' .
                '<div class="jour jour-2">' . $pointSale->getStrInfos('tuesday') . '</div>' .
                '<div class="jour jour-3">' . $pointSale->getStrInfos('wednesday') . '</div>' .
                '<div class="jour jour-4">' . $pointSale->getStrInfos('thursday') . '</div>' .
                '<div class="jour jour-5">' . $pointSale->getStrInfos('friday') . '</div>' .
                '<div class="jour jour-6">' . $pointSale->getStrInfos('saturday') . '</div>' .
                '<div class="jour jour-0">' . $pointSale->getStrInfos('sunday') . '</div>' .
            '</div>' ;
        }
    ?>
    </div>
    
    <div class="clr"></div>
    </div>
    
    <div id="products">
        
        <h3 id="step-products"><span>Produits</span></h3>

        <?php // erreur ?>
        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger"><div class="icon"></div><?= Yii::$app->session->getFlash('error'); ?></div>
        <?php endif; ?>

        <div id="">
            <div class="alert alert-warning unavailable">Produit indisponible pour ce point de vente</div>
            <table class="table table-bordered" id="table-products">
                <thead>
                    <tr>
                        <th class="th-photo">Photo</th>
                        <th class="product">Produit</th>
                        <th class="price-unit">Prix unitaire</th>
                        <th class="column-quantity">Quantité</th>
                        <th class="total">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($productsArray as $product): ?>
                        <?php
                            $quantity = 0;
                            if (isset($selectedProducts[$product->id])) {
                                $quantity = $selectedProducts[$product->id];
                            }
                                
                        ?>
                    
                        <tr class="product-<?php echo $product->id; ?>" data-no-limit="<?php if(!$product->quantity_max):  ?>1<?php else: ?>0<?php endif; ?>" data-quantity-max="<?= $quantity ?>" <?php if (count($availableProducts) && !$availableProducts[$product->id]['active']): ?>style="display:none;"<?php endif; ?>>
                            <td class="td-photo">
                                <?php if (strlen($product->photo) && file_exists(dirname(__FILE__).'/../../web/uploads/' . $product->photo)): ?><a href="<?= Yii::$app->urlManager->getBaseUrl() . '/uploads/' . $product->photo ?>" data-lightbox="product-<?php echo $product->id; ?>"><img class="photo img-rounded" src="<?= Yii::$app->urlManager->getBaseUrl() . '/uploads/' . $product->photo ?>" alt="Photo <?= Html::encode($product->name); ?>" /></a><?php endif; ?>
                            </td>
                            <td class="produit">
                                <span class="name"><?= Html::encode($product->name); ?></span> - <span class="description"><?= Html::encode($product->getDescription()); ?></span><br />
                                <span class="recipe"><?= Html::encode($product->recipe); ?></span>
                            </td>
                            <td class="price-unit">
                                <span class="price"><?= Price::format($product->price); ?></span> €
                            </td>
                            <td class="column-quantity">
                                <div class="input-group" <?php if (isset($availableProducts[$product->id]) && $availableProducts[$product->id]['quantity_remaining'] == 0 && $quantity == 0): ?>style="display:none;"<?php endif; ?>>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default move-quantity minus">-</button>
                                    </span>
                                    <input type="text" value="<?php if (isset($selectedProducts[$product->id])): echo $selectedProducts[$product->id];
                                    else: ?>0<?php endif; ?>" readonly name="Product[product_<?php echo $product->id; ?>]" class="quantity form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default move-quantity plus">+</button>
                                    </span>
                                </div>

                                <div class="quantity-remaining">Reste <span class="nb"><?php if (isset($availableProducts[$product->id])): echo $availableProducts[$product->id]['quantity_remaining'] + $quantity;
                                    endif; ?></span> <?php echo Html::encode(strtolower($product->name)); ?>(s)
                                </div>
                                
                                <div class="unavailable">Épuisé</div>
                            </td>
                            <td class="total"><strong></strong></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>                        
                        <td id="total-order"><strong></strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>

    <?php if($idProducer): ?>
    <?php
        $producer = Producer::findOne($idProducer);
    ?>
    <div id="bar-fixed" class="<?php if($producer->credit): ?>credit<?php else: ?>no-credit<?php endif; ?>">
        <div class="container">
            <?php if (isset($model->id) && !$model->date_delete): ?>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['order/cancel', 'id' => $model->id]); ?>" class="btn btn-danger cancel-order">Annuler ma commande</a>
            <?php endif; ?>
            <span id="total-order-bottom"><span></span> €</span>
            <?= Html::submitButton('<span class="glyphicon glyphicon-comment"></span> Commentaire', ['class' => 'btn btn-default btn-comment', 'data-placement' => 'top', 'data-toggle' => 'tooltip', 'data-original-title' => 'Ajouter un commentaire']) ?>
            
                <?php
                    if($producer->credit):
                        $linkCredit = '<a class="info-credit" href="'.Yii::$app->urlManager->createUrl(['site/credit']) .'" data-toggle="tooltip" data-placement="bottom" title="En savoir plus sur le Crédit"><span class="glyphicon glyphicon-info-sign"></span></a>' ; ;
                ?>
                <div id="checkbox-credit" >
                    <?php if($credit || $model->getAmount(Order::AMOUNT_PAID)): ?>
                        <?= Html::checkbox('credit', true, ['label' => 'Utiliser mon Crédit <span class="the-credit" data-toggle="tooltip" data-placement="top" data-original-title="Vous avez actuellement '.number_format($credit,2).' € sur votre compte Crédit">'.number_format($credit,2).'&nbsp;€</span><br /><span class="info"></span>']) ?>
                        <?= Html::hiddenInput('amount_credit', $credit, ['id' => 'amount-credit']) ?>
                        <?= Html::hiddenInput('str_amount_credit', number_format($credit,2).' €', ['id' => 'str-amount-credit']) ?>
                    <?php else: ?>
                        <div id="info-credit-empty">
                            Votre compte Crédit est vide <?= $linkCredit ?>
                        </div>
                    <?php endif; ?>
                    <div id="credit-disabled">Le Crédit est désactivé<br /> pour ce point de vente <?= $linkCredit ?></div>
                </div>
                <div class="clr"></div>
                <?php endif; ?>
            
                <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'placeholder' => 'Un commentaire ?'])->label(''); ?>
                <div id="block-confirm-order">
                    <?php if($model->date_delete): $strButtonConfirmOrder = 'Réactiver ma commande' ; else: $strButtonConfirmOrder = 'Valider ma commande' ; endif; ?>
                    <?= Html::submitButton('<span class="glyphicon glyphicon-ok"></span> '.$strButtonConfirmOrder, ['class' => 'btn btn-primary confirm-order']) ?>
                </div>
            <?php endif; ?>            
        </div>
    </div>
            
    <?php
    // id_etablissement
    endif; ?>
    
<?php ActiveForm::end(); ?>
    
    
<!-- modal code point de vente -->
<div class="modal fade" id="modal-code" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Code d'accès</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-warning">
            Ce point de vente nécessite un code d'accès.
        </div>
        <form action="index.php?r=order/validate-code-point-sale" method="post">
            <input type="hidden" value="" name="idPointSale" id="id-point-sale" />
            <div class="form-group field-code required">
                <label class="control-label" for="code">Code d'accès :</label>
                <input type="password" class="form-control" id="code" name="code" />
                <p class="help-block help-block-error" style="display:none;">Code incorrect</p>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Valider</button>        
            </div>
        </form>
    </div>
  </div>
</div>
</div>    
    

</div><!-- commande-form -->
