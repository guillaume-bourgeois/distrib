<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\bootstrap\ActiveForm;
use common\models\Order ;

$this->setTitle('Mes commandes') ;
$this->addButton(['label' => '<span class="glyphicon glyphicon-plus"></span> Ajouter', 'url' => 'order/order', 'class' => 'btn btn-primary']) ;

?>

<div id="index-order">

    <?php if($orderOk): ?>
            <div class="alert alert-success">
                    <div class="icon"></div>
                    Votre commande a bien été prise en compte.
            </div>
    <?php endif; ?>

    <?php if($cancelOk): ?>
            <div class="alert alert-success"><div class="icon"></div>Votre commande a bien été annulée.</div>
    <?php endif; ?>

<ul id="tabs-orders-history" class="nav nav-tabs" role="tablist">
    <li class="<?php if($type == 'incoming'): ?>active<?php endif; ?>">
        <a href="<?= Yii::$app->urlManagerProducer->createUrl(['order/history', 'type' => 'incoming']); ?>">
            À venir
            <span class="label <?php if($countIncoming): ?>label-success<?php else: ?>label-default<?php endif; ?>"><?= $countIncoming ?></span>
        </a>
    </li>
    <li class="<?php if($type == 'passed'): ?>active<?php endif; ?>">
        <a href="<?= Yii::$app->urlManagerProducer->createUrl(['order/history', 'type' => 'passed']); ?>">
            Passées
            <span class="label label-default"><?= $countPassed ?></span>
        </a>
    </li>
</ul>
            
<?= 

GridView::widget([
    'dataProvider' => $dataProviderOrders,
    'summary' => '',
    'beforeRow' => function($model) {
        $model->init() ;
    },
    'columns' => [
        [
            'attribute' => 'distribution.date',
            'label' => 'Date de livraison',
            'format' => 'raw',
            'value' => function($model) {
                return $model->getBlockDate() ;
            }
        ],
        [
            'label' => 'Point de vente',
            'format' => 'raw',
            'value' => function($model) {
                return $model->getPointSaleSummary(); 
            }
        ],
        [
            'label' => 'Résumé',
            'format' => 'raw',
            'value' => function($model) {
                return $model->getCartSummary(); 
            }
        ],
        [
            'label' => 'Montant',
            'format' => 'raw',
            'value' => function($model) {
                return $model->getAmountSummary(); 
            }
        ],
        [
            'label' => 'Statut',
            'format' => 'raw',
            'value' => function($c) {
                $html = '' ;
            
                if($c->date_delete) {
                    $html .= '<span class="label label-danger">Annulée</span><br />' ;
                }
                else {
                    if($c->getState() == Order::STATE_DELIVERED) {
                        $html .= '<span class="label label-success">Livrée</span>' ;
                    }
                    elseif($c->getState() == Order::STATE_PREPARATION) {
                        $html .= '<span class="label label-warning">En préparation</span>' ;
                    }
                    elseif($c->getState() == Order::STATE_OPEN) {
                        $html .= '<span class="label label-default">À venir</span>' ;
                    }
                }
                
                return $html ;
            }
        ],
        [
            'label' => 'Actions',
            'format' => 'raw',
            'value' => function($c) {
                $html = '' ;
                if($c->getState() == Order::STATE_OPEN) {

                    $html .= '<a href="'.Yii::$app->urlManager->createUrl(['order/order','id'=>$c->id]).'" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a> '.
                            '<a href="'.Yii::$app->urlManager->createUrl(['order/cancel','id'=>$c->id]).'" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a> '.
                            (($c->id_subscription) ? '<a href="'.Yii::$app->urlManagerProducer->createUrl(['subscription/form','id'=>$c->id_subscription]).'" class="btn btn-default"><span class="glyphicon glyphicon-repeat"></span></a>' : '') ;
                            
                }
                return $html ;
            }
        ]
    ]
]);
                    
?>
