<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use common\models\Order ;
use common\helpers\GlobalParam ;

$this->setTitle('Confirmation de commande') ;
$producer = GlobalParam::getCurrentProducer() ;

?>

<div id="order-success">

    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok glyphicon-big"></span>
        <div class="content">
            <h3>Votre commande a bien été prise en compte</h3>
            <?php if(!Yii::$app->user->isGuest): ?>
                <a href="<?= Yii::$app->urlManagerProducer->createUrl(['order/history']) ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    Voir toutes mes commandes
                </a>
            <?php endif; ?>
        </div>
        <div class="clr"></div>
    </div>
    <div class="alert alert-info alert-order-summary">
        <span class="glyphicon glyphicon-list-alt glyphicon-big"></span>
        <div class="content">
            <h3>Récapitulatif de votre commande</h3>

            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <?php if($order->reference && strlen($order->reference) > 0): ?><li><span class="glyphicon glyphicon-check"></span> Commande N°<strong><?= Html::encode($order->reference); ?></strong></li><?php endif; ?>
                        <li><span class="glyphicon glyphicon-time"></span>Le <?= date('d/m/Y',strtotime($order->distribution->date)) ?></li>
                        <li><span class="glyphicon glyphicon-map-marker"></span>
                                <?php if($order->delivery_home): ?>
                                    Livraison à domicile
                                    <br /><span class="locality"><?= nl2br(Html::encode($order->delivery_address)); ?></span>
                                <?php else: ?>
                                    <?=  Html::encode($order->pointSale->name) ?><?php if(strlen($order->pointSale->name)): ?>
                                    <br />&nbsp; &nbsp; &nbsp;<span class="locality">à <?= Html::encode($order->pointSale->locality) ?></span><?php endif; ?></li>
                                <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>
                            <p><strong>Produit<?php if($order->countProducts() > 1): ?>s<?php endif; ?></strong></p>
                            <p><?= $order->getCartSummary() ?></p>
                        </li>
                        <li>Total : <strong><?= $order->getAmountWithTax(Order::AMOUNT_TOTAL, true); ?></strong></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="clr"></div>
    </div>

        <?php if($producer->option_payment_info && strlen($producer->option_payment_info) > 0): ?>
                <div class="panel panel-warning payment-info">
                        <div class="panel-heading">
                                <h3>Paiement de votre commande</h3>
                        </div>
                        <div class="panel-body">
                                <?= nl2br($producer->option_payment_info); ?>
                        </div>
                </div>
        <?php endif; ?>

        <?php if ($producer->order_infos && strlen($producer->order_infos) > 0): ?>
                <div class="panel panel-info">
                        <div class="panel-heading">
                                <h3>Informations</h3>
                        </div>
                        <div class="panel-body">
                                <?= nl2br($producer->order_infos); ?>
                        </div>
                </div>
        <?php endif; ?>
</div>