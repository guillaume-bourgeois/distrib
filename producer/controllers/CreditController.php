<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace producer\controllers;

use common\models\CreditHistory ;
use producer\models\CreditForm ;
use common\models\User ;

class CreditController extends ProducerBaseController 
{
    
    /**
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [];
    }
   
    public function actions() 
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',                
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Affiche l'historique du crédit client.
     * 
     */
    public function actionHistory($returnPayment = '') 
    {
        $searchModel = new CreditHistorySearch() ;
        $searchModel->id_user = User::getCurrentId() ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       
        $userProducer = UserProducer::searchOne([
            'id_producer' => $this->getProducer()->id,
            'id_user' => User::getCurrentId()
        ]) ;
        
        if(strlen($returnPayment)) {
            if($returnPayment == 'success') {
                Yii::$app->getSession()->setFlash('success', "Paiement accepté : votre compte vient d'être crédité.");
            }
            if($returnPayment == 'cancel') {
                Yii::$app->getSession()->setFlash('error', 'Paiement annulé');
            }
        }
        
        return $this->render('history',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'creditUser' => $userProducer->credit
        ]) ;                    
    }
    
    public function actionAdd() 
    {
        $producer = $this->getProducer() ;
        
        if($producer->online_payment) {
            \Payplug\Payplug::setSecretKey($producer->getSecretKeyPayplug());
        
            $creditForm = new CreditForm ;

            if ($creditForm->load(Yii::$app->request->post()) && $creditForm->validate()) {
                $user = User::getCurrent() ;

                $payment = \Payplug\Payment::create(array(
                    'amount'           => (float) $creditForm->amount * 100,
                    'currency'         => 'EUR',
                    'save_card'        => false,
                    'customer'         => array(
                      'email'          => $user->email,
                      'first_name'     => $user->name,
                      'last_name'      => $user->lastname
                    ),
                    'hosted_payment'   => array(
                        'return_url'     => Yii::$app->urlManagerProducer->createAbsoluteUrl(['credit/history', 'slug_producer' => $producer->slug, 'returnPayment' => 'success']),
                        'cancel_url'     => Yii::$app->urlManagerProducer->createAbsoluteUrl(['credit/history', 'slug_producer' => $producer->slug, 'returnPayment' => 'cancel']),
                    ),
                    'notification_url' => Yii::$app->urlManagerProducer->createAbsoluteUrl(['credit/payment-notifications', 'idUser' => $user->id, 'slug_producer' => $producer->slug]),
                    'metadata'         => array(
                        'id_user'    => $user->id
                    )
                ));

                if($payment) {
                    $this->redirect($payment->hosted_payment->payment_url) ;
                }
            }

            return $this->render('add', [
                'creditForm' => $creditForm
            ]) ;
        }
        else {
            throw new \yii\base\UserException('Cette option est désactivée chez ce producteur.');
        }
    }
    
    /**
     * Interface de notification suite aux actions effectuées sur Payplug.
     * 
     * @param integer $idUser
     */
    public function actionPaymentNotifications($idUser)
    {
        $producer = $this->getProducer() ;
        \Payplug\Payplug::setSecretKey($producer->getSecretKeyPayplug());

        $input = file_get_contents('php://input');
        try {
            $resource = \Payplug\Notification::treat($input);

            if ($resource instanceof \Payplug\Resource\Payment
              && $resource->is_paid 
              ) {
                
                $creditHistory = new CreditHistory;
                $creditHistory->id_user = $idUser;
                $creditHistory->id_user_action = $idUser;
                $creditHistory->id_producer = $producer->id ;
                $creditHistory->type = CreditHistory::TYPE_CREDIT ;
                $creditHistory->comment = '' ;
                $creditHistory->amount = $resource->amount ;
                $creditHistory->mean_payment = MeanPayment::CREDIT_CARD ;
                $creditHistory->save();
              
            } else if ($resource instanceof \Payplug\Resource\Refund) {
              echo 'refund' ;
            }
        }
        catch (\Payplug\Exception\PayplugException $exception) {
            echo 'error' ;
        }
    }
    
}