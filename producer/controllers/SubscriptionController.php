<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace producer\controllers;

use common\helpers\Debug;
use common\helpers\GlobalParam;
use common\models\SubscriptionForm;
use common\models\SubscriptionSearch;
use common\models\Product;

class SubscriptionController extends ProducerBaseController
{
        var $enableCsrfValidation = false;

        public function behaviors()
        {
                return [
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                        ]
                                ],
                        ],
                ];
        }

        /**
         * Liste les commandes récurrente du producteur.
         *
         * @return string
         */
        public function actionIndex()
        {
                $searchModel = new SubscriptionSearch;
                $searchModel->id_user = User::getCurrentId();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
                ]);
        }

        public function actionAjaxProcess()
        {
                // form
                $model = new SubscriptionForm;
                $model->id_producer = GlobalParam::getCurrentProducerId();
                $model->id_user = User::getCurrentId();

                $posts = Yii::$app->request->post();

                $idSubscription = (int)$posts['idSubscription'];
                $isUpdate = false;
                if ($idSubscription) {
                        $subscription = Subscription::findOne($idSubscription);
                        if ($subscription) {
                                $model->id = $idSubscription;
                                $isUpdate = true;
                        }
                }

                if ($model->load($posts) && $model->validate()
                        && $model->save()) {
                        $subscription = Subscription::searchOne([
                                'id' => $model->id
                        ]);
                        $subscription->updateIncomingDistributions($isUpdate);

                        if ($isUpdate) {
                                Yii::$app->getSession()->setFlash('success', 'Abonnement modifié');
                        } else {
                                Yii::$app->getSession()->setFlash('success', 'Abonnement ajouté');
                        }
                }
        }

        /**
         * Crée une commande récurrente.
         *
         * @return string
         */
        public function actionForm($id = 0)
        {
                return $this->render('form', [
                        'idSubscription' => (int)$id
                ]);
        }

        /**
         * Modifie une commande récurrente.
         *
         * @param integer $id
         * @return string
         * @throws NotFoundHttpException
         */
        public function actionUpdate($id)
        {
                // form
                $model = new SubscriptionForm;
                $subscription = Subscription::findOne($id);
                if ($subscription) {
                        $model->id = $id;
                        $model->id_producer = $subscription->id_producer;
                        $model->id_user = $subscription->id_user;
                        $model->username = $subscription->username;
                        $model->id_point_sale = $subscription->id_point_sale;
                        $model->date_begin = date('d/m/Y', strtotime($subscription->date_begin));
                        if (strlen($subscription->date_end)) {
                                $model->date_end = date('d/m/Y', strtotime($subscription->date_end));
                        }

                        $model->monday = $subscription->monday;
                        $model->tuesday = $subscription->tuesday;
                        $model->wednesday = $subscription->wednesday;
                        $model->thursday = $subscription->thursday;
                        $model->friday = $subscription->friday;
                        $model->saturday = $subscription->saturday;
                        $model->sunday = $subscription->sunday;
                        $model->auto_payment = $subscription->auto_payment;
                        $model->week_frequency = $subscription->week_frequency;

                        // produits
                        $arrayProductsSubscription = ProductSubscription::searchAll([
                                'id_subscription' => $model->id
                        ]);

                        foreach ($arrayProductsSubscription as $productSubscription) {
                                $model->products['product_' . $productSubscription->id_product] = $productSubscription->quantity;
                        }
                } else {
                        throw new NotFoundHttpException('L\'abonnement est introuvable.', 404);
                }

                // produits
                $productsArray = Product::searchAll();

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                        if (!strlen($model->date_end)) {
                                $model->date_end = null;
                        }
                        if ($model->save()) {
                                Yii::$app->getSession()->setFlash('success', 'Abonnement modifié');

                                $subscription = Subscription::findOne($model->id);
                                $matchedDistributionsArray = $subscription->searchMatchedIncomingDistributions();
                                if (count($matchedDistributionsArray)) {
                                        return $this->redirect(['subscription/update-distributions', 'idSubscription' => $subscription->id, 'update' => true]);
                                } else {
                                        return $this->redirect(['subscription/index']);
                                }
                        }
                }

                return $this->render('update', [
                        'model' => $model,
                        'productsArray' => $productsArray
                ]);
        }

        /**
         * Supprime un abonnement
         *
         * @param integer $id
         */
        public function actionDelete($id)
        {
                $subscription = Subscription::searchOne([
                        'id' => $id
                ]);
                ProductSubscription::deleteAll(['id_subscription' => $id]);
                $subscription->deleteOrdersIncomingDistributions();
                $subscription->delete();
                Yii::$app->getSession()->setFlash('success', 'Abonnement supprimé');
                return $this->redirect(['subscription/index']);
        }

        public function actionAjaxInfos($idSubscription = 0)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $params = [];

                $user = User::getCurrent() ;
                $userProducer = UserProducer::searchOne([
                        'id_user' => User::getCurrentId()
                ]) ;
                $pointSale = false ;

                if ($idSubscription > 0) {
                        $arrayProductsSubscription = ProductSubscription::searchAll([
                                'id_subscription' => $idSubscription
                        ]);

                        $subscription = Subscription::findOne($idSubscription) ;

                        if($subscription) {
                                if($subscription->id_point_sale) {
                                        $pointSale = PointSale::findOne($subscription->id_point_sale) ;
                                }
                        }
                }

                // Produits
                $productsArray = Product::searchAll();
                $indexProduct = 0;
                foreach ($productsArray as &$product) {

                        $quantity = 0;
                        $coefficientUnit = Product::$unitsArray[$product->unit]['coefficient'];
                        if (isset($arrayProductsSubscription) && count($arrayProductsSubscription)) {
                                foreach ($arrayProductsSubscription as $productSubscription) {
                                        if ($product->id == $productSubscription->id_product) {
                                                $coefficientUnit = Product::$unitsArray[$productSubscription->product->unit]['coefficient'];
                                                $quantity = $productSubscription->quantity * $coefficientUnit;
                                        }
                                }
                        }

                        $product = array_merge(
                                $product->getAttributes(),
                                [
                                        'index' => $indexProduct++,
                                        'quantity_form' => $quantity,
                                        'coefficient_unit' => $coefficientUnit,
                                        'wording_unit' => Product::strUnit($product->unit, 'wording_unit', true),
                                        'wording_short' => Product::strUnit($product->unit, 'wording_short'),
                                        'price_with_tax' => $product->getPriceWithTax([
                                                'user' => $user,
                                                'user_producer' => $userProducer,
                                                'point_sale' => $pointSale
                                        ]),
                                ]
                        );
                }

                $params['products'] = $productsArray;

                $pointsSaleArray = PointSale::searchAll();
                foreach ($pointsSaleArray as &$pointSale) {
                        $pointSale = array_merge($pointSale->getAttributes(), [
                                'userPointSale' => ($pointSale->userPointSale ? $pointSale->userPointSale[0] : '')
                        ]);
                        if ($pointSale['code'] && strlen($pointSale['code'])) {
                                $pointSale['code'] = '***';
                        }
                }

                $params['points_sale'] = $pointsSaleArray;

                if ($idSubscription > 0) {
                        $subscription = Subscription::searchOne([
                                'id' => $idSubscription
                        ]);

                        if (!$subscription || $subscription->id_user != User::getCurrentId()) {
                                throw new UserException('Abonnement introuvable');
                        } else {
                                $params = array_merge($params, $subscription->getAttributes());
                        }
                }

                return $params;
        }

        /**
         * Vérifie le code saisi pour un point de vente.
         *
         * @param integer $idPointSale
         * @param string $code
         * @return boolean
         */
        public function actionAjaxValidateCodePointSale($idPointSale, $code)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $pointSale = PointSale::findOne($idPointSale);
                if ($pointSale) {
                        if ($pointSale->validateCode($code)) {
                                return 1;
                        }
                }
                return 0;
        }
}
