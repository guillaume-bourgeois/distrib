<?php
/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\helpers\Debug;
use common\helpers\GlobalParam;
use common\models\Order ;
use common\models\Product ;
use common\models\SubscriptionSearch ;

class SubscriptionController extends BackendController 
{
    var $enableCsrfValidation = false;

    public function behaviors() 
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend() ;
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Liste les commandes récurrente du producteur.
     * 
     * @return string
     */
    public function actionIndex() 
    {
        $this->checkProductsPointsSale() ;
        
        $searchModel = new SubscriptionSearch ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Crée un abonnement.
     * 
     * @return string
     */
    public function actionCreate($idOrder = 0) 
    {   
        // form
        $model = new SubscriptionForm;
        $model->isAdmin = true ;
        $model->id_producer = GlobalParam::getCurrentProducerId();

        if($idOrder) {
            $order = Order::searchOne(['id' => $idOrder]);
            if ($order) {
                $model->id_user = $order->id_user;
                $model->username = $order->username;
                $model->id_point_sale = $order->id_point_sale;
                $model->date_begin = date('d/m/Y') ;
                $dateDay = strtolower(date('l',strtotime($order->distribution->date))) ;
                $model->$dateDay = 1 ;
                $model->week_frequency = 1 ;
                if($model->id_user && Producer::getConfig('credit')) {
                    $model->auto_payment = 1 ;
                }
                
                // produits
                foreach ($order->productOrder as $productOrder) {
                    $model->products['product_' . $productOrder->id_product] = $productOrder->quantity;
                }
            } else {
                throw new NotFoundHttpException('La commande est introuvable.', 404);
            }
        }
        
        // produits
        $productsArray = Product::searchAll([], [
            'orderby' => 'product.order ASC'
        ]) ;
                
        if ($model->load(Yii::$app->request->post()) && $model->validate() 
            && $model->save()) 
        {
            Yii::$app->getSession()->setFlash('success', 'Abonnement ajouté');
            
            $subscription = Subscription::findOne($model->id) ;
            $matchedDistributionsArray = $subscription->searchMatchedIncomingDistributions() ;
            if(count($matchedDistributionsArray)) {
                return $this->redirect(['subscription/update-distributions', 'idSubscription' => $subscription->id]);
            }
            else {
                return $this->redirect(['subscription/index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'productsArray' => $productsArray
        ]);
    }

    /**
     * Modifie un abonnement.
     * 
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id) 
    {
        // form
        $model = new SubscriptionForm;
        $model->isAdmin = true ;
        $subscription = Subscription::findOne($id);
        
        if ($subscription) {
            $model->id = $id;
            $model->id_producer = $subscription->id_producer;
            $model->id_user = $subscription->id_user;
            $model->username = $subscription->username;
            $model->id_point_sale = $subscription->id_point_sale;
            $model->date_begin = date('d/m/Y', strtotime($subscription->date_begin));
            if (strlen($subscription->date_end)) {
                $model->date_end = date('d/m/Y', strtotime($subscription->date_end));
            }
            
            $model->monday = $subscription->monday;
            $model->tuesday = $subscription->tuesday;
            $model->wednesday = $subscription->wednesday;
            $model->thursday = $subscription->thursday;
            $model->friday = $subscription->friday;
            $model->saturday = $subscription->saturday;
            $model->sunday = $subscription->sunday;
            $model->auto_payment = $subscription->auto_payment;
            $model->week_frequency = $subscription->week_frequency;
            if(strlen($subscription->comment)) {
                $model->comment = $subscription->comment ;
            }

            // produits
            $arrayProductsSubscription = ProductSubscription::searchAll([
                'id_subscription' => $model->id
            ]) ;
            
            foreach ($arrayProductsSubscription as $productSubscription) {
                $model->products['product_' . $productSubscription->id_product] = $productSubscription->quantity;
            }
        } else {
            throw new NotFoundHttpException('L\'abonnement est introuvable.', 404);
        }

        // produits
        $productsArray = Product::searchAll([], [
            'orderby' => 'product.order ASC'
        ]) ;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!strlen($model->date_end)) {
                $model->date_end = null;
            }
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Abonnement modifié');
                
                $subscription = Subscription::findOne($model->id) ;
                $matchedDistributionsArray = $subscription->searchMatchedIncomingDistributions() ;
                if(count($matchedDistributionsArray)) {
                    return $this->redirect(['subscription/update-distributions', 'idSubscription' => $subscription->id,'update' => true]);
                }
                else {
                    return $this->redirect(['subscription/index']);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'productsArray' => $productsArray
        ]);
    }

    /**
     * Supprime une commande récurrente.
     * 
     * @param integer $id
     */
    public function actionDelete($id) 
    {
        $subscription = Subscription::searchOne([
            'subscription.id' => $id
        ]) ;
        $subscription->deleteOrdersIncomingDistributions() ;
        $subscription->delete();
        ProductSubscription::deleteAll(['id_subscription' => $id]);
        Yii::$app->getSession()->setFlash('success', 'Abonnement supprimé');
        return $this->redirect(['subscription/index']);
    }

    public function actionUpdateDistributions($idSubscription, $generate = false, $update = false) 
    {
        $subscription = Subscription::findOne($idSubscription) ;
        $matchedDistributionsArray = $subscription->searchMatchedIncomingDistributions() ;
        
        if($generate) {
            if($update) {
                $subscription->deleteOrdersIncomingDistributions() ;
            }
            foreach($matchedDistributionsArray as $distribution) {
                $subscription->add($distribution->date) ;
            }
            Yii::$app->getSession()->setFlash('success', 'Commandes '.($update ? 're-' : '').'générées dans les distributions futures.');
            return $this->redirect(['subscription/index']) ;
        }
        
        return $this->render('update_distributions',[
            'matchedDistributionsArray' => $matchedDistributionsArray,
            'idSubscription' => $idSubscription,
            'update' => $update
        ]) ;
    }
    
    public function actionAjaxInfos($idSubscription = 0) 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $productsQuery = Product::find()
                ->where(['id_producer' => GlobalParam::getCurrentProducerId(),]) ;
        
        if($idSubscription) {
            $productsQuery->joinWith(['productSubscription' => function($query) use($idSubscription) {
                $query->andOnCondition('product_subscription.id_subscription = '.((int) $idSubscription)) ;
            }]) ;
        }
        
        $productsArray = $productsQuery->asArray()->orderBy('order ASC')->all() ;
        /*Debug::dump($productsArray);*/
        foreach($productsArray as &$theProduct) {
            /*$theProduct['unit_save'] = $theProduct['unit'] ;
            $theProduct['units'] = [] ;
            $theProduct['units'][] = [
                'unit' => $theProduct['unit'],

                'step' => $theProduct['step'],
                'price' => $theProduct['price']
            ] ;*/

            $theProduct['wording_unit'] = Product::strUnit($theProduct['unit'], 'wording_short');

            if(isset($theProduct['productSubscription'][0])) {
                /*if($theProduct['productSubscription'][0]['unit'] != $theProduct['unit']) {
                    $theProduct['units'][] = [
                        'unit' => $theProduct['productSubscription'][0]['unit'],
                        'wording_unit' => Product::strUnit($theProduct['productSubscription'][0]['unit'], 'wording_short'),
                        'step' => $theProduct['productSubscription'][0]['step'],
                        'price' => $theProduct['productSubscription'][0]['price'],
                    ] ;
                    $theProduct['unit'] = $theProduct['productSubscription'][0]['unit'] ;
                    $theProduct['step'] = $theProduct['productSubscription'][0]['step'] ;
                    $theProduct['price'] = $theProduct['productSubscription'][0]['price'] ;
                }*/
                $theProduct['quantity'] = $theProduct['productSubscription'][0]['quantity'] * Product::$unitsArray[$theProduct['unit']]['coefficient'] ;
            }
            else {
                $theProduct['quantity'] = '' ;
            }
        }
        
        return [
            'products' => $productsArray
        ] ;
    }
            
    
}
