<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use common\helpers\GlobalParam;
use common\models\UserGroup;
use common\models\UserUserGroup;
use Yii;
use yii\filters\AccessControl;
use common\models\PointSale;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\UserPointSale;
use common\models\Order;
use common\models\Producer;
use common\models\Distribution;
use yii\helpers\Html;

/**
 * PointVenteController implements the CRUD actions for PointVente model.
 */
class UserGroupController extends BackendController
{

        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::hasAccessBackend();
                                                }
                                        ],
                                ],
                        ],
                ];
        }

        /**
         * Liste les points de vente.
         *
         * @return mixed
         */
        public function actionIndex()
        {
                $searchModel = new UserGroupSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                ]);
        }

        /**
         * Crée un groupe d'utilisateur.
         *
         * @return mixed
         */
        public function actionCreate()
        {
                $model = new UserGroup();

                $model->id_producer = GlobalParam::getCurrentProducerId() ;

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        Yii::$app->getSession()->setFlash('success', "Groupe d'utilisateur ajouté.");
                        return $this->redirect(['index']);
                }
                else {
                        return $this->render('create', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Modifie un groupe d'utilisateur.
         *
         * @param integer $id
         * @return mixed
         */
        public function actionUpdate($id)
        {
                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        Yii::$app->getSession()->setFlash('success', "Groupe d'utilisateur modifié.");
                        return $this->redirect(['index']);
                }
                else {
                        return $this->render('update', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Supprime un groupe d'utilisateur.
         *
         * @param integer $id
         * @return mixed
         */
        public function actionDelete($id)
        {
                $userGroup = $this->findModel($id);

                $userGroup->delete();
                UserUserGroup::deleteAll(['id_user_group' => $id]);
                Yii::$app->getSession()->setFlash('success', 'Groupe d\'utilisateur <strong>' . Html::encode($userGroup->name) . '</strong> supprimé.');

                return $this->redirect(['index']);
        }

        /**
         * Recherche un groupe d'utilisateur en fonction de son ID.
         *
         * @param integer $id
         * @return UserGroup
         * @throws NotFoundHttpException si le modèle n'est pas trouvé
         */
        protected function findModel($id)
        {
                if (($model = UserGroup::findOne($id)) !== null) {
                        return $model;
                } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
        }

}
