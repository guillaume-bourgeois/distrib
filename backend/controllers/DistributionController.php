<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use backend\controllers\BackendController;
use common\helpers\GlobalParam;
use common\models\DeliveryNote;
use common\models\Distribution;
use common\models\Document;
use common\models\PointSale;
use common\models\Product;
use common\models\Producer;
use common\models\Order;
use common\models\ProductCategory;
use common\models\User;
use common\models\Subscription;
use common\helpers\Price;
use common\models\PointSaleDistribution;
use common\models\UserProducer;
use DateTime;

class DistributionController extends BackendController
{

        public function behaviors()
        {
                return [
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'actions' => ['report-cron', 'report-terredepains'],
                                                'allow' => true,
                                                'roles' => ['?']
                                        ],
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::getCurrentStatus() == USER::STATUS_ADMIN
                                                                || User::getCurrentStatus() == USER::STATUS_PRODUCER;
                                                }
                                        ]
                                ],
                        ],
                ];
        }

        public function actionIndex($date = '')
        {
                $this->checkProductsPointsSale();

                $format = 'Y-m-d';
                $theDate = '';
                $dateObject = DateTime::createFromFormat($format, $date);

                if ($dateObject && $dateObject->format($format) === $date) {
                        $theDate = $date;
                }

                return $this->render('index', [
                        'date' => $theDate
                ]);
        }

        public function actionAjaxInfos($date = '')
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $json = [
                        'distribution' => [],
                        'products' => []
                ];

                $format = 'Y-m-d';
                $dateObject = DateTime::createFromFormat($format, $date);

                $producer = GlobalParam::getCurrentProducer();
                $json['producer'] = [
                        'credit' => $producer->credit,
                        'tiller' => $producer->tiller,
                        'option_display_export_grid' => $producer->option_display_export_grid
                ];

                $json['means_payment'] = MeanPayment::getAll();

                $distributionsArray = Distribution::searchAll([
                        'active' => 1
                ], [
                        'conditions' => ['date > :date_begin', 'date < :date_end'],
                        'params' => [':date_begin' => date('Y-m-d', strtotime('-1 month')), ':date_end' => date('Y-m-d', strtotime('+3 month')),],
                ]);
                $json['distributions'] = $distributionsArray;

                if ($dateObject && $dateObject->format($format) === $date) {
                        // distribution
                        $distribution = Distribution::initDistribution($date);
                        $json['distribution'] = [
                                'id' => $distribution->id,
                                'active' => $distribution->active,
                                'url_report' => Yii::$app->urlManagerBackend->createUrl(['distribution/report', 'date' => $distribution->date]),
                                'url_report_grid' => Yii::$app->urlManagerBackend->createUrl(['distribution/report-grid', 'date' => $distribution->date]),
                        ];

                        // commandes
                        $ordersArray = Order::searchAll([
                                'distribution.id' => $distribution->id,
                        ], [
                                'orderby' => 'user.lastname ASC, user.name ASC'
                        ]);

                        // montant et poids des commandes
                        $revenues = 0;
                        $weight = 0;
                        if ($ordersArray) {
                                foreach ($ordersArray as $order) {
                                        if (is_null($order->date_delete)) {
                                                $revenues += $order->getAmountWithTax();
                                                $weight += $order->weight;
                                        }
                                }
                        }
                        $json['distribution']['revenues'] = Price::format($revenues);
                        $json['distribution']['weight'] = number_format($weight, 2);

                        // products
                        $productsArray = Product::find()
                                ->orWhere(['id_producer' => GlobalParam::getCurrentProducerId(),])
                                ->joinWith(['taxRate','productDistribution' => function ($query) use ($distribution) {
                                        $query->andOnCondition('product_distribution.id_distribution = ' . $distribution->id);
                                }])
                                ->orderBy('product_distribution.active DESC, order ASC')
                                ->asArray()
                                ->all();

                        $potentialRevenues = 0;
                        $potentialWeight = 0;

                        foreach ($productsArray as &$theProduct) {

                                $quantityOrder = Order::getProductQuantity($theProduct['id'], $ordersArray);
                                $theProduct['quantity_ordered'] = $quantityOrder;

                                if (!isset($theProduct['productDistribution'][0])) {
                                        $theProductObject = (object)$theProduct;
                                        $theProduct['productDistribution'][0] = $distribution->linkProduct($theProductObject);
                                }

                                if (!is_numeric($theProduct['productDistribution'][0]['quantity_max'])) {
                                        $theProduct['quantity_remaining'] = null;
                                } else {
                                        $theProduct['quantity_remaining'] = $theProduct['productDistribution'][0]['quantity_max'] - $quantityOrder;
                                }

                                $theProduct['quantity_form'] = 0;

                                if ($theProduct['productDistribution'][0]['active'] && $theProduct['productDistribution'][0]['quantity_max']) {
                                        $potentialRevenues += $theProduct['productDistribution'][0]['quantity_max'] * $theProduct['price'];
                                        $potentialWeight += $theProduct['productDistribution'][0]['quantity_max'] * $theProduct['weight'] / 1000;
                                }

                                if(!isset($theProduct['taxRate'])) {
                                        $theProduct['taxRate'] = $producer->taxRate ;
                                }
                        }

                        $json['distribution']['potential_revenues'] = Price::format($potentialRevenues);
                        $json['distribution']['potential_weight'] = number_format($potentialWeight, 2);

                        $json['products'] = $productsArray;

                        // orders as array
                        $ordersArrayObject = $ordersArray ;
                        if ($ordersArray) {
                                foreach ($ordersArray as &$order) {
                                        $productOrderArray = [];
                                        foreach ($order->productOrder as $productOrder) {
                                                $productOrderArray[$productOrder->id_product] = [
                                                        'quantity' => $productOrder->quantity * Product::$unitsArray[$productOrder->unit]['coefficient'],
                                                        'unit' => $productOrder->unit,
                                                        'price' => Price::getPriceWithTax($productOrder->price, $productOrder->taxRate->value)
                                                ];
                                        }

                                        foreach ($productsArray as $product) {
                                                if (!isset($productOrderArray[$product['id']])) {
                                                        $productOrderArray[$product['id']] = [
                                                                'quantity' => 0,
                                                                'unit' => $product['unit'],
                                                                'price' => Price::getPriceWithTax($product['price'], $product['taxRate']['value']),
                                                        ];
                                                }
                                        }

                                        $creditHistoryArray = [];

                                        foreach ($order->creditHistory as $creditHistory) {
                                                $creditHistoryArray[] = [
                                                        'date' => date('d/m/Y H:i:s', strtotime($creditHistory->date)),
                                                        'user_action' => $creditHistory->strUserAction(),
                                                        'wording' => $creditHistory->getStrWording(),
                                                        'debit' => ($creditHistory->isTypeDebit() ? '-&nbsp;' . $creditHistory->getAmount(Order::AMOUNT_TOTAL, true) : ''),
                                                        'credit' => ($creditHistory->isTypeCredit() ? '+&nbsp;' . $creditHistory->getAmount(Order::AMOUNT_TOTAL, true) : '')
                                                ];
                                        }

                                        $arrayCreditUser = [];
                                        if (isset($order->user) && isset($order->user->userProducer) && isset($order->user->userProducer[0])) {
                                                $arrayCreditUser['credit'] = $order->user->userProducer[0]->credit;
                                        }

                                        $oneProductUnactivated = false;
                                        foreach ($order->productOrder as $productOrder) {
                                                foreach ($productsArray as $product) {
                                                        if ($productOrder->id_product == $product['id'] && !$product['productDistribution'][0]['active']) {
                                                                $oneProductUnactivated = true;
                                                        }
                                                }
                                        }

                                        $order = array_merge($order->getAttributes(), [
                                                'selected' => false,
                                                'amount' => $order->getAmountWithTax(Order::AMOUNT_TOTAL),
                                                'amount_paid' => $order->getAmount(Order::AMOUNT_PAID),
                                                'amount_remaining' => $order->getAmount(Order::AMOUNT_REMAINING),
                                                'amount_surplus' => $order->getAmount(Order::AMOUNT_SURPLUS),
                                                'user' => (isset($order->user)) ? array_merge($order->user->getAttributes(), $arrayCreditUser) : null,
                                                'pointSale' => ['id' => $order->pointSale->id, 'name' => $order->pointSale->name],
                                                'productOrder' => $productOrderArray,
                                                'creditHistory' => $creditHistoryArray,
                                                'oneProductUnactivated' => $oneProductUnactivated
                                        ]);
                                }
                        }

                        $json['orders'] = $ordersArray;

                        // points de vente
                        $pointsSaleArray = PointSale::find()
                                ->joinWith(['pointSaleDistribution' => function ($q) use ($distribution) {
                                        $q->where(['id_distribution' => $distribution->id]);
                                }])
                                ->where([
                                        'id_producer' => GlobalParam::getCurrentProducerId(),
                                ])
                                ->asArray()
                                ->all();

                        $idPointSaleDefault = 0;
                        foreach ($pointsSaleArray as $pointSale) {
                                if ($pointSale['default']) {
                                        $idPointSaleDefault = $pointSale['id'] ;
                                }
                        }
                        $json['points_sale'] = $pointsSaleArray;

                        // bons de livraison
                        $deliveryNotesArray = DeliveryNote::searchAll([
                                'distribution.date' => $date,
                        ]) ;
                        $deliveryNotesByPointSaleArray = [] ;
                        foreach($deliveryNotesArray as $deliveryNote) {
                                if(isset($deliveryNote->orders[0])) {
                                        $deliveryNotesByPointSaleArray[$deliveryNote->orders[0]->id_point_sale] =
                                                $deliveryNote->getAttributes() ;
                                }
                        }
                        $json['delivery_notes'] = $deliveryNotesByPointSaleArray ;

                        // order create
                        $productOrderArray = [];
                        foreach ($productsArray as $product) {
                                $productOrderArray[$product['id']] = [
                                        'quantity' => 0,
                                        'unit' => $product['unit'],
                                        'price' => Price::getPriceWithTax($product['price'], $product['taxRate']['value']),
                                ];
                        }
                        $json['order_create'] = [
                                'id_point_sale' => $idPointSaleDefault,
                                'id_user' => 0,
                                'username' => '',
                                'comment' => '',
                                'productOrder' => $productOrderArray
                        ];

                        // utilisateurs
                        $usersArray = User::findBy()->all();

                        $json['users'] = $usersArray;

                        // une production de la semaine activée ou non
                        $oneDistributionWeekActive = false;
                        $week = sprintf('%02d', date('W', strtotime($date)));
                        $start = strtotime(date('Y', strtotime($date)) . 'W' . $week);
                        $dateMonday = date('Y-m-d', strtotime('Monday', $start));
                        $dateTuesday = date('Y-m-d', strtotime('Tuesday', $start));
                        $dateWednesday = date('Y-m-d', strtotime('Wednesday', $start));
                        $dateThursday = date('Y-m-d', strtotime('Thursday', $start));
                        $dateFriday = date('Y-m-d', strtotime('Friday', $start));
                        $dateSaturday = date('Y-m-d', strtotime('Saturday', $start));
                        $dateSunday = date('Y-m-d', strtotime('Sunday', $start));

                        $weekDistribution = Distribution::find()
                                ->andWhere([
                                        'id_producer' => GlobalParam::getCurrentProducerId(),
                                        'active' => 1,
                                ])
                                ->andWhere(['or',
                                        ['date' => $dateMonday],
                                        ['date' => $dateTuesday],
                                        ['date' => $dateWednesday],
                                        ['date' => $dateThursday],
                                        ['date' => $dateFriday],
                                        ['date' => $dateSaturday],
                                        ['date' => $dateSunday],
                                ])
                                ->one();
                        if ($weekDistribution) {
                                $oneDistributionWeekActive = true;
                        }

                        $json['one_distribution_week_active'] = $oneDistributionWeekActive;

                        // tiller
                        if ($producer->tiller) {
                                $tiller = new Tiller();
                                $json['tiller_is_synchro'] = (int)$tiller->isSynchro($date);
                        }

                        // abonnements manquants
                        $arraySubscriptions = Subscription::searchByDate($date);
                        $json['missing_subscriptions'] = [] ;
                        if($distribution->active) {
                                foreach ($arraySubscriptions as $subscription) {
                                        if(!$subscription->hasOrderAlreadyExist($ordersArrayObject)
                                                && $subscription->productSubscription && count($subscription->productSubscription)
                                                && $subscription->id_point_sale && $subscription->id_point_sale > 0) {

                                                $json['missing_subscriptions'][] = [
                                                        'username' => $subscription->getUsername()
                                                ] ;
                                        }
                                }
                        }
                }

                return $json;
        }

        public function actionAjaxPointSaleFavorite($idUser)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $user = User::findOne(['id' => $idUser]);
                $favoritePointSale = $user->getFavoritePointSale();
                $idFavoritePointSale = 0;
                if ($favoritePointSale) {
                        $idFavoritePointSale = $favoritePointSale->id;
                }

                return [
                        'id_favorite_point_sale' => $idFavoritePointSale
                ];
        }

        public function actionAjaxUpdateProductOrder($idUser = false, $idPointSale = false)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $user = User::findOne($idUser) ;
                $userProducer = UserProducer::searchOne([
                        'id_user' => $idUser,
                ]) ;
                $pointSale = PointSale::findOne($idPointSale) ;

                $productsArray = Product::find()
                        ->where([
                                'id_producer' => GlobalParam::getCurrentProducerId(),
                                'product.active' => 1,
                        ])->joinWith(['productPrice'])
                        ->all();

                $productOrderArray = [];
                foreach ($productsArray as $product) {
                        $productOrderArray[$product['id']] = [
                                'quantity' => 0,
                                'unit' => $product->unit,
                                'price' => $product->getPriceWithTax([
                                        'user' => $user,
                                        'user_producer' => $userProducer,
                                        'point_sale' => $pointSale
                                ]),
                        ];
                }

                return $productOrderArray ;
        }

        /**
         * Génére un PDF récapitulatif des des commandes d'un producteur pour une
         * date donnée (Méthode appelable via CRON)
         *
         * @param string $date
         * @param boolean $save
         * @param integer $idProducer
         * @param string $key
         * @return PDF|null
         */
        public function actionReportCron($date = '', $save = false, $idProducer = 0, $key = '')
        {
                if ($key == '64ac0bdab7e9f5e48c4d991ec5201d57') {
                        $this->actionReport($date, $save, $idProducer);
                }
        }

        /**
         * Génére un PDF récapitulatif des commandes d'un producteur pour une
         * date donnée.
         *
         * @param string $date
         * @param boolean $save
         * @param integer $idProducer
         * @return PDF|null
         */
        public function actionReport($date = '', $save = false, $idProducer = 0, $type = "pdf")
        {
                if (!Yii::$app->user->isGuest) {
                        $idProducer = GlobalParam::getCurrentProducerId();
                }

                $ordersArray = Order::searchAll([
                        'distribution.date' => $date,
                        'distribution.id_producer' => $idProducer
                ],
                        [
                                'orderby' => 'user.lastname ASC, user.name ASC, comment_point_sale ASC',
                                'conditions' => 'date_delete IS NULL'
                        ]);

                $distribution = Distribution::searchOne([
                        'id_producer' => $idProducer
                ], [
                        'conditions' => 'date LIKE :date',
                        'params' => [':date' => $date]
                ]);

                if ($distribution) {
                        $selectedProductsArray = ProductDistribution::searchByDistribution($distribution->id);
                        $pointsSaleArray = PointSale::searchAll([
                                'point_sale.id_producer' => $idProducer
                        ]);

                        foreach ($pointsSaleArray as $pointSale) {
                                $pointSale->initOrders($ordersArray);
                        }

                        // produits
                        $productsArray = Product::find()
                                ->joinWith(['productDistribution' => function ($q) use ($distribution) {
                                        $q->where(['id_distribution' => $distribution->id]);
                                }])
                                ->where([
                                        'id_producer' => $idProducer,
                                ])
                                ->orderBy('order ASC')
                                ->all();

                        if ($type == 'pdf') {

                                $viewPdf = 'report' ;
                                $orientationPdf = Pdf::ORIENT_PORTRAIT ;
                                $producer = GlobalParam::getCurrentProducer() ;
                                if($producer->slug == 'bourlinguepacotille') {
                                        $viewPdf = 'report-bourlingue' ;
                                        $orientationPdf = Pdf::ORIENT_LANDSCAPE ;
                                }

                                // get your HTML raw content without any layouts or scripts
                                $content = $this->renderPartial($viewPdf, [
                                        'date' => $date,
                                        'distribution' => $distribution,
                                        'selectedProductsArray' => $selectedProductsArray,
                                        'pointsSaleArray' => $pointsSaleArray,
                                        'productsArray' => $productsArray,
                                        'ordersArray' => $ordersArray,
                                        'producer' => Producer::searchOne(['id' => $idProducer])
                                ]);

                                $dateStr = date('d/m/Y', strtotime($date));

                                if ($save) {
                                        $destination = Pdf::DEST_FILE;
                                } else {
                                        $destination = Pdf::DEST_BROWSER;
                                }

                                $pdf = new Pdf([
                                        // set to use core fonts only
                                        'mode' => Pdf::MODE_UTF8,
                                        // A4 paper format
                                        'format' => Pdf::FORMAT_A4,
                                        // portrait orientation
                                        'orientation' => $orientationPdf,
                                        // stream to browser inline
                                        'destination' => $destination,
                                        'filename' => Yii::getAlias('@app/web/pdf/Commandes-' . $date . '-' . $idProducer . '.pdf'),
                                        // your html content input
                                        'content' => $content,
                                        // format content from your own css file if needed or use the
                                        // enhanced bootstrap css built by Krajee for mPDF formatting
                                        //'cssFile' => Yii::getAlias('@web/css/distribution/report.css'),
                                        // any css to be embedded if required
                                        'cssInline' => '
                        table {
                            border-spacing : 0px ;
                            border-collapse : collapse ;
                            width: 100% ;
                        }
                        
                        table tr th,
                        table tr td {
                            padding: 0px ;
                            margin: 0px ;
                            border: solid 1px #e0e0e0 ;
                            padding: 3px 8px ;
                            vertical-align : top;
                            page-break-inside: avoid !important;
                        }

                        table tr th { 
                            font-size: 13px ;
                        }

                        table tr td { 
                            font-size: 13px ;
                        }
                    ',
                                        // set mPDF properties on the fly
                                        //'options' => ['title' => 'Krajee Report Title'],
                                        // call mPDF methods on the fly
                                        'methods' => [
                                                'SetHeader' => ['Commandes du ' . $dateStr],
                                                'SetFooter' => ['{PAGENO}'],
                                        ]
                                ]);

                                // return the pdf output as per the destination setting
                                return $pdf->render();
                        }
                        elseif ($type == 'csv') {
                                $datas = [];

                                $optionCsvExportAllProducts = Producer::getConfig('option_csv_export_all_products') ;
                                $optionCsvExportByPiece = Producer::getConfig('option_csv_export_by_piece') ;

                                // produits en colonne
                                $productsNameArray = [''];
                                $productsIndexArray = [];
                                $productsHasQuantity = [];
                                $cpt = 1;
                                foreach ($productsArray as $product) {
                                        $productsHasQuantity[$product->id] = 0;
                                        foreach (Product::$unitsArray as $unit => $dataUnit) {
                                                $quantity = Order::getProductQuantity($product->id, $ordersArray, true, $unit);
                                                if ($quantity) {
                                                        $productsHasQuantity[$product->id] += $quantity;
                                                }
                                        }
                                        if ($productsHasQuantity[$product->id] > 0 || $optionCsvExportAllProducts) {
                                                $productName = $product->getNameExport() ;

                                                if($optionCsvExportByPiece) {
                                                        $productUnit = 'piece' ;
                                                }
                                                else {
                                                        $productUnit = $product->unit ;
                                                }

                                                $productName .= ' (' . Product::strUnit($productUnit, 'wording_short', true) . ')';

                                                $productsNameArray[] = $productName ;
                                                $productsIndexArray[$product->id] = $cpt++;
                                        }
                                }
                                $datas[] = $productsNameArray;

                                // points de vente
                                foreach ($pointsSaleArray as $pointSale) {
                                        if (count($pointSale->orders)) {
                                                // listing commandes
                                                $datas[] = ['> ' . $pointSale->name];
                                                foreach ($pointSale->orders as $order) {
                                                        $orderLine = [$order->getStrUser()];

                                                        if($optionCsvExportByPiece) {
                                                                foreach ($order->productOrder as $productOrder) {
                                                                        $orderLine[$productsIndexArray[$productOrder->id_product]] = Order::getProductQuantityPieces($productOrder->id_product, [$order]) ;
                                                                }
                                                        }
                                                        else {
                                                                foreach ($productsIndexArray as $idProduct => $indexProduct) {
                                                                        $orderLine[$indexProduct] = '';
                                                                }

                                                                foreach ($order->productOrder as $productOrder) {
                                                                        if (strlen($orderLine[$productsIndexArray[$productOrder->id_product]])) {
                                                                                $orderLine[$productsIndexArray[$productOrder->id_product]] .= ' + ';
                                                                        }
                                                                        $orderLine[$productsIndexArray[$productOrder->id_product]] .= $productOrder->quantity;
                                                                        if ($productOrder->product->unit != $productOrder->unit) {
                                                                                $orderLine[$productsIndexArray[$productOrder->id_product]] .= Product::strUnit($productOrder->unit, 'wording_short', true);
                                                                        }
                                                                }
                                                        }

                                                        $datas[] = $this->_lineOrderReportCSV($orderLine, $cpt);
                                                }

                                                // total point de vente
                                                if($optionCsvExportByPiece) {
                                                        $totalsPointSaleArray = $this->_totalReportPiecesCSV(
                                                                'Total',
                                                                $pointSale->orders,
                                                                $productsArray,
                                                                $productsIndexArray
                                                        );
                                                }
                                                else {
                                                        $totalsPointSaleArray = $this->_totalReportCSV(
                                                                'Total',
                                                                $pointSale->orders,
                                                                $productsArray,
                                                                $productsIndexArray
                                                        );
                                                }

                                                $datas[] = $this->_lineOrderReportCSV($totalsPointSaleArray, $cpt);

                                                $datas[] = [];
                                        }
                                }

                                // global
                                if($optionCsvExportByPiece) {
                                        $totalsGlobalArray = $this->_totalReportPiecesCSV(
                                                '> Totaux',
                                                $ordersArray,
                                                $productsArray,
                                                $productsIndexArray
                                        );
                                }
                                else {
                                        $totalsGlobalArray = $this->_totalReportCSV(
                                                '> Totaux',
                                                $ordersArray,
                                                $productsArray,
                                                $productsIndexArray
                                        );
                                }

                                $datas[] = $this->_lineOrderReportCSV($totalsGlobalArray, $cpt);

                                CSV::downloadSendHeaders('Commandes_' . $date . '.csv');
                                echo CSV::array2csv($datas);
                                die();
                        }
                }

                return null;
        }

        public function actionReportGrid($date = '', $save = false, $idProducer = 0, $type = "pdf")
        {
                if (!Yii::$app->user->isGuest) {
                        $idProducer = GlobalParam::getCurrentProducerId();
                }

                $distribution = Distribution::searchOne([
                        'id_producer' => $idProducer
                ], [
                        'conditions' => 'date LIKE :date',
                        'params' => [':date' => $date]
                ]);

                if ($distribution) {

                        $ordersArray = Order::searchAll([
                                'distribution.date' => $date,
                                'distribution.id_producer' => $idProducer
                        ],
                                [
                                        'orderby' => 'user.lastname ASC, user.name ASC, comment_point_sale ASC',
                                        'conditions' => 'date_delete IS NULL'
                                ]);

                        $selectedProductsArray = ProductDistribution::searchByDistribution($distribution->id);
                        $pointsSaleArray = PointSale::searchAll([
                                'point_sale.id_producer' => $idProducer
                        ]);

                        foreach ($pointsSaleArray as $pointSale) {
                                $pointSale->initOrders($ordersArray);
                        }

                        $ordersByPage = 22 ;
                        $nbPages = ceil(count($ordersArray) / $ordersByPage) ;
                        $ordersArrayPaged = [] ;

                        foreach($pointsSaleArray as $pointSale) {

                                $index = 0 ;
                                $indexPage = 0 ;

                                foreach($pointSale->orders as $order) {
                                        if(!isset($ordersArrayPaged[$pointSale->id])) {
                                                $ordersArrayPaged[$pointSale->id] = [] ;
                                        }
                                        if(!isset($ordersArrayPaged[$pointSale->id][$indexPage])) {
                                                $ordersArrayPaged[$pointSale->id][$indexPage] = [] ;
                                        }
                                        $ordersArrayPaged[$pointSale->id][$indexPage][] = $order ;

                                        $index ++ ;

                                        if($index == $ordersByPage) {
                                                $index = 0 ;
                                                $indexPage ++ ;
                                        }
                                }
                        }

                        // catégories
                        $categoriesArray = ProductCategory::searchAll(['id_producer' => $idProducer], ['orderby' => 'product_category.position ASC']) ;
                        array_unshift($categoriesArray, null) ;

                        // produits
                        $productsArray = Product::find()
                                ->joinWith(['productDistribution' => function ($q) use ($distribution) {
                                        $q->where(['id_distribution' => $distribution->id]);
                                }])
                                ->where([
                                        'id_producer' => $idProducer,
                                ])
                                ->orderBy('order ASC')
                                ->all();


                        $viewPdf = 'report-grid';
                        $orientationPdf = Pdf::ORIENT_PORTRAIT;
                        $producer = GlobalParam::getCurrentProducer();
                        if ($producer->slug == 'bourlinguepacotille') {
                                $viewPdf = 'report-bourlingue';
                                $orientationPdf = Pdf::ORIENT_LANDSCAPE;
                        }

                        // get your HTML raw content without any layouts or scripts
                        $content = $this->renderPartial($viewPdf, [
                                'date' => $date,
                                'distribution' => $distribution,
                                'selectedProductsArray' => $selectedProductsArray,
                                'pointsSaleArray' => $pointsSaleArray,
                                'categoriesArray' => $categoriesArray,
                                'productsArray' => $productsArray,
                                'ordersArray' => $ordersArrayPaged,
                                'producer' => Producer::searchOne(['id' => $idProducer])
                        ]);

                        $dateStr = date('d/m/Y', strtotime($date));

                        if ($save) {
                                $destination = Pdf::DEST_FILE;
                        } else {
                                $destination = Pdf::DEST_BROWSER;
                        }

                        $pdf = new Pdf([
                                // set to use core fonts only
                                'mode' => Pdf::MODE_UTF8,
                                // A4 paper format
                                'format' => Pdf::FORMAT_A4,
                                // portrait orientation
                                'orientation' => $orientationPdf,
                                // stream to browser inline
                                'destination' => $destination,
                                'filename' => Yii::getAlias('@app/web/pdf/Commandes-' . $date . '-' . $idProducer . '.pdf'),
                                // your html content input
                                'content' => $content,
                                // format content from your own css file if needed or use the
                                // enhanced bootstrap css built by Krajee for mPDF formatting
                                //'cssFile' => Yii::getAlias('@web/css/distribution/report.css'),
                                // any css to be embedded if required
                                'cssInline' => '
                        table {
                            border-spacing : 0px ;
                            border-collapse : collapse ;
                            width: 100% ;
                        }
                        
                        table tr th,
                        table tr td {
                            padding: 0px ;
                            margin: 0px ;
                            border: solid 1px #e0e0e0 ;
                            padding: 3px ;
                            vertical-align : top;
                            page-break-inside: avoid !important;
                        }

                        table tr th { 
                            font-size: 10px ;
                        }
                       
                        table tr td { 
                            font-size: 10px ;
                        }
                        
                        table thead tr {
                                line-height: 220px;
                                text-align:left;
                        }
                       
                        .th-user,
                         .td-nb-products {
                                /* width: 35px ; */
                                text-align: center ;
                        }
                        
                        .th-user {
                                padding: 10px ;
                        }
                        
                        .category-name {
                                font-weight: bold ;
                        }
                        
                    ',
                                // set mPDF properties on the fly
                                //'options' => ['title' => 'Krajee Report Title'],
                                // call mPDF methods on the fly
                                'methods' => [
                                        'SetHeader' => ['Commandes du ' . $dateStr],
                                        'SetFooter' => ['{PAGENO}'],
                                ]
                        ]);

                        // return the pdf output as per the destination setting
                        return $pdf->render();

                }
        }

        /**
         * Génère un export des commandes au format CSV à destination du Google Drive
         * de Terre de pains.
         *
         * @param type $date
         * @return CSV
         */

        public function actionReportTerredepains($date, $key)
        {

                if ($key == 'ef572cc148c001f0180c4a624189ed30') {

                        $producer = Producer::searchOne([
                                'producer.slug' => 'terredepains'
                        ]);

                        $idProducer = $producer->id;

                        $ordersArray = Order::searchAll([
                                'distribution.date' => $date,
                                'distribution.id_producer' => $idProducer
                        ],
                                [
                                        'orderby' => 'user.lastname ASC, user.name ASC, comment_point_sale ASC',
                                        'conditions' => 'date_delete IS NULL'
                                ]);

                        $distribution = Distribution::searchOne([
                                'distribution.id_producer' => $idProducer
                        ], [
                                'conditions' => 'date LIKE :date',
                                'params' => [
                                        ':date' => $date,
                                ]
                        ]);

                        if ($distribution) {
                                $selectedProductsArray = ProductDistribution::searchByDistribution($distribution->id);
                                $pointsSaleArray = PointSale::searchAll([
                                        'point_sale.id_producer' => $idProducer
                                ]);

                                foreach ($pointsSaleArray as $pointSale) {
                                        $pointSale->initOrders($ordersArray);
                                }

                                // produits
                                $productsArray = Product::find()
                                        ->joinWith(['productDistribution' => function ($q) use ($distribution) {
                                                $q->where(['id_distribution' => $distribution->id]);
                                        }])
                                        ->where([
                                                'id_producer' => $idProducer,
                                        ])
                                        ->orderBy('order ASC')
                                        ->all();

                                $datas = [];

                                // produits en colonne
                                $productsNameArray = [''];
                                $productsIndexArray = [];
                                $productsHasQuantity = [];
                                $cpt = 1;
                                foreach ($productsArray as $product) {
                                        $theUnit = Product::strUnit($product->unit, 'wording_short', true);
                                        $theUnit = ($theUnit == 'p.') ? '' : ' (' . $theUnit . ')';
                                        $productsNameArray[] = $product->name . $theUnit;
                                        $productsIndexArray[$product->id] = $cpt++;
                                }
                                $productsNameArray[] = 'Total';
                                $datas[] = $productsNameArray;

                                // global
                                $totalsGlobalArray = $this->_totalReportCSV(
                                        '> Totaux',
                                        $ordersArray,
                                        $productsArray,
                                        $productsIndexArray
                                );
                                $datas[] = $this->_lineOrderReportCSV($totalsGlobalArray, $cpt - 1, true);

                                $datas[] = [];

                                // points de vente
                                foreach ($pointsSaleArray as $pointSale) {
                                        if (count($pointSale->orders)) {
                                                // listing commandes

                                                /*foreach ($pointSale->orders as $order) {
                                                        $orderLine = [$order->getStrUser()];

                                                        foreach ($productsIndexArray as $idProduct => $indexProduct) {
                                                                $orderLine[$indexProduct] = '';
                                                        }

                                                        foreach ($order->productOrder as $productOrder) {
                                                                if (strlen($orderLine[$productsIndexArray[$productOrder->id_product]])) {
                                                                        $orderLine[$productsIndexArray[$productOrder->id_product]] .= ' + ';
                                                                }
                                                                $orderLine[$productsIndexArray[$productOrder->id_product]] .= $productOrder->quantity;
                                                                if ($productOrder->product->unit != $productOrder->unit) {
                                                                        $orderLine[$productsIndexArray[$productOrder->id_product]] .= Product::strUnit($productOrder->unit, 'wording_short', true);
                                                                }
                                                        }
                                                        $datas[] = $this->_lineOrderReportCSV($orderLine, $cpt - 1, true);
                                                }*/

                                                // total point de vente
                                                $totalsPointSaleArray = $this->_totalReportCSV(
                                                        '> ' . $pointSale->name,
                                                        $pointSale->orders,
                                                        $productsArray,
                                                        $productsIndexArray
                                                );
                                                $datas[] = $this->_lineOrderReportCSV($totalsPointSaleArray, $cpt - 1, true);
                                        }
                                }

                                CSV::downloadSendHeaders('Commandes_' . $date . '.csv');
                                echo CSV::array2csv($datas);
                                die();

                        }

                        return null;
                }
        }

        public function _totalReportCSV($label, $ordersArray, $productsArray, $productsIndexArray)
        {
                $totalsPointSaleArray = [$label];
                foreach ($productsArray as $product) {
                        foreach (Product::$unitsArray as $unit => $dataUnit) {
                                $quantity = Order::getProductQuantity($product->id, $ordersArray, false, $unit);
                                if ($quantity) {
                                        $index = $productsIndexArray[$product->id];
                                        if (!isset($totalsPointSaleArray[$index])) {
                                                $totalsPointSaleArray[$index] = '';
                                        }

                                        if (strlen($totalsPointSaleArray[$index])) {
                                                $totalsPointSaleArray[$index] .= ' + ';
                                        }

                                        $totalsPointSaleArray[$index] .= $quantity;

                                        if ($product->unit != $unit) {
                                                $totalsPointSaleArray[$index] .= '' . Product::strUnit($unit, 'wording_short', true);
                                        }
                                }
                        }
                }
                return $totalsPointSaleArray;
        }

        public function _totalReportPiecesCSV($label, $ordersArray, $productsArray, $productsIndexArray)
        {
                $totalsPointSaleArray = [$label];

                foreach ($productsArray as $product) {
                        $quantity = 0 ;
                        foreach (Product::$unitsArray as $unit => $dataUnit) {
                                $quantityProduct = Order::getProductQuantity($product->id, $ordersArray, false, $unit);

                                if($unit == 'piece') {
                                        $quantity += $quantityProduct ;
                                }
                                else {
                                        if($product->weight > 0) {
                                                $quantity += ($quantityProduct * $dataUnit['coefficient']) / $product->weight ;
                                        }
                                }
                        }

                        if($quantity) {
                                $index = $productsIndexArray[$product->id];
                                $totalsPointSaleArray[$index] = $quantity;
                        }
                }

                return $totalsPointSaleArray;
        }

        public function _lineOrderReportCSV($orderLine, $cptMax, $showTotal = false)
        {
                $line = [];
                $cptTotal = 0;

                for ($i = 0; $i <= $cptMax; $i++) {
                        if (isset($orderLine[$i]) && $orderLine[$i]) {
                                $line[] = $orderLine[$i];
                                if(is_numeric($orderLine[$i])) {
                                        $cptTotal += $orderLine[$i];
                                }
                        } else {
                                $line[] = '';
                        }
                }

                if ($cptTotal > 0 && $showTotal) {
                        $line[] = $cptTotal;
                }

                return $line;
        }

        public function actionAjaxProcessProductQuantityMax($idDistribution, $idProduct, $quantityMax)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $productDistribution = ProductDistribution::searchOne([
                        'id_distribution' => $idDistribution,
                        'id_product' => $idProduct,
                ]);

                $productDistribution->quantity_max = (!$quantityMax) ? null : (float) $quantityMax;

                $productDistribution->save();

                return ['success'];
        }

        public function actionAjaxProcessActiveProduct($idDistribution, $idProduct, $active)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $productDistribution = ProductDistribution::searchOne([
                        'id_distribution' => $idDistribution,
                        'id_product' => $idProduct,
                ]);
                $productDistribution->active = $active;
                $productDistribution->save();

                return ['success'];
        }

        public function actionAjaxProcessActivePointSale($idDistribution, $idPointSale, $delivery)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $pointSaleDistribution = PointSaleDistribution::searchOne([
                        'id_distribution' => $idDistribution,
                        'id_point_sale' => $idPointSale,
                ]);
                $pointSaleDistribution->delivery = $delivery;
                $pointSaleDistribution->save();

                return ['success'];
        }

        /**
         * Active/désactive un jour de distribution.
         *
         * @param integer $idDistribution
         * @param string $date
         * @param boolean $active
         * @return array
         */
        public function actionAjaxProcessActiveDistribution($idDistribution = 0, $date = '', $active)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if ($idDistribution) {
                        $distribution = Distribution::searchOne([
                                'id' => $idDistribution
                        ]);
                }

                $format = 'Y-m-d';
                $dateObject = DateTime::createFromFormat($format, $date);
                if ($dateObject && $dateObject->format($format) === $date) {
                        $distribution = Distribution::initDistribution($date);
                }

                if ($distribution) {
                        $distribution->active($active);
                        return ['success'];
                }

                return ['error'];
        }

        /**
         * Change l'état d'une semaine de production (activé, désactivé).
         *
         * @param string $date
         * @param integer $active
         */
        public function actionAjaxProcessActiveWeekDistribution($date, $active)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $week = sprintf('%02d', date('W', strtotime($date)));
                $start = strtotime(date('Y', strtotime($date)) . 'W' . $week);
                $dateMonday = date('Y-m-d', strtotime('Monday', $start));
                $dateTuesday = date('Y-m-d', strtotime('Tuesday', $start));
                $dateWednesday = date('Y-m-d', strtotime('Wednesday', $start));
                $dateThursday = date('Y-m-d', strtotime('Thursday', $start));
                $dateFriday = date('Y-m-d', strtotime('Friday', $start));
                $dateSaturday = date('Y-m-d', strtotime('Saturday', $start));
                $dateSunday = date('Y-m-d', strtotime('Sunday', $start));

                $pointsSaleArray = PointSale::searchAll();

                $activeMonday = false;
                $activeTuesday = false;
                $activeWednesday = false;
                $activeThursday = false;
                $activeFriday = false;
                $activeSaturday = false;
                $activeSunday = false;

                foreach ($pointsSaleArray as $pointSale) {
                        if ($pointSale->delivery_monday) $activeMonday = true;
                        if ($pointSale->delivery_tuesday) $activeTuesday = true;
                        if ($pointSale->delivery_wednesday) $activeWednesday = true;
                        if ($pointSale->delivery_thursday) $activeThursday = true;
                        if ($pointSale->delivery_friday) $activeFriday = true;
                        if ($pointSale->delivery_saturday) $activeSaturday = true;
                        if ($pointSale->delivery_sunday) $activeSunday = true;
                }

                if ($activeMonday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateMonday, $active);
                }
                if ($activeTuesday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateTuesday, $active);
                }
                if ($activeWednesday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateWednesday, $active);
                }
                if ($activeThursday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateThursday, $active);
                }
                if ($activeFriday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateFriday, $active);
                }
                if ($activeSaturday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateSaturday, $active);
                }
                if ($activeSunday || !$active) {
                        $this->actionAjaxProcessActiveDistribution(0, $dateSunday, $active);
                }

                return ['success'];
        }

        /**
         * Ajoute les commandes récurrentes pour une date donnée.
         *
         * @param string $date
         */
        public function actionAjaxProcessAddSubscriptions($date)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Subscription::addAll($date, true);
                return ['success'];
        }

        /**
         * Synchronise les commandes avec la plateforme Tiller pour une date donnée.
         *
         * @param string $date
         */
        public function actionAjaxProcessSynchroTiller($date)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $return = [] ;
                $producerTiller = Producer::getConfig('tiller');

                if ($producerTiller) {

                        $tiller = new Tiller();
                        $isSynchro = $tiller->isSynchro($date);

                        if (!$isSynchro) {
                                $orders = Order::searchAll([
                                        'distribution.date' => $date,
                                        'order.tiller_synchronization' => 1
                                ]);

                                $strDate = date('Y-m-d\T12:i:s+0000', strtotime($date) + 1);

                                if ($orders && count($orders)) {
                                        foreach ($orders as $order) {
                                                $lines = [];
                                                foreach ($order->productOrder as $productOrder) {
                                                        $lines[] = [
                                                                'name' => $productOrder->product->name,
                                                                'price' => $productOrder->getPriceWithTax() * 100 * $productOrder->quantity,
                                                                'tax' => $productOrder->taxRate->value * 100,
                                                                'date' => $strDate,
                                                                'quantity' => $productOrder->quantity
                                                        ];
                                                }

                                                $typePaymentTiller = '';
                                                if ($order->mean_payment == MeanPayment::MONEY) {
                                                        $typePaymentTiller = 'CASH';
                                                }
                                                if ($order->mean_payment == MeanPayment::CREDIT_CARD
                                                    || $order->mean_payment == MeanPayment::CREDIT
                                                    || $order->mean_payment == MeanPayment::TRANSFER
                                                    || $order->mean_payment == MeanPayment::OTHER) {
                                                        $typePaymentTiller = 'CARD';
                                                }
                                                if ($order->mean_payment == MeanPayment::CHEQUE) {
                                                        $typePaymentTiller = 'BANK_CHECK';
                                                }
                                                if (!strlen($typePaymentTiller) || !$order->mean_payment) {
                                                        $typePaymentTiller = 'CASH';
                                                }

                                                $return[] = $tiller->postOrder([
                                                        'externalId' => $order->id,
                                                        'type' => 1,
                                                        'status' => "CLOSED",
                                                        'openDate' => $strDate,
                                                        'closeDate' => $strDate,
                                                        'lines' => $lines,
                                                        'payments' => [[
                                                                'type' => $typePaymentTiller,
                                                                'amount' => $order->getAmountWithTax(Order::AMOUNT_TOTAL) * 100,
                                                                'status' => 'ACCEPTED',
                                                                'date' => $strDate
                                                        ]]
                                                ]);
                                        }
                                }
                        }
                }

                return $return ;
        }

        public function actionAjaxValidateDeliveryNotes($idOrders)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if (strlen($idOrders)) {
                        $idOrders = json_decode($idOrders, true);

                        if (is_array($idOrders) && count($idOrders) > 0) {
                                foreach($idOrders as $idOrder) {
                                        $order = Order::searchOne([
                                                'id' => (int)$idOrder
                                        ]);

                                        if($order && $order->distribution->id_producer == GlobalParam::getCurrentProducerId()) {
                                                $deliveryNote = DeliveryNote::searchOne([
                                                        'id' => (int) $order->id_delivery_note
                                                ]) ;

                                                if($deliveryNote && $deliveryNote->isStatusDraft()) {
                                                        $deliveryNote->changeStatus(Document::STATUS_VALID);
                                                        $deliveryNote->save();
                                                }
                                        }
                                }

                                return [
                                        'return' => 'success',
                                        'alert' => [
                                                'type' => 'success',
                                                'message' => 'Bon(s) de livraison validé(s)'
                                        ]
                                ] ;
                        }
                }

                return [
                        'return' => 'error',
                        'alert' => [
                                'type' => 'danger',
                                'message' => 'Une erreur est survenue lors de la validation des bons de livraison'
                        ]
                ];
        }

        public function actionAjaxGenerateDeliveryNoteEachUser($idOrders)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if (strlen($idOrders)) {
                        $idOrders = json_decode($idOrders, true);

                        if (is_array($idOrders) && count($idOrders) > 0) {

                                foreach ($idOrders as $idOrder) {
                                        $order = Order::searchOne([
                                                'id' => (int)$idOrder
                                        ]);

                                        if($order && $order->distribution->id_producer == GlobalParam::getCurrentProducerId()) {

                                                $deliveryNote = null ;
                                                $idDeliveryNote = $order->id_delivery_note ;
                                                if($idDeliveryNote) {
                                                        $deliveryNote = DeliveryNote::searchOne([
                                                                'id' => (int) $idDeliveryNote
                                                        ]);
                                                }

                                                // on regénére le document si c'est un brouillon
                                                if($deliveryNote && $deliveryNote->isStatusDraft()) {
                                                        $deliveryNote->delete() ;
                                                        $deliveryNote = null ;
                                                }

                                                if(!$deliveryNote) {
                                                        $deliveryNote = new DeliveryNote() ;
                                                        $deliveryNote->id_producer = GlobalParam::getCurrentProducerId();
                                                        $deliveryNote->id_user = $order->id_user;
                                                        $deliveryNote->name = 'Bon de livraison ' . $order->getUsername() . ' (' . date('d/m/Y', strtotime($order->distribution->date)) . ')';
                                                        $deliveryNote->address = $order->user->getFullAddress() ;
                                                        $deliveryNote->save();
                                                }

                                                if($deliveryNote) {
                                                        $order->id_delivery_note = $deliveryNote->id;
                                                        $order->save();
                                                }
                                        }
                                }
                        }

                        return [
                                'return' => 'success',
                                'alert' => [
                                        'type' => 'success',
                                        'message' => 'Bon(s) de livraison généré(s)'
                                ]
                        ] ;
                }

                return [
                        'return' => 'error',
                        'alert' => [
                                'type' => 'danger',
                                'message' => 'Une erreur est survenue lors de la génération du bon de livraison.'
                        ]
                ];
        }

        public function actionAjaxGenerateDeliveryNote($idOrders)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if (strlen($idOrders)) {
                        $idOrders = json_decode($idOrders, true);

                        if (is_array($idOrders) && count($idOrders) > 0) {

                                // récupération première commande pour obtenir des infos
                                reset($idOrders) ;
                                $firstOrder = Order::searchOne([
                                        'id' => (int)$idOrders[key($idOrders)]
                                ]);

                                // deliveryNote existant
                                $deliveryNote = null ;
                                $isUpdate = false ;
                                $i = 0 ;
                                $ordersArray = Order::searchAll([
                                        'id' => $idOrders,
                                ]) ;
                                do {
                                        $order = $ordersArray[$i] ;
                                        if($order->distribution->id_producer == GlobalParam::getCurrentProducerId() && $order->id_delivery_note > 0) {
                                                $deliveryNote = DeliveryNote::searchOne([
                                                        'id' => $order->id_delivery_note
                                                ]) ;
                                                $isUpdate = true ;
                                        }
                                        $i ++ ;
                                } while($deliveryNote == null && isset($ordersArray[$i])) ;

                                if($deliveryNote && $deliveryNote->status == Document::STATUS_VALID) {
                                        return [
                                                'return' => 'error',
                                                'alert' => [
                                                        'type' => 'danger',
                                                        'message' => 'Vous ne pouvez pas modifier un bon de livraison déjà validé.'
                                                ]
                                        ] ;
                                }

                                if ($firstOrder) {
                                        // génération du BL
                                        if(!$deliveryNote) {
                                                $deliveryNote = new DeliveryNote;
                                                $deliveryNote->name = 'Bon de livraison ' . $firstOrder->pointSale->name . ' (' . date('d/m/Y', strtotime($firstOrder->distribution->date)) . ')';
                                                $deliveryNote->id_producer = GlobalParam::getCurrentProducerId();

                                                if($firstOrder->pointSale->id_user) {
                                                        $deliveryNote->id_user = $firstOrder->pointSale->id_user;
                                                        $user = User::searchOne([
                                                                'id' => $deliveryNote->id_user
                                                        ]) ;
                                                }
                                                else {
                                                        $user = new User ;
                                                        $user->type = User::TYPE_LEGAL_PERSON ;
                                                        $user->name_legal_person = $firstOrder->pointSale->name ;
                                                        $user->address = $firstOrder->pointSale->address ;
                                                        $user->id_producer = 0 ;
                                                        $user->setPassword(Password::generate());
                                                        $user->generateAuthKey();
                                                        $user->email = '' ;
                                                        if (!strlen($user->email)) {
                                                                $user->username = 'inconnu@opendistrib.net';
                                                        }
                                                        $user->save() ;

                                                        $userProducer = new UserProducer;
                                                        $userProducer->id_user = $user->id;
                                                        $userProducer->id_producer = GlobalParam::getCurrentProducerId();
                                                        $userProducer->credit = 0;
                                                        $userProducer->active = 1;
                                                        $userProducer->save();

                                                        $firstOrder->pointSale->id_user = $user->id ;
                                                        $firstOrder->pointSale->save() ;

                                                        $deliveryNote->id_user = $user->id;
                                                }

                                                $deliveryNote->address = $user->getFullAddress() ;
                                                $deliveryNote->save();
                                        }
                                        else {
                                                // réinitialisation des order.id_delivery_order
                                                Order::updateAll([
                                                        'id_delivery_note' => null
                                                ], [
                                                        'id_delivery_note' => $deliveryNote->id
                                                ]) ;
                                        }

                                        if(!isset($user) || !$user) {
                                                $user = User::searchOne([
                                                        'id' => $deliveryNote->id_user
                                                ]) ;
                                                $userProducer = UserProducer::searchOne([
                                                        'id_user' => $deliveryNote->id_user,
                                                        'id_producer' => GlobalParam::getCurrentProducerId()
                                                ]) ;
                                        }

                                        // affectation du BL aux commandes
                                        foreach ($idOrders as $idOrder) {
                                                $order = Order::searchOne([
                                                        'id' => (int)$idOrder
                                                ]);
                                                if ($order && $order->distribution->id_producer == GlobalParam::getCurrentProducerId()) {
                                                        $order->id_delivery_note = $deliveryNote->id;
                                                        $order->save();
                                                }

                                                // init invoice price
                                                $order = Order::searchOne(['id' => $idOrder]) ;
                                                if($order) {
                                                        $order->initInvoicePrices([
                                                                'user' => $user,
                                                                'user_producer' => $userProducer,
                                                                'point_sale' => $firstOrder->pointSale
                                                        ]) ;
                                                }
                                        }

                                        return [
                                                'return' => 'success',
                                                'alert' => [
                                                        'type' => 'success',
                                                        'message' => 'Bon de livraison '.($isUpdate ? 'modifié' : 'généré')
                                                ]
                                        ] ;
                                }
                        }
                }

                return [
                        'return' => 'error',
                        'alert' => [
                                'type' => 'danger',
                                'message' => 'Une erreur est survenue lors de la génération du bon de livraison.'
                        ]
                ];
        }

}