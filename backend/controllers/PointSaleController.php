<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\PointSale;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\UserPointSale;
use common\models\Order ;
use common\models\Producer ;
use common\models\Distribution ;
use yii\helpers\Html;

/**
 * PointVenteController implements the CRUD actions for PointVente model.
 */
class PointSaleController extends BackendController 
{

    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend();
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Liste les points de vente.
     * 
     * @return mixed
     */
    public function actionIndex() 
    {        
        $searchModel = new PointSaleSearch() ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Crée un point de vente.
     * 
     * @return mixed
     */
    public function actionCreate() 
    {
        $model = new PointSale();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->processPointProduction();
            $model->processRestrictedAccess();
            Distribution::linkPointSaleIncomingDistributions($model) ;
            return $this->redirect(['index']);
        } else {
            return $this->render('create', array_merge($this->initForm(), [
                'model' => $model,
            ]));
        }
    }

    /**
     * Modifie un point de vente.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) 
    {
        $model = PointSale::find()
                ->with('userPointSale')
                ->where(['id' => $id])
                ->one();

        foreach ($model->userPointSale as $userPointSale) {
            $model->users[] = $userPointSale->id_user;
            $model->users_comment[$userPointSale->id_user] = $userPointSale->comment;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->processPointProduction();
            $model->processRestrictedAccess();
            Distribution::linkPointSaleIncomingDistributions($model) ;
            Yii::$app->getSession()->setFlash('success', 'Point de vente modifié.');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', array_merge($this->initForm($id), [
                'model' => $model,
            ]));
        }
    }

    /**
     * Initialise le formulaire de création/modification.
     * 
     * @param integer $id
     * @return mixed
     */
    public function initForm($id = 0) 
    {
        $users = User::findBy()
            ->leftJoin('user_point_sale', 'user_point_sale.id_user = user.id AND user_point_sale.id_point_sale = :id_point_sale',[':id_point_sale' => $id])
            ->orderBy('user_point_sale.id_point_sale DESC, lastname ASC, name ASC')
            ->all();
        
        return [
            'users' => $users
        ];
    }

    /**
     * Supprime un point de vente et redirige vers la liste des points de vente.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $confirm = false) 
    {  
        $pointSale = $this->findModel($id) ;
        
        if($confirm) {
            $pointSale->delete();
            UserPointSale::deleteAll(['id_point_sale' => $id]);
            PointSaleDistribution::deleteAll(['id_point_sale' => $id]) ;
            Order::updateAll(['id_point_sale' => 0], 'id_point_sale = :id_point_sale', [':id_point_sale' => $id]) ;
            Yii::$app->getSession()->setFlash('success', 'Point de vente <strong>'.Html::encode($pointSale->name).'</strong> supprimé.');
        } 
        else {
            Yii::$app->getSession()->setFlash('info', 'Souhaitez-vous vraiment supprimer le point de vente <strong>'.Html::encode($pointSale->name).'</strong> ? '
                    . Html::a('Oui',['point-sale/delete','id' => $id, 'confirm' => 1], ['class' => 'btn btn-default']).' '.Html::a('Non', ['point-sale/index'], ['class' => 'btn btn-default']));
        }
        
        return $this->redirect(['index']);
    }
    
    /**
     * Définit un point de vente par défaut.
     * 
     * @param integer $id
     */
    public function actionDefault($id) 
    {
        $pointSale = $this->findModel($id) ;
        if($pointSale) {
            PointSale::updateAll(['default' => 0], 'id_producer = :id_producer', [':id_producer' => GlobalParam::getCurrentProducerId()]) ;
            if(!$pointSale->default) {
                $pointSale->default = 1 ;
                $pointSale->save() ;
                Yii::$app->getSession()->setFlash('success', 'Point de vente <strong>'.Html::encode($pointSale->name).'</strong> défini par défaut.') ;
            }
            else {
                Yii::$app->getSession()->setFlash('success', 'Aucun point de vente défini par défaut') ;
            }
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Recherche un point de vente en fonction de son ID.
     * 
     * @param integer $id
     * @return PointVente 
     * @throws NotFoundHttpException si le modèle n'est pas trouvé
     */
    protected function findModel($id) 
    {
        if (($model = PointSale::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
