<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\helpers\GlobalParam;
use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;
use common\models\User;
use common\models\Order;
use DateTime;
use DateInterval;
use DatePeriod;

class StatsController extends BackendController 
{

    public function behaviors() 
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend() ;
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Affiche les statistiques de l'année avec le CA  réalisé par mois.
     * 
     * @return mixed
     */
    public function actionIndex() 
    {
        /*
         *  Volume de commande de l'année passée (par mois)
         */

        $dateStart = date('Y-m-d', time() - 60 * 60 * 24 * 365);
        $dateEnd = date('Y-m-d', time() + 60 * 60 * 24 * 31);

        $data = [];

        // labels
        $dataLabels = [];

        $start = new DateTime($dateStart);
        $interval = new DateInterval('P1M');
        $end = new DateTime($dateEnd);

        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $date) {
            $month = date('m/Y', $date->getTimestamp());
            $dataLabels[] = $month;

            $res = Yii::$app->db->createCommand("SELECT SUM(product_order.price * product_order.quantity) AS total
                    FROM `order`, product_order, distribution
                    WHERE distribution.id_producer = :id_producer
                    AND `order`.id_distribution = distribution.id
                    AND `order`.id = product_order.id_order
                    AND distribution.date >= :date_start
                    AND distribution.date <= :date_end
                ")
                ->bindValue(':id_producer',GlobalParam::getCurrentProducerId())
                ->bindValue(':date_start', date('Y-m-', $date->getTimestamp()).'01')
                ->bindValue(':date_end', date('Y-m-', $date->getTimestamp()).'31' )
                ->queryOne();

            if($res['total']) {
                $data[$month] = $res['total'];
            }
            else {
                $data[$month] = 0;
            }

        }

        // création d'un tableau sans index car chart.js n'accepte pas les index 
        $dataNoIndex = [];
        foreach ($data as $key => $val) {
            $dataNoIndex[] = round($val, 2);
        }

        return $this->render('index', [
            'dataLabels' => $dataLabels,
            'data' => $dataNoIndex,
        ]);
    }
    
    const TOTALS = 13 ;
    
    /**
     * Affiche un tableau avec les totaux (maximums, commandés) de chaque produit
     * par mois d'une année donnée.
     * 
     * @param integer $year
     * @return mixed
     */
    public function actionProducts($year = 0, $section = 1) 
    {
        if(!$year) $year = date('Y') ;
        
        $productsArray = Product::searchAll() ;
        
        $dataProducts = [] ;
        
        $dataProducts[self::TOTALS] = ['max' => [], 'orders' => []] ;
        foreach($productsArray as $product) {
            $dataProducts[self::TOTALS]['max'][$product['name']] = 0 ;
            $dataProducts[self::TOTALS]['orders'][$product['name']] = 0 ;
        }
        
        if(!in_array($section, [1, 2, 3, 4])) {
            $section = 1 ;
        }
        
        $iStart = (3 * ($section - 1)) + 1 ;
        $iEnd = 3 * $section ;
        $empty = true ;
        for($i = $iStart; $i <= $iEnd; $i++) {
            // Maximums
            $resMaximums = Yii::$app->db->createCommand("SELECT product.name, SUM(IF(product_distribution.active, product_distribution.quantity_max,0)) AS total
                FROM distribution, product_distribution, product
                WHERE distribution.id_producer = ".GlobalParam::getCurrentProducerId()."
                AND distribution.date >= :date_begin
                AND distribution.date <= :date_end
                AND distribution.id = product_distribution.id_distribution
                AND product_distribution.id_product = product.id
                GROUP BY product.id
                ORDER BY product.name")
            ->bindValue(':date_begin', date($year.'-'.str_pad($i, 2, 0, STR_PAD_LEFT).'-01'))
            ->bindValue(':date_end', date($year.'-'.str_pad($i, 2, 0, STR_PAD_LEFT).'-31'))
            ->queryAll();
            
            $dataProducts[$i]['max'] = $resMaximums ;
            if(count($resMaximums)) $empty = false ;
            
            foreach($resMaximums as $productMax) {
                if(!isset($dataProducts[self::TOTALS]['max'][$productMax['name']])) {
                    $dataProducts[self::TOTALS]['max'][$productMax['name']] = 0 ;
                }
                $dataProducts[self::TOTALS]['max'][$productMax['name']] += $productMax['total'] ;
            }

            // Commandés
            $resOrders = Yii::$app->db->createCommand('
                SELECT product.name, SUM(product_order.quantity) AS total
                FROM `distribution`, `order`, `product_order`, `product`
                WHERE distribution.id_producer = '.GlobalParam::getCurrentProducerId().'
                AND distribution.date >= :date_begin
                AND distribution.date <= :date_end
                AND distribution.id = `order`.id_distribution
                AND `order`.id = product_order.id_order
                AND product_order.id_product = product.id
                GROUP BY product.id
                ORDER BY product.name')
            ->bindValue(':date_begin', date($year.'-'.str_pad($i, 2, 0, STR_PAD_LEFT).'-01'))
            ->bindValue(':date_end', date($year.'-'.str_pad($i, 2, 0, STR_PAD_LEFT).'-31'))
            ->queryAll();
            
            $dataProducts[$i]['orders'] = $resOrders;
            if(count($resOrders)) $empty = false ;
            
            foreach($resOrders as $productOrder) {
                if(!isset($dataProducts[self::TOTALS]['orders'][$productOrder['name']])) {
                    $dataProducts[self::TOTALS]['orders'][$productOrder['name']] = 0 ;
                }
                $dataProducts[self::TOTALS]['orders'][$productOrder['name']] += $productOrder['total'] ;
            }
            
        }
        
        ksort($dataProducts) ;
        
        $monthArray = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre','Totaux'] ;
        
        return $this->render('products', [
            'year' => $year,
            'monthArray' => $monthArray,
            'productsArray' => $productsArray,
            'dataProducts' => $dataProducts,
            'empty' => $empty,
            'section' => $section,
            'iStart' => $iStart,
            'iEnd' => $iEnd,
        ]);
    }

}

?>