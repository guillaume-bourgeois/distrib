<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use backend\models\MailForm ;
use yii\web\NotFoundHttpException ;
use common\models\User ;

/**
 * UserController implements the CRUD actions for User model.
 */
class CommunicateAdminController extends BackendController 
{

    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::getCurrentStatus() == USER::STATUS_ADMIN;
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * 
     * 
     * @return mixed
     */
    public function actionIndex($section = 'producers') 
    {
        if($section == 'producers') {
            $producers = Producer::find()->where(['producer.active' => 1])->with(['contact'])->all() ;
            $usersArray = [];
            $users = [] ;
            foreach ($producers as $producer) {
                if (isset($producer->contact) && is_array($producer->contact)) {
                    foreach($producer->contact as $contact) {
                        $usersArray[] = $contact->email ;
                        $users[] = [
                            'email' => $contact->email,
                            'name' => $contact->name,
                            'lastname' => $contact->lastname,
                        ] ;
                    }
                }
            }
        }
        elseif($section == 'users') {
            $users = User::find()
                ->where([
                    'user.status' => User::STATUS_ACTIVE
                ])
                ->all() ;
            $usersArray = [];
            foreach ($users as $user) {
                if (isset($user['email']) && strlen($user['email'])) {
                    $usersArray[] = $user['email'];
                }
            }
        }
        else {
            throw new NotFoundHttpException('Requête incorrecte.');
        }
        
        $mailForm = new MailForm() ;
        if ($mailForm->load(Yii::$app->request->post()) && $mailForm->validate()) {
            $resultSendEmail = $mailForm->sendEmail($users, false) ;
            if($resultSendEmail) {
                Yii::$app->getSession()->setFlash('success', 'Votre email a bien été envoyé.');
            }
            else {
                Yii::$app->getSession()->setFlash('error', 'Un problème est survenu lors de l\'envoi de votre email.');
            }
            $mailForm->subject = '' ;
            $mailForm->message = '' ;
        }
        
        return $this->render('index', [
            'section' => $section,
            'usersArray' => $usersArray,
            'mailForm' => $mailForm,
        ]);
    }

}
