<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\helpers\GlobalParam;
use common\models\Producer ;
use common\models\User ;
use common\models\UserSearch ;
use backend\models\AccessUserProducerForm ;

/**
 * UserController implements the CRUD actions for User model.
 */
class AccessController extends BackendController 
{

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend();
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Affiche les utilisateurs ayant accès à l'administration de ce producteur.
     * Gestion du formulaire permettant d'en ajouter de nouveaux.
     * 
     * @return string
     */
    public function actionIndex() 
    {
        $userSearch = new UserSearch ;
        $usersArray = $userSearch->search()->query->all() ;
        
        $modelAccessUserProducerForm = new AccessUserProducerForm ;
        if($modelAccessUserProducerForm->load(Yii::$app->request->post()) && $modelAccessUserProducerForm->save()) {
            Yii::$app->getSession()->setFlash('success', 'Droits ajoutés à l\'utilisateur');
        }
        
        $usersAccessArray = User::find()
            ->where([
                'id_producer' => GlobalParam::getCurrentProducerId(),
                'status' => User::STATUS_PRODUCER
            ])
            ->all() ;
        
        $producer = Producer::searchOne() ;

        return $this->render('index', [
            'usersArray' => $usersArray,
            'usersAccessArray' => $usersAccessArray,
            'producer' => $producer,
            'modelAccessUserProducerForm' => $modelAccessUserProducerForm,
        ]);
    }

    /**
     * 
     */
    public function actionDelete($idUser) 
    {
        $user = User::searchOne([
            'id' => $idUser
        ]) ;
        
        if($user) {
            $user->id_producer = 0 ;
            $user->status = User::STATUS_ACTIVE ;
            $user->save() ;
            Yii::$app->getSession()->setFlash('success', 'Droits de l\'utilisateur supprimé.');
        }
        
        return $this->redirect(['index']) ;
    }
    
}
