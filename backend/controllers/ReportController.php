<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\helpers\GlobalParam;
use Yii;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Distribution ;

class ReportController extends BackendController 
{
    var $enableCsrfValidation = false;

    public function behaviors() 
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend() ;
                        }
                    ]
                ],
            ],
        ];
    }
    
    public function actionIndex() 
    {
        $this->checkProductsPointsSale() ;
        return $this->render('index') ;
    }
    
    public function actionAjaxInit() 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
               
        $usersArray = User::findBy()->all() ;
        $pointsSaleArray = PointSale::searchAll() ;
        
        // distributions
        $firstDistribution = Distribution::searchOne([], [
            'orderby' => 'date ASC'
        ]) ;
        $lastDistribution = Distribution::searchOne([], [
            'orderby' => 'date DESC'
        ]) ;
        
        $firstYear = date('Y',strtotime($firstDistribution->date)) ;
        $lastYear = date('Y',strtotime($lastDistribution->date)) ;
        $distributionYearsArray = [] ;
        for($year = $firstYear; $year <= $lastYear; $year ++) {
            $distributionYearsArray[] = $year ;
        }
        
        $distributionsArray = Distribution::searchAll([
            'distribution.active' => 1
        ], [
            'orderby' => 'date ASC',
        ]) ;
        $distributionsByMonthArray = [] ;
        foreach($distributionsArray as $distribution) {
            $month = date('Y-m', strtotime($distribution->date)) ;
            if(!isset($distributionsByMonthArray[$month])) {
                $distributionsByMonthArray[$month] = [
                    'display' => 0,
                    'year' => date('Y',strtotime($distribution->date)),
                    'month' => strftime('%B', strtotime($distribution->date)),
                    'distributions' => []
                ] ;
            }
            $distribution->date = strftime('%A %d %B %Y', strtotime($distribution->date)) ;
            $distributionsByMonthArray[$month]['distributions'][] = $distribution ;
        }
        
        return  [
            'usersArray' => $usersArray,
            'pointsSaleArray' => $pointsSaleArray,
            'distributionYearsArray' => $distributionYearsArray,
            'distributionsByMonthArray' => $distributionsByMonthArray
        ] ;
    }
    
    public function actionAjaxReport() 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $posts = Yii::$app->request->post();
        
        $resArray = [] ;

        $conditionUsers = $this->_generateConditionSqlReport($posts, 'users', 'id_user') ;
        $conditionPointsSale = $this->_generateConditionSqlReport($posts, 'pointsSale', 'id_point_sale') ;
        $conditionDistributions = $this->_generateConditionSqlReport($posts, 'distributions', 'id_distribution') ;
        
        $res = Yii::$app->db->createCommand("SELECT product.name, SUM(product_order.quantity) AS quantity, SUM(product_order.price * product_order.quantity) AS total
            FROM `order`, product_order, product
            WHERE `order`.id = product_order.id_order
            AND product.id_producer = ".((int) GlobalParam::getCurrentProducerId()) ."
            AND product_order.id_product = product.id
            AND `order`.date_delete IS NULL
            ".$conditionUsers."
            ".$conditionPointsSale."
            ".$conditionDistributions."
            GROUP BY product.id
            ORDER BY product.order ASC
        ")
        ->queryAll();

        $totalGlobal = 0 ;
        foreach($res as $line) {
            $total = Price::format(round($line['total'], 2)) ;
            if($line['quantity'] > 0) {
                $resArray[] = [
                    'name' => $line['name'],
                    'quantity' => $line['quantity'],
                    'total' => $total,
                ] ;
                $totalGlobal += $line['total'] ;
            }
        }
        
        $resArray[] = [
            'name' => '',
            'quantity' => '',
            'total' => '<strong>'.Price::format(round($totalGlobal, 2)).'</strong>',
        ] ;
        
        return $resArray ;
    }
    
    public function _generateConditionSqlReport($posts, $name, $fieldOrder) 
    {
        $condition = '' ;
        if(isset($posts[$name]) && strlen($posts[$name])) {
            $idsArray = explode(',', $posts[$name]) ;
            for($i = 0; $i < count($idsArray); $i++) {
                $idsArray[$i] = (int) $idsArray[$i] ;
            }
            $condition = 'AND `order`.'.$fieldOrder.' IN ('.implode(',',$idsArray).') ' ;
        }
        return $condition ;
    }
    
}