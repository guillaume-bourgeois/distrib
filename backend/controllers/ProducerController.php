<?php

/**
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
 */

namespace backend\controllers;

use common\helpers\GlobalParam;
use Yii;
use common\models\User;
use backend\models\MailForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Upload;
use common\models\Producer;
use common\models\Invoice;

/**
 * UserController implements the CRUD actions for User model.
 */
class ProducerController extends BackendController
{

        public $enableCsrfValidation = false;

        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                        'delete' => ['post'],
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::hasAccessBackend();
                                                }
                                        ]
                                ],
                        ],
                ];
        }

        /**
         * Modifie un producteur.
         *
         * @return mixed
         */
        public function actionUpdate()
        {
                $request = Yii::$app->request;
                $model = $this->findModel(GlobalParam::getCurrentProducerId());
                $model->secret_key_payplug = $model->getSecretKeyPayplug() ;
                $logoFilenameOld = $model->logo;
                $photoFilenameOld = $model->photo;
                if (strlen($model->option_dashboard_date_start)) {
                        $model->option_dashboard_date_start = date('d/m/Y', strtotime($model->option_dashboard_date_start));
                }
                if (strlen($model->option_dashboard_date_end)) {
                        $model->option_dashboard_date_end = date('d/m/Y', strtotime($model->option_dashboard_date_end));
                }

                if ($model->load(Yii::$app->request->post()) && $model->save()) {

                        if(strlen($model->option_dashboard_date_start)) {
                                $model->option_dashboard_date_start = date(
                                        'Y-m-d',
                                        strtotime(str_replace('/', '-', $model->option_dashboard_date_start)
                                ));
                                $model->save() ;
                        }

                        if(strlen($model->option_dashboard_date_end)) {
                                $model->option_dashboard_date_end = date(
                                        'Y-m-d',
                                        strtotime(str_replace('/', '-', $model->option_dashboard_date_end))
                                );
                                $model->save() ;
                        }

                        Upload::uploadFile($model, 'logo', $logoFilenameOld);
                        Upload::uploadFile($model, 'photo', $photoFilenameOld);

                        $deleteLogo = $request->post('delete_logo', 0);
                        if ($deleteLogo) {
                                $model->logo = '';
                                $model->save();
                        }

                        $deletePhoto = $request->post('delete_photo', 0);
                        if ($deletePhoto) {
                                $model->photo = '';
                                $model->save();
                        }

                        $model->saveSecretKeyPayplug() ;

                        Yii::$app->getSession()->setFlash('success', 'Paramètres mis à jour.');
                        return $this->redirect(['update', 'id' => $model->id, 'edit_ok' => true]);
                } else {
                        if($model->load(Yii::$app->request->post())) {
                                Yii::$app->getSession()->setFlash('error', 'Le formulaire comporte des erreurs.');
                        }
                        return $this->render('update', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Affiche le formulaire permettant au producteur de définir le montant
         * de son abonnement.
         *
         * @return mixed
         */
        public function actionBilling()
        {
                $datasInvoices = new ActiveDataProvider([
                        'query' => Invoice::find()
                                ->where(['id_producer' => GlobalParam::getCurrentProducerId()])
                                ->orderBy('reference DESC'),
                        'pagination' => [
                                'pageSize' => 1000,
                        ],
                ]);

                $producer = Producer::findOne(GlobalParam::getCurrentProducerId());

                if ($producer->load(Yii::$app->request->post())) {
                        $producer->save();

                        if (!is_null($producer->free_price)) {
                                $alertFreeprice = true;
                        }
                }

                return $this->render('billing', [
                        'datasInvoices' => $datasInvoices,
                        'producer' => $producer,
                        'alertFreePrice' => (isset($alertFreeprice)) ? true : false
                ]);
        }

        /**
         * Recherche un établissement via son ID.
         *
         * @param integer $id
         * @return Etablissement
         * @throws NotFoundHttpException
         */
        protected function findModel($id)
        {
                if (($model = Producer::findOne($id)) !== null) {
                        return $model;
                } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
        }

}
