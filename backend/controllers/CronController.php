<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use common\models\Producer;
use common\models\Order;
use common\models\Subscription;
use common\models\Distribution;
use common\models\CreditHistory;

/**
 * UserController implements the CRUD actions for User model.
 */
class CronController extends BackendController
{
        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                        'delete' => ['post'],
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['?'],
                                        ],
                                        [
                                                'actions' => ['pay-orders'],
                                                'allow' => true,
                                                'roles' => ['@']
                                        ],
                                ],
                        ],
                ];
        }

        /**
         * Initialise le compte de démonstration.
         *
         * @param string $key
         */
        public function actionInitDemo($key = '')
        {
                if ($key == '45432df6e842ac71aa0b5bb6b9f25d44') {
                        $producer = Producer::getDemoAccount();

                        if ($producer) {
                                // initialisation de la distribution à J+7
                                $dateTime = strtotime("+7 day");
                                $dayStr = strtolower(date('l', $dateTime));
                                $fieldDeliveryDay = 'delivery_' . $dayStr;
                                $pointsSaleArray = PointSale::searchAll(['point_sale.id_producer' => $producer->id]);
                                $activeDistribution = false;
                                foreach ($pointsSaleArray as $pointSale) {
                                        if ($pointSale->$fieldDeliveryDay) {
                                                $activeDistribution = true;
                                        }
                                }

                                if ($activeDistribution) {
                                        $distribution = Distribution::initDistribution(date('Y-m-d', $dateTime), $producer->id);
                                        $distribution->active(true);
                                }
                        }
                }

        }

        public function actionPayOrders($date)
        {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if(strlen($date)) {
                        $this->actionProcessOrders('64ac0bdab7e9f5e48c4d991ec5201d57', $date) ;
                }

                return [
                        'return' => 'success',
                        'alert' => [
                                'type' => 'success',
                                'message' => 'Commandes payées.'
                        ]
                ] ;
        }

        public function actionForceProcessOrders($key = '')
        {
                $this->actionProcessOrders($key, date('Y-m-d', strtotime('-5 day'))) ;
        }

        /**
         * Routine quotidienne concernant les commandes : paiement et envoi d'un
         * récap aux producteurs.
         *
         * @param string $key
         * @param string $forceDate
         */
        public function actionProcessOrders($key = '', $forceDate = '')
        {
                if ($key == '64ac0bdab7e9f5e48c4d991ec5201d57') {

                        ini_set('display_errors', 1);
                        ini_set('display_startup_errors', 1);
                        error_reporting(E_ALL);

                        Yii::error('Cron process orders', 'log-cron');
                        $hour = 20;
                        if (strlen($forceDate)) {
                                $date = $forceDate;
                        } else {
                                $hour = date('H');

                                if ($hour == '00') {
                                        $date = date('Y-m-d');
                                        $hour = 24;
                                } else {
                                        $date = date('Y-m-d', time() + 24 * 60 * 60);
                                }
                        }

                        $arrayProducers = Producer::searchAll();

                        foreach ($arrayProducers as $producer) {

                                $countOrders = 0;
                                $mailOrdersSend = false;

                                $distribution = Distribution::findOne([
                                        'date' => $date,
                                        'active' => 1,
                                        'id_producer' => $producer->id,
                                ]);

                                if($distribution) {

                                        if ($hour == $producer->order_deadline || strlen($forceDate)) {

                                                /*
                                                 * Paiement des commandes (paiement automatique)
                                                 */

                                                $arrayOrders = Order::searchAll([
                                                        'distribution.date' => $date,
                                                        'distribution.id_producer' => $producer->id
                                                ], [
                                                        'conditions' => 'date_delete IS NULL'
                                                ]);

                                                $configCredit = Producer::getConfig('credit', $producer->id);

                                                if ($arrayOrders && is_array($arrayOrders)) {
                                                        foreach ($arrayOrders as $order) {
                                                                if ($order->auto_payment && $configCredit) {

                                                                        if ($order->getAmount(Order::AMOUNT_REMAINING) > 0) {
                                                                                $order->saveCreditHistory(
                                                                                        CreditHistory::TYPE_PAYMENT,
                                                                                        $order->getAmount(Order::AMOUNT_REMAINING),
                                                                                        $order->distribution->id_producer,
                                                                                        $order->id_user,
                                                                                        User::ID_USER_SYSTEM
                                                                                );
                                                                                $countOrders++;
                                                                        }
                                                                }
                                                        }
                                                }

                                                /*
                                                 * Envoi des commandes par email au producteur
                                                 */
                                                if (!strlen($forceDate)) {
                                                        $arrayOrders = Order::searchAll([
                                                                'distribution.date' => $date,
                                                                'distribution.id_producer' => $producer->id
                                                        ], [
                                                                'conditions' => 'date_delete IS NULL'
                                                        ]);

                                                        $user = User::searchOne([
                                                                'id_producer' => $producer->id,
                                                                'status' => User::STATUS_PRODUCER
                                                        ]);

                                                        $mail = Yii::$app->mailer->compose(
                                                                [
                                                                        'html' => 'cronOrdersSummary-html',
                                                                        'text' => 'cronOrdersSummary-text',
                                                                ], [
                                                                        'date' => $date,
                                                                        'orders' => $arrayOrders
                                                                ]
                                                        )
                                                                ->setTo($user->email)
                                                                ->setFrom([Yii::$app->params['adminEmail'] => 'distrib']);

                                                        if (is_array($arrayOrders) && count($arrayOrders)) {
                                                                $subject = '[distrib] Commandes du ' . date('d/m', strtotime($date));

                                                                // génération du pdf de commande
                                                                Yii::$app->runAction('distribution/report-cron', [
                                                                        'date' => $date,
                                                                        'save' => true,
                                                                        'idProducer' => $producer->id,
                                                                        'key' => '64ac0bdab7e9f5e48c4d991ec5201d57'
                                                                ]);
                                                                $mail->attach(Yii::getAlias('@app/web/pdf/Commandes-' . $date . '-' . $producer->id . '.pdf'));
                                                        } else {
                                                                $subject = '[distrib] Aucune commande';
                                                        }

                                                        $mail->setSubject($subject)
                                                                ->send();
                                                        $mailOrdersSend = true;

                                                        /*$mail->setTo('contact@opendistrib.net')
                                                                ->send();*/
                                                }

                                                if ($producer->active) {
                                                        $messageLog = $producer->name . ' : Distribution du ' . $date . ', ' . count($arrayOrders) . ' commande(s) enregistrée(s), ' . $countOrders . ' commande(s) payée(s), ' . ($mailOrdersSend ? 'Récapitulatif de commandes envoyé' : 'Aucun email envoyé');

                                                        /*Yii::$app->mailer->compose()
                                                                ->setFrom('contact@opendistrib.net')
                                                                ->setTo('contact@opendistrib.net')
                                                                ->setSubject('[Opendistrib] Log '.$producer->name)
                                                                ->setTextBody($messageLog)
                                                                ->send();*/

                                                }
                                        }
                                }
                        }
                }
        }
}
