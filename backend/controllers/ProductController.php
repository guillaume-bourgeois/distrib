<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use common\helpers\GlobalParam;
use common\models\ProductDistribution;
use common\models\ProductPrice;
use common\models\ProductPriceSearch;
use common\models\ProductSearch;
use common\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use common\models\Product;
use common\models\Distribution;
use common\models\User;
use common\models\UserProducer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\helpers\Upload;

/**
 * ProduitController implements the CRUD actions for Produit model.
 */
class ProductController extends BackendController
{
        var $enableCsrfValidation = false;

        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::hasAccessBackend();
                                                }
                                        ]
                                ],
                        ],
                ];
        }

        /**
         * Liste les modèles Produit.
         *
         * @return mixed
         */
        public function actionIndex()
        {
                $searchModel = new ProductSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                ]);
        }

        /**
         * Crée un modèle Produit.
         * Si la création réussit, le navigateur est redirigé vers la page 'index'.
         *
         * @return mixed
         */
        public function actionCreate()
        {
                $model = new Product();
                $model->active = 1;
                $model->id_producer = GlobalParam::getCurrentProducerId();

        $model->monday = 1 ;
        $model->tuesday = 1 ;
        $model->wednesday = 1 ;
        $model->thursday = 1 ;
        $model->friday = 1 ;
        $model->saturday = 1 ;
        $model->sunday = 1 ;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $lastProductOrder = Product::find()->where('id_producer = :id_producer')->params([':id_producer' => GlobalParam::getCurrentProducerId()])->orderBy('order DESC')->one() ;
            if($lastProductOrder) {
                $model->order = ++ $lastProductOrder->order ;
            }
            
            Upload::uploadFile($model, 'photo');
            $model->save();

            // link product / distribution
            Distribution::linkProductIncomingDistributions($model) ;

            Yii::$app->getSession()->setFlash('success', 'Produit <strong>'.Html::encode($model->name).'</strong> ajouté');

            return $this->redirect(['index']);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

        /**
         * Modifie un modèle Produit existant.
         * Si la modification réussit, le navigateur est redirigé vers la page 'index'.
         *
         * @param integer $id
         * @return mixed
         */
        public function actionUpdate($id)
        {
                $request = Yii::$app->request;

                $model = $this->findModel($id);
                $photoFilenameOld = $model->photo;

                if ($model->load(Yii::$app->request->post()) && $model->save()) {

                        Upload::uploadFile($model, 'photo', $photoFilenameOld);

                        $deletePhoto = $request->post('delete_photo', 0);
                        if ($deletePhoto) {
                                $model->photo = '';
                                $model->save();
                        }

                        if ($model->apply_distributions) {
                                // link product / distribution
                                Distribution::linkProductIncomingDistributions($model);
                        }

                        Yii::$app->getSession()->setFlash('success', 'Produit <strong>' . Html::encode($model->name) . '</strong> modifié');
                        return $this->redirect(['index']);
                }

                return $this->render('update/update', [
                        'model' => $model,
                        'action' => 'update',
                ]);
        }

        public function actionPricesList($id)
        {
                $request = Yii::$app->request;
                $model = $this->findModel($id);

                $searchModel = new ProductPriceSearch();
                $searchModel->id_product = $id ;

                $dataProvider = $searchModel->search(array_merge(Yii::$app->request->queryParams, [
                        'id_product' => $id
                ]));

                $userProducerWithProductPercent = UserProducer::searchAll([], [
                        'join_with' => ['user'],
                        'conditions' => 'user_producer.product_price_percent != 0',
                ]) ;

                $pointSaleWithProductPercent = PointSale::searchAll([], [
                        'conditions' => 'point_sale.product_price_percent != 0'
                ]) ;

                return $this->render('update/prices/list', [
                        'model' => $model,
                        'action' => 'prices-list',
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'userProducerWithProductPercent' => $userProducerWithProductPercent,
                        'pointSaleWithProductPercent' => $pointSaleWithProductPercent,
                ]);
        }

        public function actionPricesCreate($idProduct)
        {
                $model = new ProductPrice();
                $model->id_product = $idProduct;
                $modelProduct = $this->findModel($idProduct) ;

                if ($model->load(Yii::$app->request->post())) {

                        $conditionsProductPriceExist = [
                                'id_product' => $idProduct,
                                'id_user' => $model->id_user ? $model->id_user : null,
                                'id_point_sale' => $model->id_point_sale ? $model->id_point_sale : null,
                        ] ;

                        $productPriceExist = ProductPrice::findOne($conditionsProductPriceExist) ;

                        if($productPriceExist) {
                                $productPriceExist->delete() ;
                                Yii::$app->getSession()->setFlash('warning', 'Un prix existait déjà pour cet utilisateur / point de vente, il a été supprimé.');
                        }

                        if($model->save()) {
                                Yii::$app->getSession()->setFlash('success', 'Le prix a bien été ajouté.');
                                return $this->redirect(['product/prices-list', 'id' => $idProduct]);
                        }
                }

                return $this->render('update/prices/create', [
                        'model' => $model,
                        'modelProduct' => $modelProduct,
                ]);
        }

        public function actionPricesUpdate($id)
        {
                $request = Yii::$app->request;

                $model = $this->findModelProductPrice($id);
                $modelProduct = $this->findModel($model->id_product) ;

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        Yii::$app->getSession()->setFlash('success', 'Prix modifié');
                        return $this->redirect(['product/prices-list', 'id' => $model->id_product]);
                }

                return $this->render('update/prices/update', [
                        'model' => $model,
                        'modelProduct' => $modelProduct,
                        'action' => 'prices-update',
                ]);
        }

        public function actionPricesDelete($id)
        {
                $productPrice = $this->findModelProductPrice($id);
                $productPrice->delete();
                Yii::$app->getSession()->setFlash('success', 'Prix supprimé');
                return $this->redirect(['product/prices-list', 'id' => $productPrice->id_product]);
        }

        /**
         * Supprime un modèle Produit.
         * Si la suppression réussit, le navigateur est redirigé vers  la page
         * 'index'.
         *
         * @param integer $id
         * @return mixed
         */
        public function actionDelete($id, $confirm = false)
        {
                $product = $this->findModel($id);

                if ($confirm) {
                        $product->delete();
                        ProductDistribution::deleteAll(['id_product' => $id]);
                        Yii::$app->getSession()->setFlash('success', 'Produit <strong>' . Html::encode($product->name) . '</strong> supprimé');
                } else {
                        Yii::$app->getSession()->setFlash('info', 'Souhaitez-vous vraiment supprimer le produit <strong>' . Html::encode($product->name) . '</strong> ? '
                                . Html::a('Oui', ['product/delete', 'id' => $id, 'confirm' => 1], ['class' => 'btn btn-default']) . ' ' . Html::a('Non', ['product/index'], ['class' => 'btn btn-default']));
                }

                return $this->redirect(['index']);
        }

        /**
         * Modifie l'ordre des produits.
         *
         * @param array $array
         */
        public function actionOrder()
        {
                $array = Yii::$app->request->post('array');
                $orderArray = json_decode(stripslashes($array));

                foreach ($orderArray as $id => $order) {
                        $product = $this->findModel($id);
                        $product->order = $order;
                        $product->save();
                }
        }

        /**
         * Recherche un produit en fonction de son ID.
         *
         * @param integer $id
         * @return Produit
         * @throws NotFoundHttpException si le modèle n'est pas trouvé
         */
        protected function findModel($id)
        {
                if (($model = Product::findOne($id)) !== null) {
                        return $model;
                } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
        }

        protected function findModelProductPrice($id)
        {
                if (($model = ProductPrice::findOne($id)) !== null) {
                        return $model;
                } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
        }

}
