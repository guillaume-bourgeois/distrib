<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

?>

<div id="menu-users">
    <div id="nav-points-sale">
        <a class="btn <?php if(!$idPointSaleActive && !$sectionInactiveUsers && !$sectionSubscribers): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/'.$section]); ?>">
            <span class="glyphicon glyphicon-th-list"></span> Tous <span class="glyphicon glyphicon-triangle-bottom"></span>
        </a>
        <?php if(isset($sectionSubscribers) && !is_null($sectionSubscribers)): ?>
            <a class="btn <?php if($sectionSubscribers): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/'.$section,'sectionSubscribers' => 1]); ?>">
                <span class="glyphicon glyphicon-repeat"></span>
                Abonnés 
                <span class="glyphicon glyphicon-triangle-bottom"></span>
            </a>
        <?php endif; ?>
        <?php if(isset($sectionInactiveUsers) && !is_null($sectionInactiveUsers)): ?>
            <a class="btn <?php if($sectionInactiveUsers): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/'.$section,'sectionInactiveUsers' => 1]); ?>">
                <span class="glyphicon glyphicon-time"></span>
                Inactifs 
                <span class="glyphicon glyphicon-triangle-bottom"></span>
            </a>
        <?php endif; ?>
        <?php foreach($pointsSaleArray as $pointSale): ?>
            <a class="btn <?php if($idPointSaleActive == $pointSale->id): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/'.$section,'idPointSale' => $pointSale->id]); ?>">
                <span class="glyphicon glyphicon-map-marker"></span>
                <?= Html::encode($pointSale->name) ?>
                <span class="glyphicon glyphicon-triangle-bottom"></span>
            </a>
        <?php endforeach; ?>
    </div>

    <div id="submenu">
        <a class="btn btn-xs <?php if($section == 'index'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/index','idPointSale' => $idPointSaleActive]); ?>">
            <span class="glyphicon glyphicon-th-list"></span> Liste
        </a>
        <a class="btn btn-xs <?php if($section == 'mail'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['user/mail','idPointSale' => $idPointSaleActive]); ?>">
            <span class="glyphicon glyphicon-envelope"></span> Envoyer un email
        </a>
    </div>
</div>
