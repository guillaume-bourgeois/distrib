<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CreditHistory; 
use common\models\Producer;
use common\models\Order;

$this->setTitle('Commandes <small>'.Html::encode($user->lastname.' '.$user->name).'</small>', 'Commandes de '.Html::encode($user->lastname.' '.$user->name)) ;
$this->addBreadcrumb(['label' => 'Utilisateurs', 'url' => ['index']]) ;
$this->addBreadcrumb(['label' => Html::encode($user->lastname.' '.$user->name)]) ;
$this->addBreadcrumb('Commandes') ;

?>

<div class="user-orders">
    <?php if(count($ordersArray)): ?>
    <table id="historique-commandes" class="table table-striped table-bordered">
        <thead>
            <tr>                 
                <th>Date livraison</th>
                <th>Historique</th>
                <th>Résumé</th>
                <th>Point de vente</th>
                <th class="montant">Montant</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($ordersArray as $order): ?>
            <tr class="<?= $order->getClassHistory() ; ?>">
                <td><?php echo date('d/m/Y',strtotime($order->distribution->date)); ?></td>
                <td class="historique"><?= $order->getStrHistory() ; ?></td>
                <td class="resume"><?= $order->getCartSummary() ; ?></td>
                <td><?= $order->getPointSaleSummary(); ?></td>
                <td class="montant"><?= $order->getAmountSummary(); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table> 
    <?php else: ?>
        <div class="alert alert-warning">Aucune commande passée par ce client.</div>
    <?php endif; ?>
</div>
