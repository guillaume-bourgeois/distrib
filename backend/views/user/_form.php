<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ProductPrice ;

\backend\assets\VuejsUserFormAsset::register($this);

?>

<div class="user-form" id="app-user-form">

        <div class="col-md-8">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false
                ]); ?>

                <?= $form->field($model, 'type')
                        ->dropDownList([
                                User::TYPE_INDIVIDUAL => 'Particulier',
                                User::TYPE_LEGAL_PERSON => 'Personne morale',
                                User::TYPE_GUEST => 'Visiteur'
                        ], [
                            'v-model' => 'type'
                        ]) ; ?>
                <?= $form->field($model, 'name_legal_person', ['options' => ['v-show' => "type == 'legal-person'"]])->textInput() ?>
                <?= $form->field($model, 'lastname')->textInput() ?>
                <?= $form->field($model, 'name')->textInput() ?>
                <?= $form->field($model, 'phone')->textInput() ?>
                <?= $form->field($model, 'email')->textInput() ?>
                <?= $form->field($model, 'address')->textarea() ?>
                <?= $form->field($model, 'points_sale')->checkboxlist(
                        ArrayHelper::map($pointsSaleArray, 'id', function ($pointSale) use ($model) {
                                $commentUserPointSale = isset($pointSale->userPointSale[0]) ? $pointSale->userPointSale[0]->comment : '';
                                $html = Html::encode($pointSale->name);
                                if ($pointSale->restricted_access) {
                                        $html .= '<input type="text" placeholder="Commentaire" class="form-control" name="User[comment_point_sale_' . $pointSale->id . ']" value="' . (($model->id) ? Html::encode($commentUserPointSale) : '') . '" />';
                                }
                                return $html;
                        }), [
                        'encode' => false
                ]);
                ?>

                <?= $form->field($model, 'user_groups')->checkboxlist(
                        ArrayHelper::map($userGroupsArray, 'id', function ($userGroup) use ($model) {
                                return Html::encode($userGroup->name);
                        }), [
                        'encode' => false
                ]);
                ?>

                <?php /* $form->field($model, 'product_price_percent')
                        ->dropDownList(ProductPrice::percentValues(), [])->hint('Pourcentage appliqué aux prix de chaque produit pour cet utilisateur.');*/ ?>

                <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Ajouter' : 'Modifier', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
        </div>

        <div class="col-md-4">
                <h2>Mot de passe</h2>
                <?php $form = ActiveForm::begin(); ?>
                <div class="form-group">
                        <?= Html::submitButton('Envoyer un nouveau mot de passe', ['class' => 'btn btn-primary', 'name' => 'submit_new_password', 'value' => 1]) ?>
                </div>
                <?php ActiveForm::end(); ?>
        </div>

</div>
