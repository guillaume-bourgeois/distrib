<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Development;
use common\models\DevelopmentPriority;
use common\models\User;
use common\helpers\Url;
use common\helpers\GlobalParam;

$this->setTitle('Développement');
$this->addButton(['label' => 'Nouveau développement <span class="glyphicon glyphicon-plus"></span>', 'url' => ['development/create'], 'class' => 'btn btn-primary']);
$this->addBreadcrumb($this->getTitle());

?>

<div class="development-index">
    <div class="well">
        Cette page liste les besoins recencés auprès des producteurs utilisant la plateforme. N'hésitez pas à me <a
                href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/contact']); ?>">contacter</a> pour la
        faire évoluer. Les remontées de bugs sont également bienvenues.<br/>
        Afin d'orienter de manière pertinente le développement de la plateforme, je vous invite à définir la priorité
        des évolutions qui vous intéressent.
    </div>

    <ul id="tab-status-developments" class="nav nav-tabs" role="tablist">
        <li role="presentation" class="<?php if ($status == Development::STATUS_OPEN): ?>active<?php endif; ?>"><a
                    href="<?= Yii::$app->urlManager->createUrl(['development/index', 'status' => Development::STATUS_OPEN]); ?>"
                    id="" aria-controls="" role="tab">Ouvert</a></li>
        <li role="presentation" class="<?php if ($status == Development::STATUS_CLOSED): ?>active<?php endif; ?>"><a
                    href="<?= Yii::$app->urlManager->createUrl(['development/index', 'status' => Development::STATUS_CLOSED]); ?>"
                    id="" aria-controls="" role="tab">Fermé</a></li>
    </ul>

        <?php

        $columns = [
                [
                        'header' => '#',
                        'value' => function ($model) {
                                return '#' . $model->id;
                        }
                ],
                [
                        'attribute' => 'type',
                        'header' => 'Type',
                        'format' => 'raw',
                        'value' => function ($model) {
                                if ($model->type == Development::TYPE_EVOLUTION) {
                                        return '<span class="label label-success">Évolution</span>';
                                } else {
                                        return '<span class="label label-danger">Anomalie</span>';
                                }
                        }
                ],
                ['attribute' => 'sujet',
                        'format' => 'raw',
                        'value' => function ($model) {
                                $html = '<strong>' . Html::encode($model->subject) . '</strong>';
                                if (strlen($model->description))
                                        $html .= '<br />' . nl2br(Html::encode($model->description));
                                return $html;
                        }],
                ['attribute' => 'estimation_temps',
                        'header' => 'Estimation',
                        'format' => 'raw',
                        'value' => function ($model) {
                                return intval($model->time_estimate) . ' h';
                        }],
                ['attribute' => 'avancement',
                        'format' => 'raw',
                        'value' => function ($model) {
                                if ($model->progress)
                                        return '<div class="progress">
<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="' . intval($model->progress) . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . intval($model->progress) . '%;">
<span class="sr-only">' . intval($model->progress) . '% effectué</span>
</div>
</div> ';
                                else
                                        return '';
                        }],
                ['attribute' => 'date_delivery',
                        'format' => 'raw',
                        'value' => function ($model) {
                                if (strlen($model->date_delivery))
                                        return date('d/m/Y', strtotime($model->date_delivery));
                                else
                                        return '';
                        }],
        ];


        if (User::hasAccessBackend()) {

                $columns[] = [
                        'header' => 'Priorité',
                        'format' => 'raw',
                        'value' => function ($model) {

                                $currentPriority = (isset($model->developmentPrioritYCurrentProducer)) ? $model->developmentPriorityCurrentProducer->getStrPriority() : 'Non';
                                $styleButton = (isset($model->developmentPriorityCurrentProducer)) ? $model->developmentPriorityCurrentProducer->getClassCssStyleButton() : 'default';

                                $html = '<div class="btn-group btn-group-priorite">
                        <button type="button" class="btn btn-priorite btn-sm btn-' . $styleButton . ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          ' . $currentPriority . ' <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="' . Yii::$app->urlManager->createUrl(['development/priority', 'idDevelopment' => $model->id]) . '">Non</a></li>
                            <li><a href="' . Yii::$app->urlManager->createUrl(['development/priority', 'idDevelopment' => $model->id, 'priority' => DevelopmentPriority::PRIORITY_LOW]) . '">Basse</a></li>
                            <li><a href="' . Yii::$app->urlManager->createUrl(['development/priority', 'idDevelopment' => $model->id, 'priority' => DevelopmentPriority::PRIORITY_NORMAL]) . '">Normale</a></li>
                            <li><a href="' . Yii::$app->urlManager->createUrl(['development/priority', 'idDevelopment' => $model->id, 'priority' => DevelopmentPriority::PRIORITY_HIGH]) . '">Haute</a></li>
                        </ul>
                      </div><br />';

                                if (isset($model->developmentPriority) && count($model->developmentPriority)) {
                                        foreach ($model->developmentPriority as $developmentPriority) {
                                                if ($developmentPriority->id_producer != GlobalParam::getCurrentProducerId())
                                                        $html .= '<div class="label label-priorite label-sm label-' . $developmentPriority->getClassCssStyleButton() . '">' . Html::encode($developmentPriority->producer->name) . '</div> ';
                                        }
                                }

                                return $html;
                        }
                ];
        }

        if (User::getCurrentStatus() == USER::STATUS_ADMIN) {
                $columns[] = [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update}',
                        'headerOptions' => ['class' => 'actions'],
                        'buttons' => [
                                'update' => function ($url, $model) {
                                        return '<div class="btn-group">
                            <a href="' . $url . '" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span> Modifier</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="' . Yii::$app->urlManager->createUrl(['development/delete', 'id' => $model->id]) . '" class=""><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li>
                            </ul>
                          </div>';
                                },
                        ],
                ];
        }

        ?>

        <?=
        GridView::widget([
                'id' => 'tab-developments',
                'dataProvider' => $dataProvider,
                'columns' => $columns
        ]);
        ?>
</div>
