<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html ;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper ;

$this->setTitle('Accès') ;

?>

<div class="alert alert-info">Sont listés ici les comptes utilisateurs ayant accès à l'administration.</div>

<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Ajouter un utilisateur</h3>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($modelAccessUserProducerForm,  'id_user')
                ->dropDownList(ArrayHelper::map($usersArray, 'id', function($model) { return $model->lastname.' '.$model->name; }))
                ->label(''); ?>
            <?= Html::submitButton('Ajouter', ['class' => 'btn btn-success']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div class="col-md-8">
<?php if(!count($usersAccessArray)): ?>
    <div class="alert alert-warning">Aucun utilisateur n'est défini en tant que producteur.</div>
<?php else: ?>
    <table class="table table-bordered">
        <tr>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        <?php foreach($usersAccessArray as $user): ?>
        <tr>
            <td><?= Html::encode($user->lastname.' '.$user->name) ?></td>
            <td><?= Html::a('<span class="glyphicon glyphicon-trash"></span>',['access/delete','idUser' => $user->id], ['class' => 'btn btn-default']); ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
</div>
