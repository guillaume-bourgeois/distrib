<tr class="<?php if(isset($displayOrders) && $displayOrders): ?>order<?php endif; ?>">
        <td class="align-left">
                <?= Html::encode($productOrder->product->name) ?>
                <?php if($productOrder->unit == 'piece' && isset($productOrder->product->weight) && $productOrder->product->weight): ?>
                        <span class="weight"> / <?= $productOrder->product->weight ?> g</span>
                <?php endif; ?>
                <?php if(strlen($productOrder->product->description)): ?>
                        <br /><small><?= Html::encode($productOrder->product->description) ?></small>
                <?php endif; ?>
        </td>

        <?php
            $price = $productOrder->getPrice() ;
            if($document->isInvoicePrice() && $productOrder->getInvoicePrice()) {
                    $price = $productOrder->getInvoicePrice() ;
            }
        ?>

        <?php if($displayPrices): ?>
                <td class="align-center">
                        <?= Price::format($price) ?>
                </td>
        <?php endif; ?>
        <td class="align-center"><?= $productOrder->quantity * Product::$unitsArray[$productOrder->unit]['coefficient'] ?></td>
        <td class="align-center"><?= Product::strUnit($productOrder->unit, 'wording') ?></td>
        <?php if($displayPrices): ?>
                <?php if(GlobalParam::getCurrentProducer()->taxRate->value != 0): ?>
                        <td class="align-center"><?= $productOrder->taxRate->value * 100 ?> %</td>
                <?php endif; ?>
                <td class="align-center">
                        <?php if($document->getClass() == ''): ?>
                                <?= Price::format($price * $productOrder->quantity) ?>
                        <?php else: ?>
                                <?= Price::format($price * $productOrder->quantity) ?>
                        <?php endif; ?>
                </td>
        <?php endif; ?>
</tr>
