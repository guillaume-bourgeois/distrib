<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper ;
use common\models\User ;
use common\models\PointSale ;
use common\helpers\GlobalParam ;

\backend\assets\VuejsSubscriptionFormAsset::register($this);

?>

<div class="subscription-form" id="app-subscription-form">
<?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <?php if($model->id): ?>
        <?= $form->field($model, 'id')->hiddenInput() ?>
    <?php endif; ?>
    <div class="col-md-5" id="bloc-select-user">
        <?= $form->field($model, 'id_user')->dropDownList( ArrayHelper::map(User::find()->joinWith('userProducer')->where('user_producer.id_producer = '.GlobalParam::getCurrentProducerId())->andWhere('user_producer.active = 1')->orderBy('lastname ASC, name ASC')->all(), 'id', function($model, $defaultValue) {
                if (isset($model['name_legal_person']) && strlen($model['name_legal_person'])) {
                        return 'Personne morale / '.$model['name_legal_person'];
                } else {
                        return $model['lastname'] . ' ' . $model['name'];
                }
        }), ['prompt' => '--','class' => 'form-control user-id', ]) ?>
    </div>
    <div class="col-md-1" id="or-user">
        <span>OU</span>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="clr"></div>
    
    <?= $form->field($model, 'id_producer')->hiddenInput() ?>
    <?= $form->field($model, 'id_point_sale')->dropDownList(ArrayHelper::map(PointSale::find()->where('id_producer = '.GlobalParam::getCurrentProducerId())->all(), 'id', function($model, $defaultValue) {
        return $model['name'];
    }), ['prompt' => '--','class' => 'form-control user-id']) ?>
    <?= $form->field($model, 'date_begin') ?>
    <?= $form->field($model, 'date_end')->hint('Laisser vide pour une durée indéterminée') ?>
    <div class="days">
        <h2>Jours</h2>
        <?= $form->field($model, 'monday')->checkbox() ?>
        <?= $form->field($model, 'tuesday')->checkbox() ?>
        <?= $form->field($model, 'wednesday')->checkbox() ?>
        <?= $form->field($model, 'thursday')->checkbox() ?>
        <?= $form->field($model, 'friday')->checkbox() ?>
        <?= $form->field($model, 'saturday')->checkbox() ?>
        <?= $form->field($model, 'sunday')->checkbox() ?>
    </div>
    <div class="clr"></div>
    <?= $form->field($model, 'week_frequency')->dropDownList([1=>1, 2=>2, 3=>3, 4=>4]) ?>

    <?= $form->field($model, 'auto_payment')
        ->checkbox()
        ->hint('Cochez cette case si vous souhaitez que le crédit pain du client soit automatiquement débité lors de la création de la commande.<br />'
                . 'Attention, un compte client existant doit être spécifié en haut de ce formulaire.'
                . '<br /><strong>Pris en compte uniquement dans le cas d\'un Crédit défini comme optionnel au niveau du point de vente. Dans les autres cas, il sera automatiquement déduit.</strong>') ?>
    
    <div class="products">
        <h2>Produits</h2>
        <?php if(isset($model->errors['products']) && count($model->errors['products']))
        {
            echo '<div class="alert alert-danger">'.$model->errors['products'][0].'</div>' ;
        }
        ?>
        <table class="table table-bordered table-condensed table-hover" id="products">
            <tr v-for="product in products">
                <td>{{ product.name }}</td>
                <td>
                    <input type="hidden" :value="product.step" :name="'product_step_'+product.step" />
                    <input type="hidden" :value="product.price" :name="'product_price_'+product.price" />
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" @click="changeQuantityProductSubscription(false, product)"><span class="glyphicon glyphicon-minus"></span></button>
                        </span>
                        <input v-model="product.quantity" :name="'SubscriptionForm[products][product_'+product.id+']'" class="form-control input-quantity" />
                        <div class="input-group-addon">
                            <span>
                                {{ product.wording_unit }}
                            </span>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" @click="changeQuantityProductSubscription(true, product)"><span class="glyphicon glyphicon-plus"></span></button>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
    
    <?= Html::submitButton('Enregistrer' , ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
</div>