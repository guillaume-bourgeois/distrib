<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Tableau de bord','icon' => 'dashboard','url' => ['/site/index'], 'visible' => User::isCurrentProducer()],
                    ['label' => 'Distributions','icon' => 'calendar','url' => ['/distribution/index'], 'visible' => User::isCurrentProducer()],
                    [
                            'label' => 'Produits',
                            'icon' => 'clone',
                            'url' => '#',
                            'visible' => User::isCurrentProducer(),
                            'active' => Yii::$app->controller->id == 'product',
                            'items' => [
                                    ['label' => 'Liste','icon' => 'th-list','url' => ['/product/index'], 'visible' => User::isCurrentProducer()],
                                    ['label' => 'Catégories','icon' => 'book','url' => ['/product-category/index'], 'visible' => User::isCurrentProducer()],
                            ]
                    ],
                    ['label' => 'Points de vente','icon' => 'map-marker','url' => ['/point-sale/index'], 'visible' => User::isCurrentProducer(), 'active' => Yii::$app->controller->id == 'point-sale'],
                    [
                            'label' => 'Utilisateurs',
                            'icon' => 'users',
                            'url' => '#',
                            'items' => [
                                    ['label' => 'Liste','icon' => 'th-list','url' => ['/user/index'], 'visible' => User::isCurrentProducer()],
                                    ['label' => 'Groupes','icon' => 'users','url' => ['/user-group/index'], 'visible' => User::isCurrentProducer()],
                            ],
                    ],
                    ['label' => 'Abonnements','icon' => 'repeat','url' => ['/subscription/index'], 'visible' => User::isCurrentProducer(), 'active' => Yii::$app->controller->id == 'subscription'],
                    ['label' => 'Paramètres','icon' => 'cog','url' => ['/producer/update'], 'visible' => User::isCurrentProducer()],
                    ['label' => 'Communiquer','icon' => 'bullhorn','url' => ['/communicate/index'], 'visible' => User::isCurrentProducer()],
                    [
                        'label' => 'Statistiques',
                        'icon' => 'line-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Rapports','icon' => 'pencil-square-o','url' => ['/report/index'], 'visible' => User::isCurrentProducer()],
                            ['label' => 'Chiffre d\'affaire','icon' => 'line-chart','url' => ['/stats/index'], 'visible' => User::isCurrentProducer()],
                            ['label' => 'Produits','icon' => 'table','url' => ['/stats/products'], 'visible' => User::isCurrentProducer()],
                        ],
                    ],
                    [
                        'label' => 'Documents',
                        'icon' => 'clone',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Devis','icon' => 'sticky-note-o','url' => ['/quotation/index'], 'visible' => User::isCurrentProducer()],
                            ['label' => 'Bons de livraison','icon' => 'sticky-note-o','url' => ['/delivery-note/index'], 'visible' => User::isCurrentProducer()],
                            ['label' => 'Factures','icon' => 'sticky-note-o','url' => ['/invoice/index'], 'visible' => User::isCurrentProducer()],
                        ],
                    ],
                    ['label' => 'Développement','icon' => 'wrench','url' => ['/development/index'], 'visible' => User::isCurrentProducer(), 'active' => Yii::$app->controller->id == 'development'],
                    ['label' => 'Mon abonnement','icon' => 'euro','url' => ['/producer/billing'], 'visible' => User::isCurrentProducer()],
                    ['label' => 'Accès','icon' => 'lock','url' => ['/access/index'], 'visible' => User::isCurrentProducer()],
                    
                    ['label' => 'Administration', 'options' => ['class' => 'header'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Producteurs','icon' => 'th-list','url' => ['/producer-admin/index'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Taxes','icon' => 'eur','url' => ['/tax-rate-admin/index'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Communiquer','icon' => 'bullhorn','url' => ['/communicate-admin/index'], 'visible' => User::isCurrentAdmin()],
                    
                    ['label' => 'Outils', 'options' => ['class' => 'header'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'], 'visible' => User::isCurrentAdmin()],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => !User::isCurrentConnected()],
                ],
            ]
        ) ?>

    </section>

</aside>
