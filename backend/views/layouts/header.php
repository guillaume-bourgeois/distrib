<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use common\models\Producer;
use common\models\User;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\helpers\GlobalParam;

/* @var $this \yii\web\View */
/* @var $content string */

$producer = GlobalParam::getCurrentProducer();

?>

<header class="main-header">

        <?= Html::a('<span class="logo-mini"><img src="' . Yii::$app->urlManagerBackend->getBaseUrl() . '/img/logo-distrib.png" /></span><span class="logo-lg"><img src="' . Yii::$app->urlManagerBackend->getBaseUrl() . '/img/logo-distrib.png" /></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
                <?php

                $usersArray = User::findBy(['id_producer' => GlobalParam::getCurrentProducerId()])
                        ->andWhere('CAST(FROM_UNIXTIME(user.created_at) AS date) > \'' . date("Y-m-d", strtotime("-7 days")) . '\'')
                        ->orderBy('created_at DESC')
                        ->all();

                ?>

            <ul class="nav navbar-nav">

                    <?php

                    $pastDistributionsArray = Distribution::find()
                            ->where(['<', 'distribution.date', date('Y-m-d')])
                            ->andWhere([
                                    'distribution.id_producer' => GlobalParam::getCurrentProducerId(),
                                    'distribution.active' => 1
                            ])
                            ->orderBy('date DESC')
                            ->limit(3)
                            ->all();

                    $pastDistributionsArray = array_reverse($pastDistributionsArray);

                    $incomingDistributionsArray = Distribution::find()
                            ->where(['>=', 'distribution.date', date('Y-m-d')])
                            ->andWhere([
                                    'distribution.id_producer' => GlobalParam::getCurrentProducerId(),
                                    'distribution.active' => 1
                            ])
                            ->orderBy('date ASC')
                            ->limit(20)
                            ->all();
                    ?>

                <li class="dropdown distributions-menu notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-calendar"></i>
                    </a>
                    <ul class="dropdown-menu">

                            <?php if (count($pastDistributionsArray)): ?>
                                <li class="header">3 dernières distributions :</li>
                                <li>
                                    <ul class="menu">
                                            <?php foreach ($pastDistributionsArray as $distribution): ?>
                                                <li>
                                                    <a href="<?= Yii::$app->urlManagerBackend->createUrl(['distribution/index', 'date' => $distribution->date]); ?>">
                                                        <h5><?= strftime('%A %d %B', strtotime($distribution->date)) ?></h5>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>

                            <?php if (count($incomingDistributionsArray)): ?>
                                <li class="header">Prochaines distributions :</li>
                                <li>
                                    <ul class="menu">
                                            <?php foreach ($incomingDistributionsArray as $distribution): ?>
                                                <li>
                                                    <a href="<?= Yii::$app->urlManagerBackend->createUrl(['distribution/index', 'date' => $distribution->date]); ?>">
                                                        <h5><?= strftime('%A %d %B', strtotime($distribution->date)) ?></h5>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li class="header">
                                    Aucune distribution prévue.<br/>
                                    <a class="btn btn-default"
                                       href="<?= Yii::$app->urlManagerBackend->createUrl(['distribution/index']); ?>">Gérer
                                        mes distributions</a>
                                </li>
                            <?php endif; ?>
                    </ul>
                </li>

                <li class="dropdown users-menu notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-plus"></i>
                            <?php if (count($usersArray)): ?><span
                                    class="label label-success"><?= count($usersArray) ?></span>
                            <?php else: ?><span class="label label-warning">0</span><?php endif; ?>
                    </a>
                    <ul class="dropdown-menu">
                            <?php if (count($usersArray)): ?>
                                <li class="header">Inscriptions des 7 derniers jours</li>
                                <li>
                                    <ul class="menu">
                                            <?php foreach ($usersArray as $user): ?>
                                                <li>
                                                    <a href="<?= Yii::$app->urlManagerBackend->createUrl(['user/update', 'id' => $user['user_id']]); ?>">
                                                        <h5><?= Html::encode((isset($user['name_legal_person']) && strlen($user['name_legal_person'])) ? $user['name_legal_person'] : $user['name'] . ' ' . $user['lastname']); ?>
                                                            <small>
                                                                <i class="fa fa-clock-o"></i> <?= date('d/m/Y à H:i', $user['created_at']); ?>
                                                            </small>
                                                        </h5>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li class="header">Aucun nouvel inscrit ces 7 derniers jours.</li>
                            <?php endif; ?>
                    </ul>
                </li>

                    <?php

                    $usersNegativeCreditArray = User::findBy(['id_producer' => GlobalParam::getCurrentProducerId()])
                            ->andWhere('user_producer.credit < 0')
                            ->orderBy('lastname, name ASC')
                            ->all();

                    ?>

                <li class="dropdown users-negative-credit-menu notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-euro"></i>
                            <?php if (count($usersNegativeCreditArray)): ?><span
                                    class="label label-warning"><?= count($usersNegativeCreditArray) ?></span>
                            <?php else: ?><span class="label label-success">0</span><?php endif; ?>
                    </a>

                    <ul class="dropdown-menu">
                            <?php if (count($usersNegativeCreditArray)): ?>
                                <li class="header">Utilisateurs au crédit négatif</li>
                                <li>
                                    <ul class="menu">
                                            <?php foreach ($usersNegativeCreditArray as $user): ?>
                                                <li>
                                                    <a href="<?= Yii::$app->urlManagerBackend->createUrl(['user/credit', 'id' => $user['user_id']]); ?>">
                                                        <h5><?= Html::encode($user['name'] . ' ' . $user['lastname']); ?>
                                                            <small>
                                                                <i class="fa fa-euro"></i> <?= Price::format($user['credit']); ?>
                                                            </small>
                                                        </h5>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li class="header">Aucun de vos utilisateurs n'a de crédit négatif.</li>
                            <?php endif; ?>
                    </ul>

                </li>

                    <?php if (User::isCurrentProducer() || User::isCurrentAdmin()): ?>
                        <li class="dropdown producer-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <?php if ($producer->active): ?>
                                        <i class="fa fa-home"></i>
                                    <?php else: ?>
                                        <span class="label label-danger">Hors-ligne</span>
                                    <?php endif; ?>
                                <span><?= Html::encode(Yii::$app->user->identity->getNameProducer()); ?></span>
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                    <?php if (User::isCurrentAdmin()): ?>
                                        <li>
                                            <a href="<?= Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index', 'slug_producer' => GlobalParam::getCurrentProducer()->slug]); ?>">
                                                <i class="fa fa-th-large"></i>
                                                <span class="hidden-xs">Espace du producteur</span>
                                            </a>
                                        </li>
                                        <li class="header">&nbsp;<strong>Autres producteurs</strong></li>
                                            <?php $producersArray = Producer::find()->orderBy('name ASC')->all(); ?>
                                            <?php foreach ($producersArray as $producer): ?>
                                            <li>
                                                <a href="<?= Yii::$app->urlManagerBackend->createUrl(['site/change-producer', 'id' => $producer->id]); ?>"><?= Html::encode($producer->name) ?></a>
                                            </li>
                                            <?php endforeach; ?>
                                    <?php else: ?>
                                        <li>
                                            <a href="<?= Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index', 'slug_producer' => GlobalParam::getCurrentProducer()->slug]); ?>">
                                                <i class="fa fa-th-large"></i>
                                                <span class="hidden-xs">Mon espace</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span class="hidden-xs"><?= Html::encode(User::getCurrent()->name . ' ' . User::getCurrent()->lastname); ?></span>
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['user/update']); ?>"><i
                                        class="fa fa-user"></i> Profil</a></li>
                        <li><a href="<?= Yii::$app->urlManagerBackend->createUrl(['site/logout']); ?>"><i
                                        class="fa fa-sign-out"></i> Déconnexion</a></li>
                    </ul>
                </li>

                <li class="link-control-sidebar">
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>

            </ul>
        </div>
    </nav>
</header>
