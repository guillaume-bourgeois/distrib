<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Producer;
use common\models\ProductPrice ;

/* @var $this yii\web\View */
/* @var $model backend\models\PointVente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-sale-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-8">

                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'locality')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'id_user', [
                        'template' => '{label} <a href="' . Yii::$app->urlManager->createUrl(['user/create']) . '" class="btn btn-xs btn-default">Nouvel utilisateur <span class="glyphicon glyphicon-plus"></span></a><div>{input}</div>{hint}',
                ])
                        ->dropDownList(
                                ArrayHelper::map(User::findBy()->orderBy('type DESC')->all(), 'user_id', function ($model) {
                                        if(strlen($model['name_legal_person'])) {
                                                return 'Personne morale / '.$model['name_legal_person'] ;
                                        }
                                        else {
                                                return $model['lastname'] . ' ' . $model['name'];
                                        }
                                }),
                                [
                                        'prompt' => '--',
                                ]
                        )->hint('Utilisé lors de la facturation'); ?>

                <?php
                $addHintCredit = '';
                if (!Producer::getConfig('credit')):
                        $addHintCredit = '<br /><strong>Attention, le système de Crédit est désactivé au niveau des ' . Html::a('paramètres globaux', ['producer/update']) . '.</strong>';
                endif;

                echo $form->field($model, 'credit')
                        ->checkbox()
                        ->hint('Cochez cette case si le client peut régler ses commandes via son compte <strong>Crédit</strong> pour ce point de vente.'
                                . $addHintCredit);
                ?>

                <?= $form->field($model, 'credit_functioning')
                        ->dropDownList([
                                '' => 'Paramètres globaux (' . Producer::$creditFunctioningArray[Producer::getConfig('credit_functioning')] . ')',
                                Producer::CREDIT_FUNCTIONING_OPTIONAL => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_OPTIONAL],
                                Producer::CREDIT_FUNCTIONING_MANDATORY => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_MANDATORY],
                                Producer::CREDIT_FUNCTIONING_USER => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_USER],
                        ], [])->hint(Producer::HINT_CREDIT_FUNCTIONING); ?>

                <?php /*$form->field($model, 'product_price_percent')
                        ->dropDownList(ProductPrice::percentValues(), [])->hint('Pourcentage appliqué aux prix de chaque produit dans ce point de vente.');*/ ?>


                <div id="delivery-days">
                        <h2>Jours de livraison</h2>
                        <?= $form->field($model, 'delivery_monday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_tuesday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_wednesday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_thursday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_friday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_saturday')->checkbox() ?>
                        <?= $form->field($model, 'delivery_sunday')->checkbox() ?>
                </div>
                <div class="clr"></div>

                <h2>Informations</h2>
                <?= $form->field($model, 'infos_monday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_tuesday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_wednesday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_thursday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_friday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_saturday')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'infos_sunday')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-4">

                <?= $form->field($model, 'code')
                        ->label('Code d\'accès')
                        ->hint('Renseignez ce champs si vous souhaitez protéger ce point de vente par un code.')
                ?>


                <?= $form->field($model, 'restricted_access')
                        ->checkbox()
                        ->hint('Cochez cette case si seulement un groupe restreint d\'utilisateurs peuvent accéder à ce point de vente.<br />'
                                . 'Dans le cas des boîtes à pain, il vous est possible de spécifier un commentaire pour chaque utilisateur sélectionné afin de lui renseigner son numéro de boîte ou son code.') ?>
                <div id="users">
                        <?= Html::activeCheckboxList($model, 'users', ArrayHelper::map($users, 'user_id', function ($model_user, $defaultValue) use ($model) {
                                return Html::encode($model_user['lastname'] . ' ' . $model_user['name']) . '<br />'
                                        . Html::activeTextInput(
                                                $model,
                                                'users_comment[' . $model_user['user_id'] . ']',
                                                [
                                                        'class' => 'form-control commentaire',
                                                        'placeholder' => 'Commentaire',
                                                        'value' => (isset($model->users_comment[$model_user['user_id']])) ? Html::encode($model->users_comment[$model_user['user_id']]) : ''
                                                ]);
                        }), ['encode' => false, 'class' => '']) ?>
                </div>
        </div>
        <div class="clr"></div>

        <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Ajouter' : 'Modifier', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
