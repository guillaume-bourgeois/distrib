<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html ;

$this->setTitle('Communiquer') ;
$this->addBreadcrumb('Communiquer') ;

?>

<div class="col-md-6">
    <div id="email" class="panel panel-default" v-if="date">
        <div class="panel-heading">
            <h3 class="panel-title">Envoyer un email</h3>
        </div>
        <div class="panel-body">
            <p>Choisissez à quels utilisateurs vous souhaitez envoyer un email :</p>
            <div>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span> Tous',['user/mail', 'idPointSale' => 0], ['class' => 'btn btn-default btn-point-sale']); ?>
                <?= Html::a('<span class="glyphicon glyphicon-repeat"></span> Abonnés',['user/mail', 'sectionSubscribers' => 1], ['class' => 'btn btn-default btn-point-sale']); ?>
                <?= Html::a('<span class="glyphicon glyphicon-time"></span> Inactifs',['user/mail', 'sectionInactiveUsers' => 1], ['class' => 'btn btn-default btn-point-sale']); ?>
                <?php foreach($pointsSaleArray as $pointSale): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-map-marker"></span> '.Html::encode($pointSale->name), ['user/mail', 'idPointSale' => $pointSale->id], ['class' => 'btn btn-default btn-point-sale']); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div id="paper" class="panel panel-default" v-if="date">
        <div class="panel-heading">
            <h3 class="panel-title">Communiquer par papier</h3>
        </div>
        <div class="panel-body">
            <p>Imprimez ce petit encart pour indiquer à vos clients l'adresse internet leur permettant de passer leurs commandes.</p>
            <?php echo $this->render('instructions', ['producer' => $producer]) ; ?>
            <p><?php echo Html::a('<span class="glyphicon glyphicon-download-alt"></span> Télécharger', ['communicate/instructions'], ['class'=>'btn btn-primary']) ?></p>
        </div>
    </div>
</div>

