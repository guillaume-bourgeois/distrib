<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use \backend\controllers\StatsController ;
use yii\helpers\Html;

$this->setTitle('Statistiques <small>Produits '.$year.'</small>', 'Statistiques produits '.$year) ;
$this->addBreadcrumb('Statistiques produits '.$year) ;
$this->addButton(['label' => '&lt; '.($year - 1), 'url' => ['stats/products','year' => $year - 1], 'class' => 'btn btn-default']) ;
$this->addButton(['label' => ($year + 1).' &gt;', 'url' => ['stats/products','year' => $year + 1], 'class' => 'btn btn-default']) ;

$theMonth = 1 ;

?>
<div class="stats-products">
    
    
<table id="table-stats-products" class="table table-bordered table-hover header-fixed">
    <thead>
        <tr class="mois">
            <th></th>
            <?php foreach($monthArray as $key => $month): 
                if(($key + 1) >= $iStart && ($key + 1) <= $iEnd):
            ?>
                <th colspan="2">
                    <?php if($section != 1 && $key + 1 == $iStart): echo Html::a('&lt;', ['stats/products', 'year' => $year, 'section' => $section - 1], ['class' => 'btn btn-default']) ; ?><?php endif; ?>
                    <?= $month; ?>
                    <?php if($key + 1 == $iEnd && $section != 4): echo Html::a('&gt;', ['stats/products', 'year' => $year, 'section' => $section + 1], ['class' => 'btn btn-default']) ; ?><?php endif; ?>
                </th>
            <?php 
            endif;
            endforeach; 
            ?>
            <th colspan="2">Totaux</th>
        </tr>
        <tr class="sub-head">
            <th></th>
            <?php for($i=$iStart; $i<=$iEnd + 1; $i++): ?>
                <th>Maximum</th>
                <th>Commandés</th>
            <?php endfor; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($productsArray as $currentProduct):  ?>
        <tr>
            <td class="name"><?= Html::encode($currentProduct['name']) ?></td>
            <?php foreach($dataProducts as $month => $productsMonthArray): 
                
                $findMax = false ;
                $findOrders = false ;
            ?>
                <?php if($month != StatsController::TOTALS): ?>
            
                    <!-- max -->
                    <?php 
                    $tooltip = 'data-toggle="tooltip" data-placement="top" title="'.Html::encode($currentProduct['name']).' / '.$monthArray[$month - 1] .' '. $year.' / Maximum"' ;
                    foreach($productsMonthArray['max'] as $product): 
                    ?>
                        <?php if($product['name'] == $currentProduct['name']): 
                            $findMax = true ;
                        ?>
                            <td class="align-center">
                                <div <?= $tooltip; ?>><?= (int) $product['total'] ?></div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if(!$findMax): ?><td class="align-center"><div <?= $tooltip; ?>>0</div></td><?php endif; ?>

                    <!-- commandes -->
                    <?php 
                    $tooltip = 'data-toggle="tooltip" data-placement="top" title="'.Html::encode($currentProduct['name']).' / '. $monthArray[$month - 1] . ' '. $year .' / Commandés"' ;
                    foreach($productsMonthArray['orders'] as $product): 
                    ?>
                        <?php if($product['name'] == $currentProduct['name']): 
                            $findOrders = true ;
                        ?>
                            <td class="align-center">
                                <div <?= $tooltip ?> ><?= (int) $product['total'] ?></div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>        
                    <?php if(!$findOrders): ?><td class="align-center"><div <?= $tooltip ?> >0</div></td><?php endif; ?>

                
                <?php else: ?>
                    <!-- totaux max -->
                    <?php 
                    $tooltip = 'data-toggle="tooltip" data-placement="top" title="'.Html::encode($currentProduct['name']).' / Total '. $year .' / Maximums"' ;
                    foreach($dataProducts[StatsController::TOTALS]['max'] as $productName => $totalMax): 
                    ?>
                        <?php 
                        if($productName == $currentProduct['name']): 
                            $findMax = true ;
                        ?>
                            <td class="align-center">
                                <div <?= $tooltip ?> ><?= (int) $totalMax ?></div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if(!$findMax): ?><td class="align-center"><div <?= $tooltip; ?>>0</div></td><?php endif; ?>
                            
                    <!-- totaux commandés -->
                    <?php 
                    $tooltip = 'data-toggle="tooltip" data-placement="top" title="'.Html::encode($currentProduct['name']).' / Total '. $year .' / Commandés"' ;
                    foreach($dataProducts[StatsController::TOTALS]['orders'] as $productName => $totalOrders):     
                    ?>
                        <?php if($productName == $currentProduct['name']): 
                            $findOrders = true ;
                        ?>
                            <td class="align-center">
                                <div <?= $tooltip ?> ><?= (int) $totalOrders ?></div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>  
                    <?php if(!$findOrders): ?><td class="align-center"><div <?= $tooltip ?> >0</div></td><?php endif; ?>
                            
                <?php endif; ?>      
            <?php endforeach; ?>
        </tr>
        
        <?php endforeach; ?>
        
    </tbody>
</table>

</div>
