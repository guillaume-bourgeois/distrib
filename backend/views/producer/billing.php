<?php

/** 
Copyright La boîte à pain (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User ;
use common\models\Producer ;
use yii\bootstrap\ActiveForm;

$this->setTitle('Mon abonnement') ;
$this->addBreadcrumb($this->getTitle()) ;

?>

<div class="callout callout-info">
    <h4>Abonnement à prix libre</h4>
    <p><i>distrib</i> fonctionne avec système d'abonnement à prix libre pour en
        assurer la plus grande diffusion possible. Ceci correspond également à l'unique source de revenus 
        nécessaire à la maintenance et au développement de la plateforme.<br />
        Si cet outil est important dans votre activité, nous vous encourageons à nous soutenir en vous abonnant.
    </p>
</div>

<div id="free-price" class="">
    
    <?php if($alertFreePrice): ?>
        <div class="alert alert-success">
            Le montant de votre abonnement a bien été mis à jour.
            <?php if($producer->free_price != 0): ?>
                Vos prochaines factures mensuelles auront un montant de <strong><?= number_format(Html::encode($producer->free_price),2).' € HT' ; ?></strong>.
            <?php endif; ?>
        </div>
    <?php endif; ?>
    
    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
            ],
        ],
    ]); ?>
    
    <?= $form->field($producer, 'free_price',[
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span> HT / mois</span></div>',
        ])
        ->label('Prix libre');  ?>
    
    <div class="form-group field-user-prix_libre">
        <div class="col-sm-6">
            <?= Html::submitButton('Valider', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clr"></div>
</div>