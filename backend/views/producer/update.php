<?php

/** 
Copyright La boîte à pain (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Url ;
use common\models\Producer ;

\backend\assets\VuejsProducerUpdateAsset::register($this);

$this->setTitle('Paramètres') ;
$this->addBreadcrumb($this->getTitle()) ;

?>

<div class="user-update" id="app-producer-update">
    
    <div id="nav-params">
        <button v-for="section in sectionsArray" :class="'btn '+((currentSection == section.name) ? 'btn-primary' : 'btn-default')" @click="changeSection(section)">
            {{ section.nameDisplay }}
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </button>
    </div>
    
    <div class="user-form">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]); ?>
        <div>
            <div v-show="currentSection == 'general'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Général</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'active')
                        ->dropDownList([
                            0 => 'Non',
                            1 => 'Oui'
                        ], [])
                        ->label('En ligne')
                        ->hint('Activez cette option pour rendre votre établissement visible à vos clients.') ; ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'type') ?>
                    <?= $form->field($model, 'description')
                            ->textarea(['rows' => 4])
                            ->hint('Affiché sur la page d\'accueil') ?>
                    <?= $form->field($model, 'address')
                            ->textarea(['rows' => 4])  ?>
                    <?= $form->field($model, 'postcode') ?>
                    <?= $form->field($model, 'city') ?>

                    <?= $form->field($model, 'code')->hint("Saisissez ce champs si vous souhaitez protéger l'accès à votre boutique par un code, sinon laissez-le vide.<br />"
                            . "Ce code est à communiquer à vos client pour qu'ils puissent ajouter votre établissement à leur tableau de bord.<br />"
                            . "<a href=\"".Yii::$app->urlManager->createUrl(['communicate/index'])."\">Cliquez ici</a> pour télécharger un mode d'emploi comprenant ce code à distribuer à vos clients.") ?>
                    <?= $form->field($model, 'background_color_logo') ?>
                    <?= $form->field($model, 'logo')->fileInput() ?>
                    <?php
                    if (strlen($model->logo)) {
                        echo '<img src="'.Yii::$app->urlManagerProducer->getHostInfo().'/'.Yii::$app->urlManagerProducer->baseUrl.'/uploads/' . $model->logo . '" width="200px" /><br />';
                        echo '<input type="checkbox" name="delete_logo" id="delete_logo" /> <label for="delete_logo">Supprimer le logo</label><br /><br />';
                    }
                    ?>
                    <?= $form->field($model, 'photo')->fileInput()->hint('Format idéal : 900 x 150 px') ?>
                    <?php
                    if (strlen($model->photo)) {
                        echo '<img src="'.Yii::$app->urlManagerProducer->getHostInfo().'/'.Yii::$app->urlManagerProducer->baseUrl.'/uploads/' . $model->photo . '" width="400px" /><br />';
                        echo '<input type="checkbox" name="delete_photo" id="delete_photo" /> <label for="delete_photo">Supprimer la photo</label><br /><br />';
                    }
                    ?>
                    <?= $form->field($model, 'behavior_home_point_sale_day_list')
                            ->dropDownList([
                                    Producer::BEHAVIOR_HOME_POINT_SALE_DAY_LIST_WEEK => 'Jours de la semaine',
                                    Producer::BEHAVIOR_HOME_POINT_SALE_DAY_LIST_INCOMING_DISTRIBUTIONS => 'Distributions à venir',
                            ]); ?>
                </div>
            </div>

            <div v-show="currentSection == 'tableau-bord'" class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Tableau de bord</h3>
                </div>
                <div class="panel-body">

                        <?= $form->field($model, 'option_dashboard_number_distributions')
                                ->dropDownList([
                                        3 => '3',
                                        6 => '6',
                                        9 => '9',
                                        12 => '12',
                                        15 => '15',
                                        18 => '18',
                                        21 => '21',
                                        24 => '24',
                                        27 => '27',
                                        30 => '30',
                                ], []); ?>

                        <?= $form->field($model, 'option_dashboard_date_start')->textInput([
                                'class' => 'datepicker form-control'
                        ]) ; ?>
                        <?= $form->field($model, 'option_dashboard_date_end')->textInput([
                                'class' => 'datepicker form-control'
                        ]) ; ?>
                </div>
            </div>

             <div v-show="currentSection == 'prise-commande'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Prise de commande</h3>
                </div>
                <div class="panel-body">

                        <?php

                                $delaysArray = [
                                        1 => '1 jour',
                                        2 => '2 jours',
                                        3 => '3 jours',
                                        4 => '4 jours',
                                        5 => '5 jours',
                                        6 => '6 jours',
                                        7 => '7 jours',
                                ] ;

                                $deadlinesArray = [
                                        24 => 'Minuit',
                                        23 => '23h',
                                        22 => '22h',
                                        21 => '21h',
                                        20 => '20h',
                                        19 => '19h',
                                        18 => '18h',
                                        17 => '17h',
                                        16 => '16h',
                                        15 => '15h',
                                        14 => '14h',
                                        13 => '13h',
                                        12 => '12h',
                                        11 => '11h',
                                        10 => '10h',
                                        9 => '9h',
                                        8 => '8h',
                                ] ;

                                $daysArray = [
                                        'monday' => 'Lundi',
                                        'tuesday' => 'Mardi',
                                        'wednesday' => 'Mercredi',
                                        'thursday' => 'Jeudi',
                                        'friday' => 'Vendredi',
                                        'saturday' => 'Samedi',
                                        'sunday' => 'Dimanche'
                                ] ;
                        ?>

                        <div class="row">
                                <div class="col-md-2">
                                        <strong>Par défaut</strong>
                                </div>
                                <div class="col-md-5">
                                        <?= $form->field($model, 'order_delay')
                                                ->dropDownList($delaysArray, ['prompt' => '--'])
                                                ->hint('Si <strong>1 jour</strong> est sélectionné, le client pourra commander jusqu\'à la veille de la production.<br />'
                                                        . 'Si <strong>2 jours</strong> est sélectionné, le client pourra commander jusqu\'à l\'avant-veille de la production, etc.') ; ?>
                                </div>
                                <div class="col-md-5">
                                        <?= $form->field($model, 'order_deadline')
                                                ->dropDownList($deadlinesArray, ['prompt' => '--'])
                                                ->hint('Heure limite jusqu\'à laquelle les clients peuvent commander pour satisfaire le délai de commande.<br />'
                                                        . 'Par exemple, si <strong>2 jours</strong> est sélectionné dans le délai de commande, le client devra commander l\'avant-veille de la production avant l\'heure précisée ici.') ; ?>
                                </div>
                        </div>
                        <?php foreach($daysArray as $day => $labelDay): ?>
                        <div class="row">
                                <div class="col-md-2">
                                        <strong><?= $labelDay ?></strong>
                                </div>
                                <div class="col-md-5">
                                        <?= $form->field($model, 'order_delay_'.$day, [
                                                'template' => '{input}',
                                        ])->dropDownList($delaysArray, ['prompt' => '--'])->label(''); ?>
                                </div>
                                <div class="col-md-5">
                                        <?= $form->field($model, 'order_deadline_'.$day, [
                                                'template' => '{input}',
                                        ])->dropDownList($deadlinesArray, ['prompt' => '--'])->label(''); ?>
                                </div>
                        </div>
                        <?php endforeach; ?>

                    
                    <?= $form->field($model, 'order_infos')
                        ->textarea(['rows' => 6])
                        ->hint('Affichées au client lors de sa commande')?>
                    
                    <?= $form->field($model, 'option_allow_user_gift')
                            ->dropDownList([
                                0 => 'Non',
                                1 => 'Oui',
                            ], []) ; ?>
                    
                    <?= $form->field($model, 'option_behavior_cancel_order')
                            ->dropDownList([
                                Producer::BEHAVIOR_DELETE_ORDER_DELETE => 'Suppression de la commande',
                                Producer::BEHAVIOR_DELETE_ORDER_STATUS => 'Passage de la commande en statut "supprimé"',
                            ], []) ; ?>

                    <?= $form->field($model, 'behavior_order_select_distribution')
                        ->dropDownList([
                                Producer::BEHAVIOR_ORDER_SELECT_DISTRIBUTION_CALENDAR => 'Calendrier',
                                Producer::BEHAVIOR_ORDER_SELECT_DISTRIBUTION_LIST => 'Liste',
                        ]); ?>

                        <?= $form->field($model, 'option_payment_info')
                                ->textarea(['rows' => 6])
                                ->hint('Affiché au client à la fin de la prise de commande')?>

                        <?= $form->field($model, 'option_email_confirm')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>
                        <?= $form->field($model, 'option_email_confirm_producer')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>

                        <?= $form->field($model, 'option_csv_export_all_products')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>
                        <?= $form->field($model, 'option_csv_export_by_piece')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>

                        <?= $form->field($model, 'option_display_export_grid')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>

                        <?= $form->field($model, 'option_order_reference_type')
                                ->dropDownList([
                                        Producer::ORDER_REFERENCE_TYPE_NONE => '--',
                                        Producer::ORDER_REFERENCE_TYPE_YEARLY => 'Annuelle',
                                ], []) ; ?>

                        <?= $form->field($model, 'option_export_display_product_reference')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>

                        <?= $form->field($model, 'option_allow_order_guest')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>

                        <?= $form->field($model, 'option_order_entry_point')
                                ->dropDownList([
                                        Producer::ORDER_ENTRY_POINT_DATE => 'Date',
                                        Producer::ORDER_ENTRY_POINT_POINT_SALE => 'Point de vente',
                                ], []); ?>

                        <?= $form->field($model, 'option_delivery')
                                ->dropDownList([
                                        0 => 'Non',
                                        1 => 'Oui'
                                ], []); ?>
                </div>
             </div>

            
            <div v-show="currentSection == 'abonnements'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Abonnements</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'user_manage_subscription')
                            ->dropDownList([
                                0 => 'Non',
                                1 => 'Oui',
                            ], []) ; ?>
                </div>
            </div>
            
            <div v-show="currentSection == 'credit-payment'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Crédit</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'credit')
                        ->dropDownList([
                            0 => 'Non',
                            1 => 'Oui',
                        ], [])
                        ->label('Activer le système de Crédit')
                        ->hint('Le système de Crédit permet à vos clients d\'avoir un compte prépayé sur le site <em>distrib</em>.<br />'
                                . 'Ils créditent leur compte en vous donnant la somme de leur choix et c\'est ensuite à vous de '.Html::a('mettre à jour', ['user/index']).' leur Crédit en ligne.<br />'
                                . 'Ceci fait, les clients paient leur commande directement via leur Crédit.') ; ?>

                    <?= $form->field($model, 'credit_functioning')
                            ->dropDownList([
                                Producer::CREDIT_FUNCTIONING_OPTIONAL => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_OPTIONAL],
                                Producer::CREDIT_FUNCTIONING_MANDATORY => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_MANDATORY],
                                Producer::CREDIT_FUNCTIONING_USER => Producer::$creditFunctioningArray[Producer::CREDIT_FUNCTIONING_USER],
                            ], [])->hint(Producer::HINT_CREDIT_FUNCTIONING) ; ?>
                    
                    <?= $form->field($model, 'use_credit_checked_default')
                            ->dropDownList([
                                0 => 'Non',
                                1 => 'Oui',
                            ], [])->hint('Utilisation optionnelle du Crédit.') ; ?>
                    
                    <?= $form->field($model, 'credit_limit_reminder',[
                            'template' => '{label}<div class="input-group">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span></div>{hint}',
                        ])
                        ->hint("Une relance est envoyé au client dès que ce seuil est dépassé.") ;  ?>
                    
                    
                    
                    <?= $form->field($model, 'credit_limit',[
                            'template' => '{label}<div class="input-group">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span></div>{hint}',
                        ])->hint('Limite de crédit que l\'utilisateur ne pourra pas dépasser. Laisser vide pour permettre un crédit négatif et infini.');  ?>
                    
                </div>
            </div>
            
            <div v-show="currentSection == 'infos'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Informations légales</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'mentions')
                            ->textarea(['rows' => 15])
                            ->hint('') ?>
                    <?= $form->field($model, 'gcs')
                            ->textarea(['rows' => 15])
                            ->hint('') ?>
                </div>
            </div>
            
            <div v-show="currentSection == 'logiciels-caisse'" class="panel panel-default">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Logiciels de caisse</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'tiller')
                        ->dropDownList([
                            0 => 'Non',
                            1 => 'Oui'
                        ], [])
                        ->label('Synchroniser avec Tiller'); ?>
                    <?= $form->field($model, 'tiller_provider_token') ; ?>
                    <?= $form->field($model, 'tiller_restaurant_token') ; ?>
                </div>
            </div>
                
            <div v-show="currentSection == 'facturation'" class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Facturation</h3>
                </div>
                <div class="panel-body">
                        <?= $form->field($model, 'id_tax_rate_default')
                                ->dropDownList(ArrayHelper::map(TaxRate::find()->all(), 'id', function($model) { return $model->name; }))
                                ->label('TVA à appliquer par défaut'); ?>
                        <?= $form->field($model, 'document_quotation_prefix') ; ?>
                        <?= $form->field($model, 'document_quotation_first_reference') ; ?>
                        <?= $form->field($model, 'document_quotation_duration') ; ?>
                        <?= $form->field($model, 'document_invoice_prefix') ; ?>
                        <?= $form->field($model, 'document_invoice_first_reference') ; ?>
                        <?= $form->field($model, 'document_delivery_note_prefix') ; ?>
                        <?= $form->field($model, 'document_delivery_note_first_reference') ; ?>
                        <?= $form->field($model, 'document_display_orders_invoice')->dropDownList([
                            0 => 'Non',
                            1 => 'Oui'
                        ]) ; ?>
                        <?= $form->field($model, 'document_display_orders_delivery_note')->dropDownList([
                                0 => 'Non',
                                1 => 'Oui'
                        ]) ; ?>
                        <?= $form->field($model, 'document_display_prices_delivery_note')->dropDownList([
                                0 => 'Non',
                                1 => 'Oui'
                        ]) ; ?>
                        <?= $form->field($model, 'document_infos_bottom')
                            ->textarea(['rows' => 15]) ?>
                        <?= $form->field($model, 'document_infos_quotation')
                            ->textarea(['rows' => 15]) ?>
                        <?= $form->field($model, 'document_infos_invoice')
                            ->textarea(['rows' => 15]) ?>
                        <?= $form->field($model, 'document_infos_delivery_note')
                            ->textarea(['rows' => 15]) ?>
                </div>
            </div>
            
                
            <div class="form-group">
                <?= Html::submitButton('Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>




