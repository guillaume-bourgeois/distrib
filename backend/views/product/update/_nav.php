<div id="nav-params">
        <?= $this->render('_nav_item', [
                'title' => 'Général',
                'action' => 'update',
                'currentAction' => $action,
                'model' => $model,
        ]); ?>
        <?= $this->render('_nav_item', [
                'title' => 'Prix spécifiques',
                'action' => 'prices-list',
                'currentAction' => $action,
                'model' => $model,
        ]); ?>
</div>