<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\TaxRate;
use common\models\Producer;
use common\helpers\GlobalParam;
use common\models\User ;


?>

<div class="product-form">

        <?=

            $this->render('_base_price', [
                    'model' => $modelProduct,
            ]) ;

        ?>

        <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <?= $form->field($model, 'id_user')->dropDownList(User::populateDropdownList()); ?>
        <?= $form->field($model, 'id_user_group')->dropDownList(UserGroup::populateDropdownList()); ?>
        <?= $form->field($model, 'id_point_sale')->dropDownList(PointSale::populateDropdownList()); ?>

        <?php
            $producer = GlobalParam::getCurrentProducer();
            $taxRateValue = $producer->taxRate->value;
            if($modelProduct->taxRate) {
                    $taxRateValue = $modelProduct->taxRate->value ;
            }
        ?>
        <?= $form->field($model, 'price', [
                'template' => '
                        <div class="row">
                        <div class="col-xs-6">
                            <label for="product-price" class="control-label without-tax">Prix ('.Product::strUnit($modelProduct->unit, 'wording_unit').') HT</label>
                            <div class="input-group">
                            {input} <span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <label for="productprice-price-with-tax" class="control-label with-tax">Prix ('.Product::strUnit($modelProduct->unit, 'wording_unit').') TTC</label>
                            <div class="input-group">
                                <input type="text" id="productprice-price-with-tax" class="form-control" name="" value="" data-tax-rate-value="'.$taxRateValue.'">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
                            </div>
                        </div>
                        </div>',
        ]) ?>

        <div class="form-group">
                <?= Html::a('Annuler', ['prices-list', 'id' => $model->id_product], ['class' => 'btn btn-default']) ?>
                <?= Html::submitButton($model->isNewRecord ? 'Ajouter' : 'Modifier', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
