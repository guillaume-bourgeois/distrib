<a href="<?= Yii::$app->urlManager->createUrl(['product/'.$action, 'id' => $model->id]) ?>" class="btn <?php if($action == $currentAction): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">
        <?= $title ?>
        <span class="glyphicon glyphicon-triangle-bottom"></span>
</a>
