<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

$this->setTitle('Bons de livraison');
$this->addBreadcrumb($this->getTitle());
$this->addButton(['label' => 'Nouveau bon de livraison <span class="glyphicon glyphicon-plus"></span>', 'url' => ['distribution/index', 'message_generate_bl' => 1], 'class' => 'btn btn-primary']);

?>

<div class="delivery-note-index">
        <?php if(DeliveryNote::searchCount()): ?>
                <?= GridView::widget([
                        'filterModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'columns' => [
                                [
                                        'attribute' => 'status',
                                        'label' => 'Statut',
                                        'filter' => [
                                                'draft' => 'Brouillon',
                                                'valid' => 'Valide',
                                        ],
                                        'format' => 'raw',
                                        'value' => function($model) {
                                                return $model->getHtmlLabel() ;
                                        }
                                ],
                                [
                                        'attribute' => 'reference',
                                        'value' => function($model) {
                                                if(strlen($model->reference) > 0) {
                                                        return $model->reference ;
                                                }
                                                return '' ;
                                        }
                                ],
                                'name',
                                [
                                        'attribute' => 'date_distribution',
                                        'header' => 'Jour de distribution',
                                        'filter' => \yii\jui\DatePicker::widget([
                                                'language' => 'fr',
                                                'dateFormat' => 'dd/MM/yyyy',
                                                'model' => $searchModel,
                                                'attribute' => 'date_distribution',
                                                'options' => ['class' => 'form-control']
                                        ]),
                                        'value' => function($model) {
                                                $distribution = $model->getDistribution() ;
                                                if($distribution) {
                                                        return date('d/m/Y',strtotime($distribution->date)) ;
                                                }
                                                return '' ;
                                        }
                                ],
                                [
                                        'attribute' => 'id_point_sale',
                                        'header' => 'Point de vente',
                                        'filter' => ArrayHelper::map(PointSale::searchAll([], ['as_array'=>true]), 'id', 'name'),
                                        'format' => 'html',
                                        'value' => function($model) {
                                                $pointSale = $model->getPointSale() ;
                                                if($pointSale) {
                                                        return Html::encode($pointSale->name);
                                                }
                                                return '' ;
                                        }
                                ],
                                [
                                        'attribute' => 'amount',
                                        'header' => 'Montant',
                                        'value' => function($deliveryNote) {
                                                return $deliveryNote->getAmountWithTax(Order::INVOICE_AMOUNT_TOTAL, true) ;
                                        }
                                ],
                                [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{validate} {update} {delete} {send} {download}',
                                        'headerOptions' => ['class' => 'column-actions'],
                                        'contentOptions' => ['class' => 'column-actions'],
                                        'buttons' => [
                                                'send' => function($url, $model) {
                                                        return ((isset($model->user) && strlen($model->user->email) > 0) ? Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                                                                'title' => 'Envoyer', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'download' => function($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, [
                                                                'title' => 'Télécharger', 'class' => 'btn btn-default'
                                                        ]);
                                                },
                                                'validate' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                                                'title' => 'Valider', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'update' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                                'title' =>  'Modifier', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'delete' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                                'title' =>  'Supprimer', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                }
                                        ],
                                ],
                        ],
                ]); ?>
        <?php else: ?>
                <div class="alert alert-info">Aucun bon de livraison enregistré</div>
        <?php endif; ?>
</div>
