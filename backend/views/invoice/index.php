<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\Url;
use common\models\Product;
use common\models\TaxRate;
use common\models\Producer;

$this->setTitle('Factures');
$this->addBreadcrumb($this->getTitle());
$this->addButton(['label' => 'Nouvelle facture <span class="glyphicon glyphicon-plus"></span>', 'url' => 'invoice/create', 'class' => 'btn btn-primary']);

?>

<div class="invoice-index">

        <?php if(Invoice::searchCount()): ?>
                <?= GridView::widget([
                        'filterModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'columns' => [
                                [
                                        'attribute' => 'status',
                                        'label' => 'Statut',
                                        'filter' => [
                                                'draft' => 'Brouillon',
                                                'valid' => 'Valide',
                                        ],
                                        'format' => 'raw',
                                        'value' => function($model) {
                                                return $model->getHtmlLabel() ;
                                        }
                                ],
                                [
                                        'attribute' => 'reference',
                                        'value' => function($model) {
                                                if(strlen($model->reference) > 0) {
                                                        return $model->reference ;
                                                }
                                                return '' ;
                                        }
                                ],
                                'name',
                                [
                                        'attribute' => 'id_user',
                                        'header' => 'Utilisateur',
                                        'value' => function($model) {
                                                return $model->user->getUsername() ;
                                        }
                                ],
                                [
                                        'attribute' => 'date',
                                        'header' => 'Date',
                                        'value' => function($model) {
                                                return date('d/m/Y',strtotime($model->date)) ;
                                        }
                                ],
                                [
                                        'attribute' => 'amount',
                                        'header' => 'Montant',
                                        'value' => function($invoice) {
                                                return $invoice->getAmountWithTax(Order::INVOICE_AMOUNT_TOTAL, true) ;
                                        }
                                ],
                                [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{validate} {update} {delete} {send} {download}',
                                        'headerOptions' => ['class' => 'column-actions'],
                                        'contentOptions' => ['class' => 'column-actions'],
                                        'buttons' => [
                                                'validate' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                                                'title' =>  'Valider', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'send' => function($url, $model) {
                                                        return ((isset($model->user) && strlen($model->user->email) > 0) ? Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                                                                'title' =>  'Envoyer', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'download' => function($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, [
                                                                'title' =>  'Télécharger', 'class' => 'btn btn-default'
                                                        ]);
                                                },
                                                'update' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                                'title' =>  'Modifier', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                },
                                                'delete' => function ($url, $model) {
                                                        return ($model->isStatusDraft() ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                                'title' =>  'Supprimer', 'class' => 'btn btn-default'
                                                        ]) : '');
                                                }
                                        ],
                                ],
                        ],
                ]); ?>
        <?php else: ?>
                <div class="alert alert-info">Aucune facture enregistrée</div>
        <?php endif; ?>
</div>
