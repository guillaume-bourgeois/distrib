<?php

$html = '' ;

foreach($pointsSaleArray as $keyPointSale => $pointSale) {

        if(isset($ordersArray[$pointSale->id]) && count($ordersArray[$pointSale->id]) > 0) {
                $html .= '<h1>'.$pointSale->name.'</h1>' ;

                foreach ($ordersArray[$pointSale->id] as $indexPage => $orders) {

                        $html .= '<table class="">'
                                . '<thead>'
                                . '<tr>'
                                . '<th></th>';

                        foreach ($orders as $order) {
                                $html .= '<th class="th-user" text-rotate="90">'
                                        . '<div class="user">' . $order->getStrUser() . '</div>'
                                        //.'<div class="amount">'.number_format($order->amount_with_tax, 2) .' € </div>'
                                        . '</th>';
                        }

                        $html .= '</tr>'
                                . '<thead>'
                                . '<tbody>';


                        foreach ($categoriesArray as $category) {
                                if ($category) {
                                        $html .= '<tr><td class="category-name">' . $category->name . '</td><td colspan="' . (count($orders)) . '"></td></tr>';
                                }

                                foreach ($productsArray as $product) {
                                        if (($category && $product->id_product_category == $category->id) || (!$category && !$product->id_product_category)) {
                                                $html .= line_product($product, $orders);
                                        }
                                }
                        }

                        $html .= '</tbody>'
                                . '</table>';
                        $html .= '<pagebreak>';


                        $html .= '<table class="">'
                                . '<thead>'
                                . '<tr>'
                                . '<th>Client</th>'
                                . '<th>Contact</th>'
                                . '<th>Commentaire</th>'
                                . '<th>Montant</th>'
                                . '</thead>'
                                . '<tbody>';

                        foreach ($orders as $order) {
                                $html .= '<tr>';
                                $strUser = $order->getStrUser();
                                if ($producer->option_order_reference_type == Producer::ORDER_REFERENCE_TYPE_YEARLY && $order->reference && strlen($order->reference) > 0) {
                                        $strUser .= '<br />' . $order->reference;
                                }

                                $html .= '<td>' . $strUser . '</td>';
                                $contactUser = '';
                                if ($order->user) {
                                        $contactUser .= $order->user->phone . '<br />' . $order->user->email;
                                }
                                $html .= '<td>' . $contactUser . '</td>';
                                $html .= '<td>' . nl2br($order->comment) . '</td>';
                                $html .= '<td>' . number_format($order->amount_with_tax, 2) . ' € </td>';
                                $html .= '</tr>';
                        }

                        $html .= '</tbody></table>';

                        $html .= ' <pagebreak>';
                }
        }
}

$html = substr($html, 0, strlen($html) - 11) ;

echo $html ;

function line_product($product, $orders) {
        $html = '' ;

        if(has_quantity($product, $orders)) {
                $html .= '<tr>' ;
                $html .= '<td>'.$product->name.'</td>' ;

                foreach($orders as $order) {
                        $quantity = '' ;
                        foreach($order->productOrder as $productOrder) {
                                if($product->id == $productOrder->id_product) {
                                        $unit = (Product::strUnit($productOrder->unit, 'wording_short', true) == 'p.') ? '' : '&nbsp;'.Product::strUnit($productOrder->unit, 'wording_short', true) ;

                                        $quantity .= $productOrder->quantity .$unit ;
                                        if($productOrder->quantity > 1) {
                                                $quantity = '<strong>'.$quantity.'</strong>' ;
                                        }
                                }
                        }

                        $html .= '<td class="td-nb-products">'.$quantity.'</td>' ;
                }

                $html .= '</tr>' ;
        }

        return $html ;
}

function has_quantity($product, $orders) {
        foreach($orders as $order) {
                foreach($order->productOrder as $productOrder) {
                        if($product->id == $productOrder->id_product) {
                                return true ;
                        }
                }
        }

        return false ;
}
