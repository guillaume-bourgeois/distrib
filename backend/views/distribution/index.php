<?php

/**
Copyright distrib (2018)

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs
à distribuer leur production en circuits courts.

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

\backend\assets\VuejsDistributionIndexAsset::register($this);

$this->setTitle('Distributions') ;
$this->setPageTitle('Distributions') ;

?>
<div id="app-distribution-index" class="app-vuejs">
    <?php if(strlen($date)): ?>
        <span id="distribution-date"><?= $date; ?></span>
    <?php endif; ?>
    <div id="loading" v-if="showLoading">
        <img src="<?= Yii::$app->urlManagerBackend->getBaseUrl(); ?>/img/loader.gif" alt="Chargement ..." />
    </div>
    <div id="wrapper-app-distribution-index" :class="'wrapper-app-vuejs '+(loading ? '' : 'loaded')">
        <div class="col-md-4">
            <div id="calendar">
                <v-date-picker
                    is-inline
                    is-expanded
                    v-model="date"
                    popover-visibility="hidden"
                    firstDayOfWeek="1"
                    :mode="calendar.mode"
                    :formats="calendar.formats"
                    :theme-styles="calendar.themeStyles"
                    :attributes="calendar.attrs"
                    @dayclick='dayClicked'>
                ></v-date-picker>
            </div>
            <div class="clr"></div>
        </div>
        <div class="col-md-8">
            <div v-if="date">
                <div id="infos-top">
                    <div class="col-md-12">
                        <div class="info-box" id="info-box-distribution">
                            <span :class="'info-box-icon '+(distribution.active ? 'bg-green' : 'bg-red')"><i :class="'fa '+(distribution.active ? 'fa-check' : 'fa-remove')"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    <h4>Distribution du <strong>{{ dateFormat }}</strong></h4>
                                    <a @click="activeWeekDistribution" data-active="0" class="btn btn-default btn-active-week" v-if="oneDistributionWeekActive">Désactiver cette semaine</a>
                                    <a @click="activeWeekDistribution" data-active="1" class="btn btn-default btn-active-week" v-else>Activer cette semaine</a>
                                    
                                    <a @click="activeDistribution" data-active="0" class="btn btn-default" v-if="distribution.active">Désactiver ce jour</a>
                                    <a @click="activeDistribution" data-active="1" class="btn btn-default" v-else>Activer ce jour</a>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- produits -->
                    <div class="col-md-6">
                        <div class="info-box col-md-4">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-clone"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    {{ countActiveProducts }} Produits<br /><br />
                                    <button class="btn btn-default" @click="showModalProducts = true">Configurer</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <modal v-if="showModalProducts" id="modal-products" @close="showModalProducts = false">
                        <h3 slot="header">Produits</h3>
                        <div slot="body">
                            <table class="table table-condensed table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td>Actif</td>
                                        <td>Nom</td>
                                        <td class="quantity-ordered">Commandé</td>
                                        <td class="quantity-max">Maximum</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="product in products">
                                        <td>
                                            <button class="btn btn-success" v-if="product.productDistribution[0].active == 1"><span class="glyphicon glyphicon-ok"></span></button>
                                            <button class="btn btn-default" v-else data-active-product="1" :data-id-product="product.id" @click="productActiveClick"><span class="glyphicon glyphicon-ok"></span></button>
                                            <button class="btn btn-danger" v-if="product.productDistribution[0].active == 0"><span class="glyphicon glyphicon-remove"></span></button>
                                            <button class="btn btn-default" v-else data-active-product="0" :data-id-product="product.id" @click="productActiveClick"><span class="glyphicon glyphicon-remove"></span></button>
                                        </td>
                                        <td>{{ product.name }}</td>
                                        <td class="quantity-ordered">{{ product.quantity_ordered ? product.quantity_ordered + ' '+ ((product.unit == 'piece') ? ' p.' : ' '+(product.unit == 'g' || product.unit == 'kg') ? 'kg' : 'litre(s)') : '&empty;' }}</td>
                                        <td class="quantity-max">
                                            <div class="input-group">
                                                <input type="text" class="form-control quantity-max" placeholder="&infin;" :data-id-product="product.id" v-model="product.productDistribution[0].quantity_max" @keyup="productQuantityMaxChange"  />
                                                <span class="input-group-addon">{{ (product.unit == 'piece') ? 'p.' : ' '+((product.unit == 'g' || product.unit == 'kg') ? 'kg' : 'litre(s)') }}</span>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div slot="footer">
                            <div class="actions-form">
                                <button class="modal-default-button btn btn-default" @click="closeModalProducts">Fermer</button>
                            </div>
                        </div>
                    </modal>

                    <div class="col-md-6">
                        <div class="info-box col-md-4">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-map-marker"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    {{ countActivePointsSale }} Points de vente<br /><br />
                                    <button class="btn btn-default" @click="showModalPointsSale = true">Configurer</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <modal v-if="showModalPointsSale" id="modal-points-sale" @close="showModalPointsSale = false">
                        <h3 slot="header">Points de vente</h3>
                        <div slot="body">
                            <table class="table table-condensed table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td>Actif</td>
                                        <td>Nom</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="pointSale in pointsSale">
                                        <td>
                                            <button class="btn btn-success" v-if="pointSale.pointSaleDistribution[0].delivery == 1"><span class="glyphicon glyphicon-ok"></span></button>
                                            <button class="btn btn-default" v-else data-delivery-point-sale="1" :data-id-point-sale="pointSale.id" @click="pointSaleActiveClick"><span class="glyphicon glyphicon-ok"></span></button>
                                            <button class="btn btn-danger" v-if="pointSale.pointSaleDistribution[0].delivery == 0"><span class="glyphicon glyphicon-remove"></span></button>
                                            <button class="btn btn-default" v-else data-delivery-point-sale="0" :data-id-point-sale="pointSale.id" @click="pointSaleActiveClick"><span class="glyphicon glyphicon-remove"></span></button>
                                        </td>
                                        <td>{{ pointSale.name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </modal>

                    <div class="col-md-6">
                        <div id="summary-ca-weight" class="info-box col-md-4">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-euro"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">CA (TTC)</span>
                                <span class="info-box-number">{{ distribution.revenues }} <span class="normal" v-if="distribution.potential_revenues != '0,00 €'">/ {{ distribution.potential_revenues }}</span></span>
                                <span class="info-box-text">Poids</span>
                                <span class="info-box-number">{{ distribution.weight }} kg <span class="normal" v-if="distribution.potential_weight > 0">/ {{ distribution.potential_weight }} kg</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="info-box col-md-4">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-download"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    {{ countOrders }} Commande<span v-if="countOrders > 1">s</span><br />
                                    
                                    <a :href="distribution.url_report" class="btn btn-xs btn-default" v-if="countOrders > 0">Liste (PDF)</a>
                                    <a :href="distribution.url_report+'&type=csv'" class="btn btn-xs btn-default" v-if="countOrders > 0">Tableau (CSV)</a>

                                    <br v-if="producer.option_display_export_grid && countOrders > 0" />
                                    <a :href="distribution.url_report_grid" class="btn btn-xs btn-default" v-if="producer.option_display_export_grid && countOrders > 0">Grille (PDF)</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="callout callout-info" v-else>
                <h4><i class="fa fa-info"></i> Pour commencer</h4>
                <p>Veuillez choisir une date de distribution.</p>
            </div>
        </div>

      <div class="clr"></div>

        <div id="orders" class="panel panel-default" v-if="date">
            <div class="panel-heading">
                <h3 class="panel-title">Commandes <label class="label label-success" v-if="orders.length">{{ orders.length }}</label><label class="label label-danger" v-else>0</label></h3>
                <div class="buttons">
                </div>
            </div>
            <div class="panel-body">
                <order-form
                    v-if="showModalFormOrderCreate"
                    :date="date"
                    :order="orderCreate"
                    :points-sale="pointsSale"
                    :means-payment="meansPayment"
                    :users="users"
                    :products="products"
                    :producer="producer"
                    @close="showModalFormOrderCreate = false"
                    @ordercreatedupdated="orderCreatedUpdated"
                    @updateproductorderprices="updateProductOrderPrices"
                ></order-form>

                <div id="wrapper-nav-points-sale">
                        <ul id="nav-points-sale">
                                <li data-id-point-sale="0" data-id-point-sale="0" v-if="countActivePointsSale > 1" @click="pointSaleClick">
                                        <a class="btn btn-default btn-primary" v-if="idActivePointSale == 0">Tous <span class="label label-default">{{ orders.length }}</span> <span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                        <a class="btn btn-default" v-else>Tous <span class="label label-default">{{ orders.length }}</span><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                </li>
                                <li v-for="pointSale in pointsSale" :data-id-point-sale="pointSale.id" v-if="pointSale.pointSaleDistribution[0].delivery == 1" @click="pointSaleClick">
                                        <a class="btn btn-default btn-primary" v-if="idActivePointSale == pointSale.id">{{ pointSale.name }} <span class="label label-default">{{ countOrdersByPointSale[pointSale.id] }}</span><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                        <a class="btn btn-default" v-else>{{ pointSale.name }} <span class="label label-default">{{ countOrdersByPointSale[pointSale.id] }}</span><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                </li>
                        </ul>
                    <div class="clr"></div>
                </div>

                    <div id="buttons-top-orders">
                            <div class="right">
                                    <div class="dropdown">
                                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <span class="glyphicon glyphicon-file"></span> Documents
                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                    <li><a v-if="idActivePointSale > 0" @click="generateDeliveryNote" href="javascript:void(0);" >Générer un bon de livraison pour ce point de vente</a></li>
                                                    <li><a @click="generateDeliveryNoteEachUser" href="javascript:void(0);">Générer un bon de livraison pour chaque client</a></li>
                                                    <li><a @click="validateDeliveryNotes" href="javascript:void(0);">Valider les bons de livraisons</a></li>
                                            </ul>
                                    </div>
                                    <button id="btn-add-subscriptions" @click="addSubscriptions" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span> Importer les abonnements</button>
                                    <template v-if="producer.tiller == true">
                                            <button v-if="tillerIsSynchro" id="btn-tiller"  class="btn btn-success btn-xs" disabled><span class="glyphicon glyphicon-refresh"></span> Synchronisé avec Tiller</button>
                                            <button v-else id="btn-tiller" class="btn btn-xs btn-default" @click="synchroTiller"><span class="glyphicon glyphicon-refresh"></span> Synchroniser avec Tiller</button>
                                    </template>
                                    <button v-if="producer.credit" id="btn-pay-orders" class="btn btn-default btn-xs" @click="payOrders"><span class="glyphicon glyphicon-euro"></span> Payer les commandes</button>
                                    <button id="btn-add-order" @click="openModalFormOrderCreate" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter une commande</button>
                            </div>
                            <div class="left">
                                    <a v-if="false && deliveryNoteExist(idActivePointSale)" :href="UrlManager.getBaseUrl()+'delivery-note/update?id='+deliveryNotes[idActivePointSale].id" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-file"></span> Bon de livraison</a>
                            </div>
                            <div class="clr"></div>
                    </div>

                <div class="alert alert-danger" v-if="missingSubscriptions && missingSubscriptions.length > 0">
                    {{ missingSubscriptions.length }} abonnement<template v-if="missingSubscriptions.length > 1">s</template> manquant<template v-if="missingSubscriptions.length > 1">s</template> :
                    <ul>
                        <li v-for="subscription in missingSubscriptions">{{ subscription.username }}</li>
                    </ul>
                </div>

                <div class="alert alert-danger" v-if="distribution && !distribution.active && orders && orders.length > 0">
                    Attention, ce jour de distribution n'est pas activé et vous avez quand même des commandes enregistrées.
                </div>

                <table class="table table-condensed table-bordered table-hover" v-if="countOrdersByPointSale[idActivePointSale] > 0 || (idActivePointSale == 0 && orders.length > 0)">
                        <thead>
                                <tr>
                                        <th class="column-checkbox" v-if="idActivePointSale > 0">
                                                <input type="checkbox" v-model="checkboxSelectAllOrders" @change="selectAllOrdersEvent" />
                                        </th>
                                        <th class="column-origin">Origine</th>
                                        <th class="column-state">État</th>
                                        <th class="column-user">Utilisateur</th>
                                        <th class="column-point-sale" v-if="idActivePointSale == 0">Point de vente</th>
                                        <th class="column-amount">Montant</th>
                                        <th class="column-state-payment">Paiement</th>
                                        <th class="column-payment"></th>
                                        <th class="column-tiller" v-if="producer.tiller">Tiller</th>
                                        <th class="column-actions"></th>
                                        <th class="column-delivery-note"></th>
                                </tr>
                        </thead>
                    <tbody>
                        <template v-for="(order, key, index) in orders" v-if="idActivePointSale == 0 || idActivePointSale == order.id_point_sale">
                            <tr>
                                <td class="column-checkbox" v-if="idActivePointSale > 0">
                                        <input type="checkbox" v-model="order.selected" />
                                </td>
                                <td class="column-origin">
                                    <label class="label label-success" v-if="order.origin == 'user'">client</label>
                                    <label class="label label-default" v-else-if="order.origin == 'auto'">auto</label>
                                    <label class="label label-warning" v-else>admin</label>
                                </td>
                                <td class="column-state">
                                    <span class="label label-danger" v-if="order.date_delete"><span class="glyphicon glyphicon-trash"></span></span>
                                    <span class="label label-warning" v-if="order.date_update"><span class="glyphicon glyphicon-pencil"></span></span>
                                    <span class="label label-success" v-if="!order.date_update && !order.date_delete"><span class="glyphicon glyphicon-check"></span></span>
                                </td>
                                <td class="column-user">
                                    <span v-if="order.user">
                                            <template v-if="order.user.name_legal_person && order.user.name_legal_person.length">
                                                    {{ order.user.name_legal_person }}
                                            </template>
                                            <template v-else>
                                                {{ order.user.lastname+' '+order.user.name }}
                                            </template>

                                    </span>
                                    <span v-else>{{ order.username }}</span>
                                    <span v-if="order.comment && order.comment.length > 0" class="glyphicon glyphicon-comment"></span>
                                    <span v-if="order.delivery_home && order.delivery_address && order.delivery_address.length > 0" class="glyphicon glyphicon-home"></span>
                                </td>
                                <td class="column-point-sale" v-if="idActivePointSale == 0">
                                    {{ order.pointSale.name }}
                                </td>
                                <td class="column-amount">{{ order.amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+'&nbsp;€' }}</td>
                                <td class="column-state-payment">
                                    <div class="input-group">
                                        <span class="label label-success input-group-addon" v-if="order.amount_paid == order.amount">payé</span>
                                        <span class="label label-default input-group-addon" v-else-if="order.amount_paid == 0">non réglé</span>
                                        <span class="label label-default input-group-addon" v-else-if="order.amount_paid > order.amount">surplus</span>
                                        <span class="label label-warning input-group-addon" v-else-if="order.amount_paid < order.amount">reste à payer</span>

                                        <span class="glyphicon glyphicon-time" title="Paiement automatique" v-if="order.auto_payment && producer.credit && (order.amount_paid == 0 || order.amount_paid < order.amount)"></span>
                                    </div>
                                </td>
                                <td class="column-payment" v-if="producer.credit">
                                    <div class="btn-group" v-if="order.user && !order.date_delete">
                                        <button class="btn btn-xs btn-default" v-if="order.amount_paid == order.amount" @click="orderPaymentClick" :data-id-order="order.id" data-type="refund" :data-amount="order.amount">
                                            <span class="glyphicon glyphicon-euro"></span> Rembourser
                                        </button>
                                        <button class="btn btn-xs btn-default" v-else-if="order.amount_paid == 0" @click="orderPaymentClick" :data-id-order="order.id" data-type="payment" :data-amount="order.amount">
                                            <span class="glyphicon glyphicon-euro"></span> Payer
                                        </button>
                                        <button class="btn btn-xs btn-default" v-else-if="order.amount_paid < order.amount" @click="orderPaymentClick" :data-id-order="order.id" data-type="payment" :data-amount="order.amount_remaining">
                                            <span class="glyphicon glyphicon-euro"></span> Payer
                                        </button>
                                        <button class="btn btn-xs btn-default" v-else-if="order.amount_paid > order.amount" @click="orderPaymentClick" :data-id-order="order.id" data-type="refund" :data-amount="order.amount_surplus">
                                            <span class="glyphicon glyphicon-euro"></span> Rembourser
                                        </button>

                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <span class="caret"></span>
                                          <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                          <li><a href="javascript:void(0);" @click="orderPaymentModalClick" :data-id-order="order.id">Historique</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td v-if="producer.tiller" class="tiller column-tiller">
                                    <input v-if="order.tiller_synchronization == true" type="checkbox" checked="checked" :id="'checkbox-tiller-synchronization-'+order.id" :data-id-order="order.id" @change="changeSynchroTiller" />
                                    <input v-else type="checkbox" :id="'checkbox-tiller-synchronization-'+order.id" :data-id-order="order.id" @change="changeSynchroTiller" />
                                    <label :for="'checkbox-tiller-synchronization-'+order.id">Tiller</label>
                                </td>
                                <td class="column-actions">
                                    <span v-if="order.oneProductUnactivated" class="glyphicon glyphicon-warning-sign" title="Contient un produit non activé"></span>

                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);" class="" :data-id-order="order.id" @click="orderViewClick"><span :class="'glyphicon ' + ((showViewProduct && idOrderView == order.id) ? 'glyphicon-eye-close' : 'glyphicon-eye-open')"></span> Voir</a></li>
                                                <li><a href="javascript:void(0);" class="" :data-id-order="order.id" @click="updateOrderClick"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                                                <li><a href="javascript:void(0);" class="" :data-id-order="order.id" @click="deleteOrderClick"><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li>
                                                <li v-if="order.id_subscription > 0"><a class="" :href="baseUrl+'/subscription/update?id='+order.id_subscription"><span class="glyphicon glyphicon-repeat"></span> Modifier l'abonnement associé</a></li>
                                                <li v-else><a class="add-subscription" :href="baseUrl+'/subscription/create?idOrder='+order.id"><span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-repeat"></span>Créer un abonnement sur cette base</a></li>
                                        </ul>

                                    <order-form
                                        v-if="showModalFormOrderUpdate && idOrderUpdate == order.id"
                                        :date="date"
                                        :id-point-sale="idActivePointSale"
                                        :points-sale="pointsSale"
                                        :means-payment="meansPayment"
                                        :users="users"
                                        :products="products"
                                        :order="ordersUpdate[key]"
                                        :producer="producer"
                                        @close="showModalFormOrderUpdate = false"
                                        @ordercreatedupdated="orderCreatedUpdated"
                                        @updateproductorderprices="updateProductOrderPrices"
                                    ></order-form>

                                    <modal v-if="showModalPayment && idOrderPayment == order.id" class="modal-payment" @close="showModalPayment = false">
                                        <h3 slot="header">
                                            Commande du <strong>{{ dateFormat }}</strong> &gt;
                                            <strong><span v-if="order.user">{{ order.user.name +' '+order.user.lastname }}</span>
                                            <span v-else>{{ order.username }}</span></strong>
                                        </h3>
                                        <div slot="body">
                                            <div class="col-md-4">
                                                <div class="info-box">
                                                    <span :class="'info-box-icon ' +((order.amount_paid == order.amount) ? 'bg-green' : 'bg-red')"><i class="fa fa-check"></i></span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Montant</span>
                                                        <span class="info-box-number">{{ order.amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}</span>
                                                        <span class="info-box-text">
                                                            Statut<br />
                                                            <span class="label label-success" v-if="order.amount_paid == order.amount">payé</span>
                                                            <span class="label label-default" v-else-if="order.amount_paid == 0">non réglé</span>
                                                            <span class="label label-default" v-else-if="order.amount_paid > order.amount">surplus</span>
                                                            <span class="label label-warning" v-else-if="order.amount_paid < order.amount">reste à payer</span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="info-box">
                                                    <span :class="'info-box-icon ' +((order.user.credit > 0) ? 'bg-green' : 'bg-red')"><i class="fa fa-user"></i></span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Crédit utilisateur</span>
                                                        <span class="info-box-number">{{ order.user.credit.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}</span>
                                                    </div>
                                                </div>

                                                <button v-if="order.amount_paid == order.amount"
                                                    class="btn btn-default"
                                                    :data-amount="order.amount"
                                                    data-type="refund"
                                                    @click="orderPaymentClick" >
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    Rembourser {{ order.amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}
                                                </button>

                                                <button v-else-if="order.amount_paid == 0"
                                                    class="btn btn-default"
                                                    :data-amount="order.amount"
                                                    data-type="payment"
                                                    @click="orderPaymentClick">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    Payer {{ order.amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}
                                                </button>

                                                <button v-else-if="order.amount_paid > order.amount"
                                                    class="btn btn-default"
                                                    :data-amount="order.amount_surplus"
                                                    data-type="refund"
                                                    @click="orderPaymentClick">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    Rembourser {{ order.amount_surplus.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}
                                                </button>

                                                <button v-else-if="order.amount_paid < order.amount"
                                                    class="btn btn-default"
                                                    :data-amount="order.amount_remaining"
                                                    data-type="payment"
                                                    @click="orderPaymentClick">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    Payer le restant {{ order.amount_remaining.toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' €' }}
                                                </button>
                                            </div>

                                            <div class="col-md-8">
                                                <h4>Historique paiement</h4>
                                                <table class="table table-condensed table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <td>Date</td>
                                                            <td>Utilisateur</td>
                                                            <td>Action</td>
                                                            <td>- Débit</td>
                                                            <td>+ Crédit</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="creditHistory in order.creditHistory">
                                                            <td>{{ creditHistory.date }}</td>
                                                            <td>{{ creditHistory.user_action }}</td>
                                                            <td v-html="creditHistory.wording"></td>
                                                            <td v-html="creditHistory.debit"></td>
                                                            <td v-html="creditHistory.credit"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </modal>

                                </td>
                                <td class="column-delivery-note">
                                        <a v-if="order.id_delivery_note" class="btn btn-default btn-xs" :href="UrlManager.getBaseUrl()+'delivery-note/update?id='+order.id_delivery_note">
                                                <span class="glyphicon glyphicon-file"></span> BL #{{ order.id_delivery_note }}
                                        </a>
                                </td>
                            </tr>
                            <tr class="view" v-if="showViewProduct && idOrderView == order.id">
                                <td colspan="6">
                                    <strong><span class="glyphicon glyphicon-menu-right"></span> Produits</strong>
                                    <ul>
                                        <li v-for="product in products" v-if="order.productOrder[product.id].quantity > 0">
                                            {{ product.name }} : {{ order.productOrder[product.id].quantity }} {{ order.productOrder[product.id].unit == 'piece' ? ' pièce(s)' : ' '+order.productOrder[product.id].unit }} <span v-if="product.productDistribution[0].active == 0" class="glyphicon glyphicon-warning-sign" title="Ce produit n'est pas activé"></span>
                                        </li>
                                    </ul>
                                    <div v-if="order.comment && order.comment.length > 0" class="comment">
                                        <strong><span class="glyphicon glyphicon-menu-right"></span> Commentaire</strong><br />
                                        {{ order.comment }}
                                    </div>

                                    <div v-if="order.delivery_home && order.delivery_address && order.delivery_address.length > 0" class="delivery">
                                        <strong><span class="glyphicon glyphicon-menu-right"></span> Livraison à domicile</strong><br />
                                        {{ order.delivery_address }}
                                    </div>
                                </td>
                            </tr>
                        </template>
                        <tr v-if="idActivePointSale > 0">
                                <td colspan="4"><strong>Total (TTC)</strong></td>
                                <td><strong>{{ totalActivePointSale() }}</strong></td>
                                <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
                <div class="alert alert-warning" v-else>
                    Aucune commande
                </div>
            </div>
        </div>
    </div>
</div>

<!-- template for the order-form component -->
<script type="text/x-template" id="order-form-template">
    <modal class="modal-form-order" @close="$emit('close')">
        <h3 slot="header">Ajouter une commande</h3>
        <div slot="body">
            <div class="callout callout-warning" v-if="errors.length">
                <ul>
                    <li v-for="error in errors">{{ error }}</li>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <a v-if="producer.credit && order.id_user > 0 && user.id_user == order.id_user" class="btn btn-xs btn-primary btn-credit" :href="baseUrl+'/user/credit?id='+user.id_user" v-for="user in users">{{ parseFloat(user.credit).toFixed(2).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+'&nbsp;€' }}</a>
                    <label class="control-label" for="select-id-user">
                        Utilisateur
                    </label>
                    <select class="form-control" v-model="order.id_user" @change="userChange">
                        <option value="0">--</option>
                        <option v-for="user in users" :value="user.id_user">
                                <template v-if="user.name_legal_person && user.name_legal_person.length">
                                        Personne morale / {{ user.name_legal_person  }}
                                </template>
                                <template v-else>
                                        {{ user.lastname +' '+ user.name }}
                                </template>
                        </option>
                    </select>
                    <input v-model="order.username" type="text" class="form-control" placeholder="Ou saisissez ici le nom de l'utilisateur" />
                </div>
                <div class="form-group">
                    <label class="control-label" for="select-id-point-sale">Point de vente</label>
                    <select class="form-control" id="select-id-point-sale" v-model="order.id_point_sale" @change="pointSaleChange">
                        <option value="0">--</option>
                        <option v-for="pointSale in pointsSale" v-if="pointSale.pointSaleDistribution[0].delivery == 1" :value="pointSale.id"">{{ pointSale.name }}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="select-mean-payment">Moyen de paiement</label>
                    <select class="form-control" id="select-mean-payment" v-model="order.mean_payment">
                        <option value="0">--</option>
                        <option v-for="(wordingMeanPayment, keyMeanPayment) in meansPayment" :value="keyMeanPayment">{{ wordingMeanPayment }}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="textarea-comment">Commentaire</label>
                    <textarea class="form-control" id="textarea-comment" v-model="order.comment"></textarea>
                </div>
            </div>
            <div class="col-md-8">
                <label class="control-label">Produits</label>
                <table class="table table-condensed table-bordered table-hover table-products">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nom</th>
                            <th>Prix unitaire</th>
                            <th>Quantité</th>
                            <th>Reste</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="product in products" :class="(order.productOrder[product.id] > 0) ? 'product-ordered' : ''">
                            <td>
                                <span class="label label-success" v-if="product.productDistribution[0].active == 1">Actif</span>
                                <span class="label label-danger" v-else>Inactif</span>
                            </td>
                            <td>{{ product.name }}</td>
                            <td class="price">
                                    <div class="input-group">
                                            <input type="text" v-model="order.productOrder[product.id].price" class="form-control" @change="productPriceChange" :data-id-product="product.id" />
                                            <span class="input-group-addon" id="basic-addon2">€ TTC</span>
                                    </div>
                            </td>
                            <td class="quantity">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-moins" type="button" @click="productQuantityClick(product.id, order.productOrder[product.id].unit == 'piece' ? -1 : -parseFloat(product.step))"><span class="glyphicon glyphicon-minus"></span></button>
                                    </span>
                                    <input type="text" v-model="order.productOrder[product.id].quantity" class="form-control" />
                                    <span class="input-group-addon">{{ order.productOrder[product.id].unit == 'piece' ? 'p.' : order.productOrder[product.id].unit }}</span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-plus" type="button" @click="productQuantityClick(product.id, order.productOrder[product.id].unit == 'piece' ? 1 : parseFloat(product.step))"><span class="glyphicon glyphicon-plus"></span></button>
                                    </span>
                                </div>
                            </td>
                            <td class="quantity-remaining infinite" v-if="product.quantity_remaining === null || order.productOrder[product.id].unit != product.unit">&infin;</td>
                            <td class="quantity-remaining negative" v-else-if="product.quantity_remaining <= 0">{{ product.quantity_remaining }} {{ order.productOrder[product.id].unit == 'piece' ? ' p.' : ' '+(order.productOrder[product.id].unit == 'g' || order.productOrder[product.id].unit == 'kg') ? 'kg' : 'litre(s)' }}</td>
                            <td class="quantity-remaining has-quantity" v-else>{{ product.quantity_remaining }} {{ order.productOrder[product.id].unit == 'piece' ? ' p.' : ' '+(order.productOrder[product.id].unit == 'g' || order.productOrder[product.id].unit == 'kg') ? 'kg' : 'litre(s)' }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div slot="footer">
            <div class="actions-form">
                <button class="modal-default-button btn btn-primary" @click="submitFormCreate" v-if="!order.id && order.id_user > 0" data-process-credit="1">Créer et payer</button>
                <button class="modal-default-button btn btn-primary" @click="submitFormUpdate" v-if="order.id && order.id_user > 0" data-process-credit="1">Modifier et payer</button>

                <button class="modal-default-button btn btn-primary" @click="submitFormUpdate" v-if="order.id">Modifier</button>
                <button class="modal-default-button btn btn-primary" @click="submitFormCreate" v-else>Créer</button>

                <button class="modal-default-button btn btn-default" @click="$emit('close')">Annuler</button>
            </div>
        </div>
    </modal>
</script>

<!-- template for the modal component -->
<script type="text/x-template" id="modal-template">
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header"></slot>
          </div>

          <div class="modal-body">
            <slot name="body"></slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">
              <button class="modal-default-button btn btn-default" @click="$emit('close')">Fermer</button>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
</script>

