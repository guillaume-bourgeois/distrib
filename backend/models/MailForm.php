<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\models;

use common\helpers\GlobalParam;
use Yii;
use yii\base\Model;
use common\helpers\Price ;

/**
 * ContactForm is the model behind the contact form.
 */
class MailForm extends Model 
{

    public $id_distribution ;
    public $subject;
    public $message;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['subject', 'message'], 'required', 'message' => 'Champs obligatoire'],
            [['id_distribution'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'subject' => 'Sujet',
            'message' => 'Message',
            'id_distribution' => 'Distribution'
        ];
    }

    /**
     * Envoie un email aux utilisateurs définis en paramètre.
     *
     * @param array $usersArray
     * @param boolean $fromProducer
     */
    public function sendEmail($usersArray, $fromProducer = true) 
    {
        $mj = new \Mailjet\Client(
            Mailjet::getApiKey('public'),
            Mailjet::getApiKey('private'),
            true,
            ['version' => 'v3.1']
        );

        $body = ['Messages' => []] ;
        
        $messageAutoText = '' ;
        $messageAutoHtml = '' ;
        if($this->id_distribution) {
            
            $messageAutoText = '

' ;
            $messageAutoHtml = '<br /><br />' ;
            
            $distribution = Distribution::searchOne(['id' => $this->id_distribution]) ;
            
            if($distribution) {
                
                $linkOrder = Yii::$app->urlManagerProducer->createAbsoluteUrl(['order/order','slug_producer' => GlobalParam::getCurrentProducer()->slug, 'date' => $distribution->date]) ;
                $dateOrder = strftime('%A %d %B %Y', strtotime($distribution->date)) ;
                $messageAutoHtml .= '<a href="'.$linkOrder.'">Passer ma commande du '.$dateOrder.'</a>' ;
                $messageAutoText .= 'Suivez ce lien pour passer votre commande du '.$dateOrder.' :
'.$linkOrder ;
                
                $productsArray = Product::find()
                    ->where([
                        'id_producer' => GlobalParam::getCurrentProducerId(),
                    ])
                    ->innerJoinWith(['productDistribution' => function($query) use($distribution) {
                        $query->andOnCondition([
                            'product_distribution.id_distribution' => $distribution->id,
                            'product_distribution.active' => 1
                        ]); 
                    }])
                    ->orderBy('product.name ASC')
                    ->all();

                if(count($productsArray) > 1) {
                    $messageAutoHtml .= '<br /><br />Produits disponibles : <br /><ul>' ;
                    $messageAutoText .= '

Produits disponibles :
' ;
                    foreach($productsArray as $product) {
                        
                        $productDescription = $product->name ;
                        if(strlen($product->description)) {
                            $productDescription .= ' / '.$product->description ; 
                        }
                        if($product->price) {
                            $productDescription .= ' / '.Price::format($product->getPriceWithTax()) ;
                            $productDescription .= ' ('.Product::strUnit($product->unit, 'wording_unit').')' ;
                        }
                        
                        $messageAutoText .= '- '.$productDescription.'
' ;
                        $messageAutoHtml .= '<li>'.Html::encode($productDescription).'</li>' ;
                    }
                    $messageAutoHtml .= '</ul>' ;
                }
            }
        }
        
        if($fromProducer) {
            $producer = GlobalParam::getCurrentProducer() ;
            $fromEmail = $producer->getEmailOpendistrib() ;
            $fromName = $producer->name ;
        }
        else {
            $fromEmail = 'contact@opendistrib.net' ;
            $fromName = 'Opendistrib' ;
        }
        
        foreach($usersArray as $user) {
            $body['Messages'][] = [
                'From' => [
                    'Email' => $fromEmail,
                    'Name' => $fromName
                ],
                'To' => [
                    [
                        'Email' => $user['email'],
                        'Name' => $user['name'].' '.$user['lastname']
                    ]
                ],
                'Subject' => $this->subject,
                'TextPart' => $this->message.$messageAutoText,
                'HTMLPart' => nl2br($this->message).$messageAutoHtml
            ] ;

            if(count($body['Messages']) == 50) {
                $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
                $body['Messages'] = [] ;
            }
        }

        if(count($body['Messages']) > 0) {
            $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
        }
        
        $success = $response->success() ;
        
        if(!$success) {
            Yii::error($response->getBody(), 'Mailjet');
        }
        
        return $response ;
    }

}
