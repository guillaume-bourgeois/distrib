<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\models;

use common\helpers\GlobalParam;
use Yii;
use yii\base\Model;
use common\models\User ;
use common\models\Producer ;
use common\models\UserProducer ;

/**
 * ContactForm is the model behind the contact form.
 */
class AccessUserProducerForm extends Model 
{

    public $id_user ;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id_user' => 'Utilisateur',
        ];
    }
    
    public function save() 
    {
        $user = User::searchOne([
            'id' => $this->id_user
        ]) ;
        
        if($user) {
            $user->id_producer = GlobalParam::getCurrentProducerId() ;
            if($user->status != User::STATUS_PRODUCER && $user->status != User::STATUS_ADMIN) {
                $user->status = User::STATUS_PRODUCER ;
            }
            return $user->save(); 
        }
        
        return false;
    }

}
