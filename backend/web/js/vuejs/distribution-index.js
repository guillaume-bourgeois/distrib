
/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

var app = new Vue({
    el: '#app-distribution-index',
    data: {
        UrlManager: UrlManager,
        baseUrl: $('meta[name=baseurl]').attr('content'),
        date: null,
        dateFormat: null,
        loading: true,
        distribution: {
            active: false,
        },
        producer: null,
        oneDistributionWeekActive: false,
        products: [],
        countActiveProducts: 0,
        pointsSale: [],
        meansPayment: [],
        idActivePointSale: 0,
        idDefaultPointSale: 0,
        countActivePointsSale: 0,
        countOrdersByPointSale: [],
        orders: [],
        ordersUpdate: [],
        countOrders: 0,
        users: [],
        deliveryNotes: [],
        showModalProducts: false,
        showModalPointsSale: false,
        showModalFormOrderCreate: false,
        orderCreate: null,
        showModalFormOrderUpdate: false,
        idOrderUpdate: 0,
        showViewProduct: false,
        idOrderView: 0,
        showModalPayment: false,
        idOrderPayment: 0,
        showLoading: false,
        tillerIsSynchro: false,
        checkboxSelectAllOrders: false,
        messageGenerateDeliveryNoteDisplayed: false,
        missingSubscriptions: false,
        calendar: {
            mode: 'single',
            attrs: [],
            themeStyles: {
                wrapper: {
                    background: '#F7F7F7',
                    color: '#333',
                    border: 'solid 1px #e0e0e0'
                },
                header: {
                    padding: '10px 10px',
                },
                headerHorizontalDivider: {
                    borderTop: 'solid rgba(255, 255, 255, 0.2) 1px',
                    width: '80%',
                },
                weekdays: {
                    color: '#e0e0e0', 
                    fontWeight: '600', 
                    padding: '10px 10px',
                    fontSize: '2rem'
                },
                weeks: {
                    padding: '0 15px 15px 15px',
                },
                dayContent: function(object) {
                    var style = {
                        fontSize: '2rem',
                        padding: '16px',
                    };
                    
                    if(object.isHovered || object.isFocus) {
                        style.backgroundColor = '#F39C12' ;
                    }
                    
                    return style ;
                },
            },
            formats: {
                dayPopover: 'DD/MM/YYYY'
            }
        },
    },
    
    mounted: function() {
        if($('#distribution-date').size()) {
            this.date = new Date($('#distribution-date').html()) ;
            this.dateFormat = ('0' + this.date.getDate()).slice(-2)+ '/'
            + ('0' + (this.date.getMonth() +1)).slice(-2) + '/'
            + this.date.getFullYear() ;
        }
        
        this.init() ;
        this.loading = false ;
    },
    
    methods: {
        getDate: function() {
            return this.formatDate(this.date) ;
        },
        formatDate: function(date) {
            if(date) {
                return date.getFullYear() + '-'
                    + ('0' + (date.getMonth() +1)).slice(-2) + '-'
                    + ('0' + date.getDate()).slice(-2) ;
            }
            return false ;
        },
        init: function(idActivePointSale) {
            var app = this ;
            this.showLoading = true ; 
            
            axios.get("ajax-infos",{params: {date : this.getDate()}})
                .then(function(response) {
                    app.distribution = response.data.distribution ;
                    app.producer = response.data.producer ;
                    app.products = response.data.products ;
                    app.initCountActiveProducts() ;
                    app.meansPayment = response.data.means_payment ;
                    app.oneDistributionWeekActive = response.data.one_distribution_week_active ;
                    app.missingSubscriptions = response.data.missing_subscriptions ;
                    app.countOrders = 0 ;
                    if(response.data.orders) {
                        app.orders = JSON.parse(JSON.stringify(response.data.orders)) ;
                        app.ordersUpdate = JSON.parse(JSON.stringify(response.data.orders)) ;

                        for(i=0 ; i < app.orders.length ; i++) {
                            if(!app.orders[i].date_delete) {
                                app.countOrders ++ ;
                            }
                        }        
                    }
                    else {
                        app.orders = [] ;
                    }
                    
                    if(response.data.order_create) {
                        app.orderCreate = response.data.order_create ;
                        app.idDefaultPointSale = app.orderCreate.id_point_sale ;
                    }
                    
                    if(response.data.points_sale) {
                        app.pointsSale = response.data.points_sale ;
                        app.initPointsSale(idActivePointSale) ;
                    }
                    else {
                        app.pointsSale = [] ;
                    }
                    
                    if(response.data.users) {
                        app.users = response.data.users ;
                    }

                    if(response.data.delivery_notes) {
                        app.deliveryNotes = response.data.delivery_notes ;
                    }

                    app.tillerIsSynchro = response.data.tiller_is_synchro ;
                    
                    app.calendar.attrs = [] ;
                    var distributions = response.data.distributions ;
                    if(distributions.length) {
                        for(var i= 0; i < distributions.length; i++) {
                             app.calendar.attrs.push({
                                highlight: {
                                    backgroundColor: '#5cb85c',
                                },
                                contentStyle: {
                                    color: 'white',
                                },
                                dates: distributions[i].date,
                            }) ;
                        }
                    }
                    
                    app.showLoading = false ;
                    app.checkboxSelectAllOrders = false ;

                    let searchParams = new URLSearchParams(window.location.search) ;
                    if(searchParams.has('message_generate_bl') && !app.messageGenerateDeliveryNoteDisplayed) {
                        appAlerts.alert('info','Pour générer un bon de livraison, sélectionnez tout d\'abord un jour et un lieu de distribution.', 6000) ;
                        app.messageGenerateDeliveryNoteDisplayed = true ;
                    }
                }) ;
        },
        initCountActiveProducts: function() {
            this.countActiveProducts = 0 ;
            for(var i= 0; i < this.products.length; i++) {
                 if(this.products[i].productDistribution[0].active == 1) {
                     this.countActiveProducts ++ ;
                 }
            }  
        },
        initPointsSale: function(idActivePointSale) {
            this.countActivePointsSale = 0 ;
            this.setIdActivePointSale(0) ;
            for(var i= 0; i < this.pointsSale.length; i++) {
                 if(this.pointsSale[i].pointSaleDistribution[0].delivery == 1) {
                     this.countActivePointsSale ++ ;
                     this.setIdActivePointSale(this.pointsSale[i].id) ;
                 }
            }
            if(this.countActivePointsSale > 1) {
                this.setIdActivePointSale(0) ;
            }
            if(idActivePointSale) {
                this.setIdActivePointSale(idActivePointSale) ;
            }
                
            this.countOrdersByPointSale = [] ;
            for(var i = 0; i < this.pointsSale.length ; i++) {
                this.countOrdersByPointSale[this.pointsSale[i].id] = 0 ;
            }
            for(var i = 0; i < this.orders.length ; i++) {
                this.countOrdersByPointSale[this.orders[i].id_point_sale] ++ ;
            }
        },
        dayClicked: function(day) {
            this.date = day.date ;
            this.dateFormat = ('0' + this.date.getDate()).slice(-2)+ '/'
                    + ('0' + (this.date.getMonth() +1)).slice(-2) + '/'
                    + this.date.getFullYear() ;
            this.init() ;
        },
        productQuantityMaxChange: function(event) {
            axios.get("ajax-process-product-quantity-max",{params: {
                    idDistribution: this.distribution.id,
                    idProduct: event.currentTarget.getAttribute('data-id-product'),
                    quantityMax: event.currentTarget.value
            }})
            .then(function(response) {

            }) ;
        },
        productActiveClick: function(event) {
            var idProduct = event.currentTarget.getAttribute('data-id-product') ;
            var activeProduct = event.currentTarget.getAttribute('data-active-product') ;
            axios.get("ajax-process-active-product",{params: {
                    idDistribution: this.distribution.id,
                    idProduct: idProduct,
                    active: activeProduct
            }})
            .then(function(response) {
                
            }) ;
    
            for(i = 0 ; i < this.products.length ; i++) {
                if(this.products[i].id == idProduct) {
                    this.products[i].productDistribution[0].active = activeProduct ;
                }
            }
            this.initCountActiveProducts() ;
            
        },
        pointSaleActiveClick: function(event) {
            var idPointSale = event.currentTarget.getAttribute('data-id-point-sale') ;
            var deliveryPointSale = event.currentTarget.getAttribute('data-delivery-point-sale') ;
            axios.get("ajax-process-active-point-sale",{params: {
                    idDistribution: this.distribution.id,
                    idPointSale: idPointSale,
                    delivery: deliveryPointSale
            }})
            .then(function(response) {
                
            }) ;
    
            for(i = 0 ; i < this.pointsSale.length ; i++) {
                if(this.pointsSale[i].id == idPointSale) {
                    this.pointsSale[i].pointSaleDistribution[0].delivery = deliveryPointSale ;
                }
            }
            
            this.initPointsSale() ;
        },
        activeDistribution: function(event) {
            var app = this ;
            axios.get("ajax-process-active-distribution",{params: {
                    idDistribution: this.distribution.id,
                    active: event.currentTarget.getAttribute('data-active')
            }})
            .then(function(response) {
                app.init() ;
            }) ;
        },
        activeWeekDistribution: function(event) {
            var app = this ;
            axios.get("ajax-process-active-week-distribution",{params: {
                    date: this.date.getFullYear() + '-'
                        + ('0' + (this.date.getMonth() +1)).slice(-2) + '-'
                        + ('0' + this.date.getDate()).slice(-2),
                    active: event.currentTarget.getAttribute('data-active')
            }})
            .then(function(response) {
                app.init() ;
            }) ;
        },
        pointSaleClick: function(event) {
            this.setIdActivePointSale(event.currentTarget.getAttribute('data-id-point-sale')) ;
        },
        setIdActivePointSale: function(id) {
            this.idActivePointSale = id ;
            if(!id) {
                this.orderCreate.id_point_sale = this.idDefaultPointSale ;
            }
            else {
                this.orderCreate.id_point_sale = id ;
            }
        },
        orderCreatedUpdated: function() {
            this.showModalFormOrderCreate = false ;
            this.showModalFormOrderUpdate = false ;
            this.init(this.idActivePointSale) ;
        },
        deleteOrderClick: function(event) {
            var app = this ;
            var idOrder = event.currentTarget.getAttribute('data-id-order') ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"order/ajax-delete",{params: {
                idOrder: idOrder
            }})
            .then(function(response) {
                app.init(app.idActivePointSale) ;
            }) ;
        },
        updateOrderClick: function(event) {
            var idOrder = event.currentTarget.getAttribute('data-id-order') ;
            this.idOrderUpdate = idOrder ;
            this.showModalFormOrderUpdate = true ;
            this.initModalFormOrder() ;
        },
        openModalFormOrderCreate: function() {
            this.showModalFormOrderCreate = true ;
            this.initModalFormOrder() ;
            this.updateProductOrderPrices() ;
        },
        initModalFormOrder: function() {
                setTimeout(function() {
                    $('.modal-body').css('height',$(window).height()) ;
                    $('.modal-body').css('maxHeight','unset') ;
                },500);
        },
        orderPaymentModalClick: function(event) {
            var idOrder = event.currentTarget.getAttribute('data-id-order') ;
            this.idOrderPayment = idOrder ;
            this.showModalPayment = true ;
        },
        orderPaymentClick: function(event) {
            var app = this ;
            var idOrder = event.currentTarget.getAttribute('data-id-order') ;
            if(!idOrder) {
                idOrder = this.idOrderPayment ;
            }
            axios.get(UrlManager.getBaseUrlAbsolute()+"order/ajax-payment",{params: {
                idOrder: idOrder,
                type: event.currentTarget.getAttribute('data-type'),
                amount: event.currentTarget.getAttribute('data-amount')
            }})
            .then(function(response) {
                app.init(app.idActivePointSale) ;
            }) ;
        },
        orderViewClick: function(event) {
            var currentIdOrderView = event.currentTarget.getAttribute('data-id-order') ;
            if(this.idOrderView == currentIdOrderView) {
                this.showViewProduct = !this.showViewProduct ;
            }
            else {
                this.showViewProduct = true ;
                this.idOrderView = currentIdOrderView ;
            }
            
        },
        closeModalProducts: function() {
            this.showModalProducts = false ;
            this.init(this.idActivePointSale) ;
        },
        cloneOrder: function(order) {
            var clone = Object.assign({}, order) ;
            
            clone.productOrder = {} ;
            for(var key in order.productOrder) {
                clone.productOrder[key] = order.productOrder[key] ;
            }

            return clone ;
        },
        addSubscriptions: function() {
            var app = this ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-process-add-subscriptions",{params: {
                date: this.getDate()
            }})
            .then(function(response) {
                app.init(app.idActivePointSale) ;
            }) ;
        },
        synchroTiller: function() {
            var app = this ;
            this.showLoading = true ; 
            axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-process-synchro-tiller",{params: {
                date: this.getDate()
            }})
            .then(function(response) {
                app.init(app.idActivePointSale) ;
            }) ;
        },
        totalActivePointSale: function() {
            var total = 0 ;
            for(var i = 0; i < this.orders.length ; i++) {
                if(this.orders[i].id_point_sale == this.idActivePointSale) {
                    total += this.orders[i].amount ;
                }
            }
            return formatPrice(total) ;
        },
        changeSynchroTiller: function(event) {
            var app = this ;
            var idOrder = event.currentTarget.getAttribute('data-id-order') ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"order/ajax-change-synchro-tiller",{params: {
                    idOrder: idOrder,
                    boolSynchroTiller: event.currentTarget.checked ? 1 : 0
            }})
            .then(function(response) {
                app.init() ;
            }) ;
        },
        selectAllOrdersEvent: function(event) {
            var bool = event.currentTarget.checked ;
            this.selectAllOrders(bool) ;
        },
        selectAllOrders: function(bool) {
            for(var key in this.orders) {
                if(this.orders[key].id_point_sale == this.idActivePointSale) {
                    this.orders[key].selected = bool ;
                }
            }
        },
        oneOrderSelected: function() {
            for(var key in this.orders) {
                if(this.orders[key].selected == true) {
                    return true ;
                }
            }
            return false ;
        },
        generateDeliveryNote: function() {
            if(!this.oneOrderSelected()) {
                this.selectAllOrders(true) ;
            }

            if(this.oneOrderSelected()) {
                var app = this ;
                var idOrders = {} ;
                for(var key in this.orders) {
                    if(this.orders[key].selected == true) {
                        idOrders[key] = this.orders[key].id ;
                    }
                }
                axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-generate-delivery-note",{params: {
                        idOrders: JSON.stringify(idOrders)
                    }})
                    .then(function(response) {
                        appAlerts.alertResponse(response) ;
                        app.init(app.idActivePointSale) ;
                    }) ;
            }
            else {
                appAlerts.alert('danger','Veuillez sélectionner au moins une commande pour générer un bon de livraison') ;
            }
        },
        generateDeliveryNoteEachUser: function() {
            if(!this.oneOrderSelected()) {
                this.selectAllOrders(true) ;
            }

            if(this.oneOrderSelected()) {
                var app = this ;
                var idOrders = {} ;
                for(var key in this.orders) {
                    if(this.orders[key].selected == true) {
                        idOrders[key] = this.orders[key].id ;
                    }
                }
                axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-generate-delivery-note-each-user",{params: {
                        idOrders: JSON.stringify(idOrders)
                    }})
                    .then(function(response) {
                        appAlerts.alertResponse(response) ;
                        app.init(app.idActivePointSale) ;
                    }) ;
            }
            else {
                appAlerts.alert('danger','Veuillez sélectionner au moins une commande pour générer un bon de livraison') ;
            }
        },
        validateDeliveryNotes: function() {
            if(!this.oneOrderSelected()) {
                this.selectAllOrders(true) ;
            }
            if(this.oneOrderSelected()) {
                var app = this ;
                var idOrders = {} ;
                for(var key in this.orders) {
                    if(this.orders[key].selected == true) {
                        idOrders[key] = this.orders[key].id ;
                    }
                }
                axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-validate-delivery-notes",{params: {
                        idOrders: JSON.stringify(idOrders)
                    }})
                    .then(function(response) {
                        appAlerts.alertResponse(response) ;
                        app.init(app.idActivePointSale) ;
                    }) ;
            }
            else {
                appAlerts.alert('danger','Veuillez sélectionner au moins une commande pour valider un bon de livraison') ;
            }
        },
        deliveryNoteExist: function(idPointSale) {
            return typeof this.deliveryNotes[this.idActivePointSale] != 'undefined' ;
        },
        payOrders: function() {
            var app = this ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"cron/pay-orders",{params: {
                    date: app.getDate()
                }})
                .then(function(response) {
                    appAlerts.alertResponse(response) ;
                    app.init(app.idActivePointSale) ;
                }) ;
        },

        updateProductOrderPrices: function() {
            var app = this ;
            var order = null ;

            if(app.showModalFormOrderCreate) {
                order = app.orderCreate ;
            }

            if(app.showModalFormOrderUpdate && app.idOrderUpdate) {
                for (keyOrderUpdate in app.ordersUpdate) {
                    if (app.ordersUpdate[keyOrderUpdate].id == app.idOrderUpdate) {
                        order = app.ordersUpdate[keyOrderUpdate] ;
                    }
                }
            }

            if(order) {
                axios.get(UrlManager.getBaseUrlAbsolute() + "distribution/ajax-update-product-order", {
                    params: {
                        idUser: order.id_user,
                        idPointSale: order.id_point_sale
                    }
                })
                    .then(function (response) {
                        if (response.data) {
                            for (idProduct in response.data) {
                                if (app.showModalFormOrderCreate) {
                                    Vue.set(app.orderCreate.productOrder[idProduct], 'price', response.data[idProduct].price);
                                }

                                if (app.showModalFormOrderUpdate && app.idOrderUpdate) {
                                    for (keyOrderUpdate in app.ordersUpdate) {
                                        if (order.id == app.idOrderUpdate) {
                                            Vue.set(app.ordersUpdate[keyOrderUpdate].productOrder[idProduct], 'price', response.data[idProduct].price);
                                        }
                                    }
                                }
                            }
                        }
                    });
            }
        }
    },
});

Vue.component('modal', {
    template: '#modal-template'
})

Vue.component('order-form',{
    props: ['date', 'pointsSale','meansPayment', 'users', 'products', 'order', 'producer'],
    data: function() {
        return {
            errors: [],
            idPointSale: 0, 
            idUser: 0,
            username : '',
            comment: '',
            baseUrl: $('meta[name=baseurl]').attr('content'),
        } ;
    },
    template: '#order-form-template',
    methods: {
        checkForm: function() {
            this.errors = [] ;
            
            var countProducts = 0 ;
            for(var key in this.order.productOrder) {
                if(this.order.productOrder[key].quantity > 0) {
                    countProducts ++ ;
                }
            }
            
            if(this.order.id_point_sale
                && (this.order.id_user > 0 || (this.order.username && this.order.username.length)) 
                && countProducts > 0) {
                return true ;
            }
            
            if(!this.order.id_point_sale) {
                this.errors.push('Veuillez sélectionner un point de vente') ;
            }
            
            if((!this.order.id_user || this.order.id_user == 0)  && !this.order.username.length) {
                this.errors.push('Veuillez sélectionner ou saisir un utilisateur') ;
            }
            
            if(!countProducts) {
                this.errors.push('Veuillez sélectionner au moins un produit') ;
            }
        },
        submitFormCreate: function(event) {
            var app = this ;
            if(this.checkForm()) {
                var processCredit = event.currentTarget.getAttribute('data-process-credit') ;
                axios.get(UrlManager.getBaseUrlAbsolute()+"order/ajax-create",{params: {
                    date: this.date.getFullYear() + '-'
                        + ('0' + (this.date.getMonth() +1)).slice(-2) + '-'
                        + ('0' + this.date.getDate()).slice(-2),
                    idPointSale: this.order.id_point_sale,
                    idUser: this.order.id_user,
                    username: this.order.username, 
                    meanPayment: this.order.mean_payment,
                    products: JSON.stringify(this.order.productOrder),
                    comment: this.order.comment,
                    processCredit: processCredit
                }})
                .then(function(response) {
                    app.order.id_point_sale = 0 ;
                    app.order.id_user = 0 ;
                    app.order.username = '' ;
                    app.order.comment = '' ;
                    for(i=0 ; i<app.order.productOrder.length ; i++) {
                        app.order.productOrder[i] = 0 ;
                    }

                    app.$emit('ordercreatedupdated') ;
                }) ;
            }
        },
        submitFormUpdate: function(event) {
            var app = this ;
            if(this.checkForm()) {
                var processCredit = event.currentTarget.getAttribute('data-process-credit') ;
                if(processCredit == null) {
                    processCredit = 0 ;
                }

                var data = new FormData();
                data.append('date', this.date.getFullYear() + '-'
                    + ('0' + (this.date.getMonth() +1)).slice(-2) + '-'
                    + ('0' + this.date.getDate()).slice(-2));
                data.append('idOrder', this.order.id) ;
                data.append('idPointSale', this.order.id_point_sale) ;
                data.append('meanPayment', this.order.mean_payment) ;
                data.append('idUser', this.order.id_user) ;
                data.append('username', ''+this.order.username) ;
                data.append('products', JSON.stringify(this.order.productOrder)) ;
                data.append('comment', this.order.comment) ;
                data.append('processCredit', processCredit) ;

                axios.post(UrlManager.getBaseUrlAbsolute()+"order/ajax-update",data)
                    .then(function(response) {
                        app.$emit('ordercreatedupdated') ;
                    }) ;
            }
        },
        productQuantityClick: function(id_product, quantity) {
            if(!this.order.productOrder[id_product].quantity) {
                this.order.productOrder[id_product].quantity = 0 ;
            }
            if(parseFloat(this.order.productOrder[id_product].quantity) + quantity >= 0) {
                var theQuantity = parseFloat(this.order.productOrder[id_product].quantity) + parseFloat(quantity) ;
                Vue.set(this.order.productOrder, id_product, {
                    quantity: theQuantity,
                    unit: this.order.productOrder[id_product].unit,
                    price: this.order.productOrder[id_product].price
                });
            }
        },
        productPriceChange: function(event) {
            var idProduct = event.currentTarget.getAttribute('data-id-product') ;
            var price = parseFloat(event.currentTarget.value) ;
            if(isNaN(price)) {
                price = 0 ;
            }

            Vue.set(this.order.productOrder, idProduct, {
                quantity: this.order.productOrder[idProduct].quantity,
                unit: this.order.productOrder[idProduct].unit,
                price: price
            });
        },
        userChange: function(event) {
            var app = this ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"distribution/ajax-point-sale-favorite",{params: {
                idUser: app.order.id_user,
            }})
            .then(function(response) {
                app.order.id_point_sale = response.data.id_favorite_point_sale ;
                app.updateProductOrderPrices() ;
            }) ;
        },
        pointSaleChange: function(event) {
            this.updateProductOrderPrices() ;
        },
        updateProductOrderPrices: function() {
            this.$emit('updateproductorderprices') ;
        }
    }
}) ;