
var app = new Vue({
    el: '#app-report-index',
    data: {
        loading: true,
        showLoading: true,
        showReport: false,
        tableReport: [],
        currentSection: 'users',
        sections: [
            {
              name: 'Utilisateurs',
              id: 'users',
              icon: 'fa-users',
            },
            {
                name: 'Points de vente',
                id: 'points-sale',
                icon: 'fa-map-marker',
            },
            {
                name: 'Distributions',
                id: 'distributions',
                icon: 'fa-calendar',
            }
        ],
        termSearchUser: '',
        usersArray: [],
        pointsSaleArray: [],
        distributionYearsArray: [],
        distributionYear: null,
        distributionsByMonthArray: []
    },
    mounted: function() {
        this.init() ;
    },
    methods: {
        init: function() {
            var app = this ;
            axios.get("ajax-init",{params: {}})
                .then(function(response) {
                    app.usersArray = response.data.usersArray ;
                    app.pointsSaleArray = response.data.pointsSaleArray ;
                    app.distributionYearsArray = response.data.distributionYearsArray ;
                    app.distributionYear = app.distributionYearsArray[app.distributionYearsArray.length - 1] ;
                    app.distributionsByMonthArray = response.data.distributionsByMonthArray ;
                    
                    app.loading = false ;
                    app.showLoading = false ;
                });
        },
        changeSection: function(section) {
            this.currentSection = section.id ;
        }, 
        countUsers: function() {
            var count = 0 ;
            for(var i = 0; i < this.usersArray.length; i++) {
                if(this.usersArray[i].checked) {
                    count ++ ;
                }
            }
            return count ;
        },
        countPointsSale: function() {
            var count = 0 ;
            for(var i = 0; i < this.pointsSaleArray.length; i++) {
                if(this.pointsSaleArray[i].checked) {
                    count ++ ;
                }
            }
            return count ;
        },
        countDistributions: function() {
            var count = 0 ;
            for(var i in this.distributionsByMonthArray) {
                for(var j = 0; j < this.distributionsByMonthArray[i].distributions.length; j++) {
                    if(this.distributionsByMonthArray[i].distributions[j].checked) {
                        count ++ ;
                    }
                }
            }
            return count ;
        },
        countDistributionsByMonth: function(month) {
            var count = 0 ;
            for(var j = 0; j < this.distributionsByMonthArray[month].distributions.length; j++) {
                if(this.distributionsByMonthArray[month].distributions[j].checked) {
                    count ++ ;
                }
            }
            return count ;
        },
        selectDistributions: function(month) {
            var countDistributions = this.countDistributionsByMonth(month) ;
            for(var j = 0; j < this.distributionsByMonthArray[month].distributions.length; j++) {
                Vue.set(this.distributionsByMonthArray[month].distributions[j], 'checked', countDistributions ? false : true);
            }
            this.reportChange();
        },
        generateReport: function() {
            var app = this ;
            app.showLoading = true ;
            
            var data = new FormData();
            var idsUsersArray = [] ;
            for(var i = 0; i < app.usersArray.length; i++) {
                if(app.usersArray[i].checked) {
                    idsUsersArray.push(app.usersArray[i].user_id) ;
                }
            }
            var idsPointsSaleArray = [] ;
            for(var i = 0; i < app.pointsSaleArray.length; i++) {
                if(app.pointsSaleArray[i].checked) {
                    idsPointsSaleArray.push(app.pointsSaleArray[i].id) ;
                }
            }
            
            var idsDistributionsArray = [] ;
            for(var i in this.distributionsByMonthArray) {
                for(var j = 0; j < this.distributionsByMonthArray[i].distributions.length; j++) {
                    if(this.distributionsByMonthArray[i].distributions[j].checked) {
                        idsDistributionsArray.push(app.distributionsByMonthArray[i].distributions[j].id) ;
                    }
                }
            }

            data.append('users', idsUsersArray);
            data.append('pointsSale', idsPointsSaleArray);
            data.append('distributions', idsDistributionsArray);
            
            axios.post("ajax-report",data)
                .then(function(response) {
                    app.tableReport = response.data ;
                    app.showLoading = false ;
                    app.showReport = true ;
                });
            
        },
        reportChange: function() {
            this.showReport = false;
        }
    }
});
