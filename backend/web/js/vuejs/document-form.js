
/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

var app = new Vue({
    el: '#app-document-form',
    data: {
        taxRateProducer: null,
        document: [],
        deliveryNotes: [],
        idDocument: 0,
        typeDocument: '',
        idUser: '',
        productsArray: [],
        productAddId: 0,
        productAddPrice: '',
        productAddQuantity: 1,
        ordersArray: [],
        total: 0,
        total_with_tax: 0
    },
    mounted: function() {
        this.init() ;
    },
    methods: {
        formatPrice: formatPrice,
        init: function() {
                
                if(this.getDocumentId()) {
                        var app = this ;                
                        axios.get(UrlManager.getBaseUrlAbsolute()+"document/ajax-init",{params: {
                                idDocument: this.getDocumentId(),
                                classDocument: this.getDocumentClass()
                        }})
                        .then(function(response) {
                                if(response.data.return == 'success') {
                                        app.document = response.data.document ;
                                        app.taxRateProducer = response.data.tax_rate_producer ;
                                        app.idUser = response.data.id_user ;
                                        app.productsArray = response.data.products ;
                                        app.ordersArray = response.data.orders ;
                                        app.total = response.data.total ;
                                        app.total_with_tax = response.data.total_with_tax ;

                                        if(app.idUser > 0) {
                                            app.changeUser() ;
                                        }
                                }
                        }) ;
                }
        },
        getDocumentId: function() {
            var documentId = $('#app-document-form').attr('data-id-document') ;
            return documentId ;
        },
        getDocumentClass: function() {
            var documentClass = $('#app-document-form').attr('data-class-document') ;
            return documentClass ;
        },
        getProductOrderPrice: function(productOrder) {
            var documentClass = this.getDocumentClass() ;
            var price = 0 ;
            if(documentClass == 'DeliveryNote' || documentClass == 'Invoice') {
                price = productOrder.invoice_price ;
                if(!price) {
                    price = productOrder.price ;
                }
            }
            else {
                price = productOrder.price ;
            }

            return price ;
        },
        changeUser: function() {
                var app = this ;                
                axios.get(UrlManager.getBaseUrlAbsolute()+"document/ajax-user-infos",{params: {
                        idUser: app.idUser,
                        classDocument: app.getDocumentClass(),
                        idDocument: app.getDocumentId(),
                        typeAction: $('#type-action').val(),
                }})
                .then(function(response) {
                        if(response.data.return == 'success') {
                                Vue.set(app.document, 'address', response.data.address);
                                app.deliveryNotes = response.data.delivery_notes ;
                        }
                        else {
                                app.document.address = '' ;
                        }
                }) ;
        }, 
        formatPrice: formatPrice,
        validateDocument: function() {
            var app = this ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"document/ajax-validate-document",{params: {
                    idDocument: app.getDocumentId(),
                    classDocument: app.getDocumentClass(),
                }})
                .then(function(response) {
                    appAlerts.alertResponse(response) ;
                    app.init() ;
                }) ;
        },
        getStepProductAdd: function() {
            return parseInt(this.productsArray[this.productAddId].step) ;
        },
        changeProductAdd: function(event) {
                var idProduct = event.currentTarget.value ;
                this.productAddId = idProduct ;
                this.productAddPrice = parseFloat(this.productsArray[idProduct].price_with_tax).toFixed(2) ;
                this.productAddQuantity = this.getStepProductAdd() ;
        }, 
        changeQuantityProductAdd: function(quantity) {
                var step = this.getStepProductAdd() ;
                quantity = quantity * step ;
                this.productAddQuantity += quantity ;
                if(this.productAddQuantity < 1) {
                   this.productAddQuantity = step  ;
                }
        },
        submitProductAdd: function() {
                var app = this ;                
                axios.get(UrlManager.getBaseUrlAbsolute()+"document/ajax-add-product",{params: {
                        idDocument: this.getDocumentId(),
                        classDocument: this.getDocumentClass(),
                        idProduct: app.productAddId,
                        quantity: app.productAddQuantity,
                        price: app.productAddPrice,
                }})
                .then(function(response) {
                        appAlerts.alertResponse(response) ;
                        app.productAddId = 0 ;
                        app.init() ;
                }) ;   
        },
        deleteProductOrder: function(idProductOrder) {
            var app = this ;
            axios.get(UrlManager.getBaseUrlAbsolute()+"document/ajax-delete-product-order",{params: {
                    idProductOrder: idProductOrder
            }})
            .then(function(response) {
                appAlerts.alertResponse(response) ;
                app.init() ;
            }) ;
        },
        formatProductAddPrice: function() {
            this.productAddPrice = Number(this.productAddPrice).toFixed(2).replace(',', '.');
            if(isNaN(this.productAddPrice)) {
                this.productAddPrice = 0 ;
            }
        },
        formatProductAddQuantity: function() {
            this.productAddQuantity = parseInt(this.productAddQuantity) ;
            if(isNaN(this.productAddQuantity)) {
                this.productAddQuantity = 1 ;
            }
        }
    }
});


