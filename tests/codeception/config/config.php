<?php

$config =  yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../../frontend/config/main.php'),
    require(__DIR__ . '/../../../frontend/config/main-local.php'),
    [
        'id' => 'app-tests',
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=reservation_pain_tests',
                'username' => 'keun',
                'password' => 'k&mysql*$=',
                'charset' => 'utf8',
            ],
            'mailer' => [
                'useFileTransport' => true,
            ],
            'urlManager' => [
                'showScriptName' => true,
            ],
        ],
        'controllerMap' => [
            'fixture' => [
                'class' => 'yii\faker\FixtureController',
                'fixtureDataPath' => '@tests/codeception/common/fixtures/data',
                'templatePath' => '@tests/codeception/common/templates/fixtures',
                'namespace' => 'tests\codeception\common\fixtures',
            ],
            //'site' => 'app\controllers\SiteController',
            //'frontend' => 'app\controllers\FrontendController',
        ],        
    ]
);
return $config;