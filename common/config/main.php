<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

$serverName = $_SERVER['SERVER_NAME'];

return [
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'on beforeRequest' => function () {
                if (method_exists(Yii::$app->request, 'getAbsoluteUrl')) {
                        $url = Yii::$app->request->getAbsoluteUrl();
                        if ($_SERVER['SERVER_NAME'] != 'localhost' && !empty($url) && substr($url, -1) == '/' && substr($url, -5) != '.net/') {
                                $url = substr($url, 0, strlen($url) - 1);
                                Yii::$app->getResponse()->redirect($url, 301);
                                Yii::$app->end();
                        }
                }
        },
        'components' => [
                'assetManager' => [
                        'linkAssets' => YII_ENV == "dev" ? 'true' : false,
                ],
                'user' => [
                        'class' => 'yii\web\User',
                        'identityClass' => 'common\models\User',
                        'enableAutoLogin' => true,
                        'identityCookie' => [
                                'name' => 'distrib',
                                'domain' => (($serverName != 'localhost') ? '.' : '') . Yii::getAlias('@domainName'),
                                'path' => '/',
                        ]
                ],
                'session' => [
                        'class' => 'yii\web\Session',
                        'name' => 'PHPDISTRIBSESSID',
                        'cookieParams' => [
                                'domain' => (($serverName != 'localhost') ? '.' : '') . Yii::getAlias('@domainName'),
                                'httpOnly' => true,
                        ],
                ],
                'cache' => [
                        'class' => 'yii\caching\FileCache',
                ],
                'image' => [
                        'class' => 'yii\image\ImageDriver',
                        'driver' => 'GD',  //GD or Imagick
                ],
                'urlManagerProducer' => [
                        'class' => 'producer\components\UrlManagerProducer',
                        'subDomain' => Yii::getAlias('@producerSubdomain'),
                        'domainName' => Yii::getAlias('@domainName'),
                        'baseUrl' => Yii::getAlias('@baseUrl') . Yii::getAlias('@baseUrlProducer'),
                        'enablePrettyUrl' => true,
                        'showScriptName' => false,
                        'enableStrictParsing' => false,
                        'rules' => [
                                '<slug_producer:\w+>' => 'site/index',
                                '<slug_producer:\w+>/<controller>/<action>' => '<controller>/<action>',
                        ],
                ],
                'urlManagerFrontend' => [
                        'class' => 'common\components\UrlManagerCommon',
                        'subDomain' => Yii::getAlias('@frontendSubdomain'),
                        'domainName' => Yii::getAlias('@domainName'),
                        'baseUrl' => Yii::getAlias('@baseUrl') . Yii::getAlias('@baseUrlFrontend'),
                        'enablePrettyUrl' => true,
                        'showScriptName' => false,
                        'enableStrictParsing' => false,
                        'rules' => [
                        ],
                ],
                'urlManagerBackend' => [
                        'class' => 'common\components\UrlManagerCommon',
                        'subDomain' => Yii::getAlias('@backendSubdomain'),
                        'domainName' => Yii::getAlias('@domainName'),
                        'baseUrl' => Yii::getAlias('@baseUrl') . Yii::getAlias('@baseUrlBackend'),
                        'enablePrettyUrl' => true,
                        'showScriptName' => false,
                        'enableStrictParsing' => false,
                        'rules' => [
                        ],
                ],
        ],
        'language' => 'fr-FR',
];
