<?php

/**
Copyright distrib (2018)

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs
à distribuer leur production en circuits courts.

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
 */

use common\models\Producer ;
use common\helpers\GlobalParam ;
$producer = GlobalParam::getCurrentProducer() ;

?>

Bonjour <?= $user->name; ?>,

Votre commande <?= ($order->reference && strlen($order->reference) > 0) ? '<strong>N°'.$order->reference.'</strong>' : ''  ?> d'une valeur de <?= $order->getAmountWithTax(Order::AMOUNT_TOTAL, true); ?> a bien été prise en compte.

Récapitulatif des produits commandés :
<?= $order->getCartSummary(false) ?>

Elle sera à retirer le <?= date('d/m/Y',strtotime($distribution->date)) ?> au point de retrait <?= $pointSale->name ?><?php if(strlen($pointSale->locality) > 0): ?> situé à <?= Html::encode($pointSale->locality) ?><?php endif ?>.

<?php $payment_infos = Producer::getConfig('option_payment_info') ; ?>
<?php if($payment_infos && strlen($payment_infos) > 0): ?>
        Informations de paiement :
        <?= $payment_infos ?>
<?php endif; ?>

<?php $order_infos = Producer::getConfig('order_infos') ; ?>
<?php if($order_infos && strlen($order_infos) > 0): ?>
        Informations générales :
        <?= $order_infos ?>
<?php endif; ?>

À bientôt,
<?= $producer->name ?>