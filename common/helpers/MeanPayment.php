<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\helpers;

class MeanPayment {
    
    const CREDIT      = 'credit';
    const CREDIT_CARD = 'credit-card';
    const MONEY       = 'money';
    const CHEQUE      = 'cheque';
    const TRANSFER    = 'transfer';
    const OTHER       = 'other';
    
    /**
     * Retourne le libellé du moyen de paiement.
     * 
     * @param string $meanPayment
     * @return string
     */
    public static function getStrBy($meanPayment) 
    {
        switch($meanPayment) {
            case self::CREDIT_CARD : return 'Carte bancaire' ;
            case self::MONEY : return 'Espèces' ;
            case self::CHEQUE : return 'Chèque' ;
            case self::TRANSFER : return 'Virement' ;    
            case self::OTHER : return 'Autre' ;
            case self::CREDIT : return 'Crédit' ;
            default: return 'Crédit' ;
        }
    }
    
    /** 
     * etourne tous les moyens de paiement sour forme de tableau.
     * 
     * @return array
     */
    public static function getAll() 
    {
        return [
            self::CREDIT => self::getStrBy(self::CREDIT),
            self::CREDIT_CARD => self::getStrBy(self::CREDIT_CARD),
            self::MONEY => self::getStrBy(self::MONEY),
            self::CHEQUE => self::getStrBy(self::CHEQUE),
            self::TRANSFER => self::getStrBy(self::TRANSFER),
            self::OTHER => self::getStrBy(self::OTHER),
            self::CREDIT => self::getStrBy(self::CREDIT),
        ] ;
    }
    
}