<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace common\helpers;

use linslin\yii2\curl;

class Tiller
{
        var $curl;
        var $producer_tiller;
        var $provider_token;
        var $restaurant_token;
        //var $url_api = 'https://developers.tillersystems.com/api/';
        var $url_api = 'https://app.tillersystems.com/api/';

        public function __construct()
        {
                $this->curl = new curl\Curl();
                $this->producer_tiller = Producer::getConfig('tiller');
                $this->provider_token = Producer::getConfig('tiller_provider_token');
                $this->restaurant_token = Producer::getConfig('tiller_restaurant_token');
        }

        public function getOrders($date)
        {
                if ($this->producer_tiller) {
                        $orders = $this->curl->setGetParams([
                                'provider_token' => $this->provider_token,
                                'restaurant_token' => $this->restaurant_token,
                                'dateFrom' => date('Y-m-d H-i-s', strtotime($date)),
                                'dateTo' => date('Y-m-d H-i-s', strtotime($date) + 24 * 60 * 60 - 1),
                                'status' => 'CLOSED',
                        ])->get($this->url_api . 'orders');

                        return json_decode($orders);
                }
        }

        public function isSynchro($date)
        {
                if ($this->producer_tiller) {
                        $ordersTiller = $this->getOrders($date);
                        $ordersOpendistrib = Order::searchAll([
                                'distribution.date' => $date,
                                'order.tiller_synchronization' => 1
                        ]);

                        $ordersOpendistribSynchro = [];

                        if ($ordersOpendistrib) {
                                foreach ($ordersOpendistrib as $orderOpendistrib) {
                                        $ordersOpendistribSynchro[$orderOpendistrib->id] = false;
                                        if(isset($ordersTiller->orders)) {
                                                foreach ($ordersTiller->orders as $orderTiller) {
                                                        if ($orderOpendistrib->id == $orderTiller->externalId
                                                                && (int) round($orderOpendistrib->getAmountWithTax(Order::AMOUNT_TOTAL) * 100) == (int) $orderTiller->currentBill) {

                                                                $ordersOpendistribSynchro[$orderOpendistrib->id] = true;
                                                        }
                                                }
                                        }
                                }
                        }

                        foreach ($ordersOpendistribSynchro as $idOrder => $isSynchro) {
                                if (!$isSynchro) {
                                        return false;
                                }
                        }

                        return true;
                }
        }

        public function postOrder($params)
        {
                if ($this->producer_tiller) {
                        return $this->curl->setPostParams(array_merge([
                                'provider_token' => $this->provider_token,
                                'restaurant_token' => $this->restaurant_token,
                        ], $params))
                                ->post($this->url_api . 'orders');
                }
        }
}