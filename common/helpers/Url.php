<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\helpers;

class Url 
{
    public static function frontend($action = '') 
    {
        $server_name = Yii::$app->getRequest()->serverName ;
        $address = '' ;
        if($server_name == 'localhost')
        {
            $address = 'http://localhost/reservation_pain/frontend/web/' ;
        }
        elseif($server_name == 'www.laboiteapain.net' || 
               $server_name ==  'boulanger.laboiteapain.net')
        {
            $address = 'http://www.laboiteapain.net/' ;
        }
        elseif($server_name == 'dev.laboiteapain.net' || 
               $server_name ==  'boulanger-dev.laboiteapain.net') 
        {
            $address = 'http://dev.laboiteapain.net/' ;
        }
        elseif($server_name == 'demo.laboiteapain.net' || 
               $server_name ==  'boulanger-demo.laboiteapain.net') 
        {
            $address = 'http://demo.laboiteapain.net/' ;
        }
        
        if(strlen($action))
        {
            $address .= 'index.php?r='.$action ;
        }
        
        return $address ;
    }
    
    public static function backend($action = '')
    {
        $server_name = Yii::$app->getRequest()->serverName ;
        $address = '' ;
        if($server_name == 'localhost')
        {
            $address = 'http://localhost/reservation_pain/backend/web/' ;
        }
        elseif($server_name == 'www.laboiteapain.net' || 
               $server_name ==  'boulanger.laboiteapain.net')
        {
            $address = 'http://boulanger.laboiteapain.net/' ;
        }
        elseif($server_name == 'dev.laboiteapain.net' || 
               $server_name ==  'boulanger-dev.laboiteapain.net') 
        {
            $address = 'http://boulanger-dev.laboiteapain.net/' ;
        }
        elseif($server_name == 'demo.laboiteapain.net' || 
               $server_name ==  'boulanger-demo.laboiteapain.net') 
        {
            $address = 'http://boulanger-demo.laboiteapain.net/' ;
        }
        
        if(strlen($action))
        {
            $address .= 'index.php?r='.$action ;
        }
        
        return $address ;
    }   
    
    public static function env($env, $section)
    {
        $server_name = Yii::$app->getRequest()->serverName ;
        
        if($section == 'backend')
        {
            if($server_name == 'localhost')
            {
                return 'http://localhost/reservation_pain/backend/web/' ;
            }
            else {
                if($env == 'prod')
                {
                    return 'http://boulanger.laboiteapain.net/' ;
                }
                else {
                    return 'http://boulanger-'.$env.'.laboiteapain.net/' ;
                }
            }
        }
        else {
            if($server_name == 'localhost')
            {
                return 'http://localhost/reservation_pain/frontend/web/' ;
            }
            else {
                if($env == 'prod')
                {
                    return 'http://www.laboiteapain.net/' ;
                }
                else {
                    return 'http://'.$env.'.laboiteapain.net/' ;
                }
            }
        }
    }
    
    public static function slugify($string) 
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '', $string)));
    }
}
