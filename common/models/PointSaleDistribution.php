<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use common\helpers\GlobalParam;
use Yii;
use common\components\ActiveRecordCommon ;
use common\models\PointVente;
use common\models\Production;

/**
 * This is the model class for table "production_point_vente".
 *
 * @property integer $id_distribution
 * @property integer $id_point_sale
 * @property integer $delivery
 */
class PointSaleDistribution extends ActiveRecordCommon 
{

    var $points_sale_distribution;

    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'point_sale_distribution';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id_distribution', 'id_point_sale'], 'required'],
            [['id_distribution', 'id_point_sale', 'delivery'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id_distribution' => 'Distribution',
            'id_point_sale' => 'Point de vente',
            'delivery' => 'Livraison',
        ];
    }

    /*
     * Relations
     */
    
    public function getDistribution() 
    {
        return $this->hasOne(Distribution::className(), ['id' => 'id_distribution']);
    }

    public function getPointSale() 
    {
        return $this->hasOne(PointSale::className(), ['id' => 'id_point_sale']);
    }
    
    /**
     * Retourne les options de base nécessaires à la fonction de recherche.
     * 
     * @return array
     */
    public static function defaultOptionsSearch() {
        return [
            'with' => ['distribution', 'pointSale'],
            'join_with' => [],
            'orderby' => '',
            'attribute_id_producer' => ''
        ] ;
    }

    /**
     * Définit les jours de livraisons des points de vente pour un jour de 
     * production donné.
     * 
     * @param integer $id_production
     * @param boolean $bool_livraison
     */
    public static function setAll($idDistribution, $boolDelivery) 
    {   
        // liaison PointSale / Distribution    
        $arrPointsSale = PointSale::find()
            ->with(['pointSaleDistribution' => function($q) use ($idDistribution) {
                $q->where(['id_distribution' => $idDistribution]);
            }])
            ->where([
                'id_producer' => GlobalParam::getCurrentProducerId(),
            ])
            ->all();
        
        foreach ($arrPointsSale as $pointSale) {
            if(!$pointSale->pointSaleDistribution) {
                $pointSaleDistribution = new PointSaleDistribution();
                $pointSaleDistribution->id_distribution = $idDistribution;
                $pointSaleDistribution->id_point_sale = $pointSale->id;
                $pointSaleDistribution->save();
            }
        }

        $distribution = Distribution::findOne($idDistribution);

        if ($distribution) {
            $day = date('N', strtotime($distribution->date));

            $arrPointsSaleDistribution = self::searchAll([
                'id_distribution' => $idDistribution
            ]) ;
                    
            foreach ($arrPointsSaleDistribution as $pointSaleDistribution) {
                if ($boolDelivery &&
                    (($day == 1 && $pointSaleDistribution->pointSale->delivery_monday) ||
                    ($day == 2 && $pointSaleDistribution->pointSale->delivery_tuesday) ||
                    ($day == 3 && $pointSaleDistribution->pointSale->delivery_wednesday) ||
                    ($day == 4 && $pointSaleDistribution->pointSale->delivery_thursday) ||
                    ($day == 5 && $pointSaleDistribution->pointSale->delivery_friday) ||
                    ($day == 6 && $pointSaleDistribution->pointSale->delivery_saturday) ||
                    ($day == 7 && $pointSaleDistribution->pointSale->delivery_sunday)
                )) {
                    $pointSaleDistribution->delivery = 1;
                } else {
                    $pointSaleDistribution->delivery = 0;
                }

                $pointSaleDistribution->save();
            }
        }
    }

}
