<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace common\models;


use common\helpers\GlobalParam;

class DeliveryNoteSearch extends DeliveryNote
{
        public $id_point_sale ;
        public $date_distribution ;

        public function rules()
        {
                return [
                        [[], 'safe'],
                        [['comment', 'address', 'status', 'date_distribution'], 'string'],
                        [['id_user', 'id_point_sale'], 'integer'],
                        [['name', 'reference'], 'string', 'max' => 255],
                ];
        }

        public function search($params)
        {
                $optionsSearch = self::defaultOptionsSearch();

                $query = DeliveryNote::find()
                        ->with($optionsSearch['with'])
                        ->joinWith($optionsSearch['join_with'])
                        ->where(['delivery_note.id_producer' => GlobalParam::getCurrentProducerId()])
                        ->orderBy('delivery_note.status ASC, delivery_note.reference DESC')
                        ->groupBy('delivery_note.id');

                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'sort' => ['attributes' => ['name', 'reference', 'date']],
                        'pagination' => [
                                'pageSize' => 20,
                        ],
                ]);

                $this->load($params);
                if (!$this->validate()) {
                        return $dataProvider;
                }

                $query->andFilterWhere(['like', 'delivery_note.name', $this->name]);
                $query->andFilterWhere(['like', 'delivery_note.reference', $this->reference]);
                $query->andFilterWhere(['like', 'delivery_note.status', $this->status]);

                if($this->id_point_sale) {
                        $query->andWhere(['order.id_point_sale' => $this->id_point_sale]);
                }

                if($this->date_distribution && strlen($this->date_distribution)) {
                        $query->andFilterWhere(['like', 'distribution.date', date('Y-m-d',strtotime(str_replace('/','-',$this->date_distribution)))]);
                }

                return $dataProvider;
        }
}