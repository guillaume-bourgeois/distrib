<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\Subscription;
use common\models\SubscriptionProduct;

/**
 * Login form
 */
class SubscriptionForm extends Model 
{
    public $isAdmin = false ;
    public $id;
    public $id_user;
    public $username;
    public $id_producer;
    public $id_point_sale;
    public $date_begin;
    public $date_end;
    public $monday;
    public $tuesday;
    public $wednesday;
    public $thursday;
    public $friday;
    public $saturday;
    public $sunday;
    public $week_frequency;
    public $products;
    public $auto_payment;
    public $comment ;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id_producer', 'week_frequency', 'id_point_sale','id'], 'integer'],
            [['date_begin', 'date_end'], 'date', 'format' => 'php:d/m/Y'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'auto_payment'], 'boolean'],
            [['id_point_sale', 'id_producer', 'date_begin'], 'required', 'message' => 'Champs obligatoire'],
            [['products', 'id_user', 'username','comment'], 'safe'],
            ['id_user', function ($attribute, $params) {
                    if (!$this->id_user && !strlen($this->username)) {
                        $this->addError($attribute, 'Vous devez sélectionner ou saisir un utilisateur.');
                    }
                }, 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'id_user' => 'Utilisateur',
            'id_producer' => 'Producteur',
            'id_point_sale' => 'Point de vente',
            'date_begin' => 'Date de début',
            'date_end' => 'Date de fin',
            'monday' => 'Lundi',
            'tuesday' => 'Mardi',
            'wednesday' => 'Mercredi',
            'thursday' => 'Jeudi',
            'friday' => 'Vendredi',
            'saturday' => 'Samedi',
            'sunday' => 'Dimanche',
            'week_frequency' => 'Périodicité (semaines)',
            'username' => 'Nom d\'utilisateur',
            'auto_payment' => 'Paiement automatique',
            'comment' => 'Commentaire'
        ];
    }

    /**
     * Enregistre l'abonnement.
     * 
     * @return boolean
     */
    public function save() 
    {
        if ($this->id) {
            $subscription = Subscription::searchOne(['id' => $this->id]) ;
        } 
        else {
            $subscription = new Subscription ;
        }

        if ($subscription) {
            $subscription->id_user = $this->id_user;
            $subscription->username = $this->username;
            $subscription->id_producer = $this->id_producer;
            $subscription->id_point_sale = $this->id_point_sale;
            $subscription->date_begin = date(
                'Y-m-d', 
                strtotime(str_replace('/', '-', $this->date_begin)
            ));
            if (strlen($this->date_end)) {
                $subscription->date_end = date(
                    'Y-m-d', 
                    strtotime(str_replace('/', '-', $this->date_end)
                ));
            }
            $subscription->monday = $this->monday;
            $subscription->tuesday = $this->tuesday;
            $subscription->wednesday = $this->wednesday;
            $subscription->thursday = $this->thursday;
            $subscription->friday = $this->friday;
            $subscription->saturday = $this->saturday;
            $subscription->sunday = $this->sunday;
            $subscription->week_frequency = $this->week_frequency;
            $subscription->auto_payment = $this->auto_payment;
            $subscription->comment = $this->comment;

            $subscription->save();

            // produits
            if ($this->id) {
                $productsSubscriptionsArray = ProductSubscription::findAll(['id_subscription' => $this->id]) ;
                ProductSubscription::deleteAll(['id_subscription' => $this->id]);
            }

            foreach ($this->products as $nameInput => $quantity) {
                if ($quantity) {
                    $idProduct = (int) str_replace('product_', '', $nameInput);
                    $product = Product::findOne($idProduct) ;

                    $newProductSubscription = new ProductSubscription;
                    $newProductSubscription->id_subscription = $subscription->id;
                    $newProductSubscription->id_product = $idProduct;
                    $newProductSubscription->quantity = $quantity / Product::$unitsArray[$product->unit]['coefficient'];

                    $newProductSubscription->save();
                }
            }
            
            if(!$this->id) {
                $this->id = $subscription->id ;
            }
            
        }
        return true;
    }

}
