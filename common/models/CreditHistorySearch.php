<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models ;

use common\helpers\GlobalParam;
use common\models\CreditHistory ;

class CreditHistorySearch extends CreditHistory
{
    
    public function rules() 
    {
        return [
            [['id_user', 'id_user_action', 'id_order', 'id_producer'], 'integer'],
            [['date'], 'safe'],
            [['amount'], 'double'],
            [['type', 'mean_payment', 'comment'], 'string', 'max' => 255],
        ];
    }
    
    public function search($params) 
    {
        $optionsSearch = self::defaultOptionsSearch() ;
        
        $query = CreditHistory::find()
            ->with($optionsSearch['with'])
            ->innerJoinWith($optionsSearch['join_with'], true)
            ->where(['credit_history.id_producer' => GlobalParam::getCurrentProducerId()])
            ->orderBy('id DESC')
            ;
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if(isset($this->id_user) && is_numeric($this->id_user)) {
            $query->andWhere([
                'credit_history.id_user' => $this->id_user
            ]) ;    
        }

        return $dataProvider;
    }
    
}