<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use common\helpers\GlobalParam;
use common\models\Subscription ;

class SubscriptionSearch extends Subscription
{
    var $username; 
    var $product_name; 
    var $day ;
    
    public function rules() 
    {
        return [
            [['id_point_sale', 'week_frequency'], 'integer'],
            [['auto_payment'], 'boolean'],
            [['date_begin', 'username','product_name', 'day'], 'safe'],
        ];
    }
    
    public function search($params) {
        
        $optionsSearch = self::defaultOptionsSearch() ;
        
        $query = Subscription::find()
            ->with($optionsSearch['with'])
            ->joinWith($optionsSearch['join_with'], true)
            ->where(['subscription.id_producer' => GlobalParam::getCurrentProducerId()])
            ->groupBy('subscription.id')
            ;
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username']],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.lastname' => SORT_ASC, 'user.name' => SORT_ASC],
            'desc' => ['user.lastname' => SORT_DESC, 'user.name' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['id_point_sale'] = [
            'asc' => ['point_sale.name' => SORT_ASC],
            'desc' => ['point_sale.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['auto_payment'] = [
            'asc' => ['subscription.auto_payment' => SORT_ASC],
            'desc' => ['subscription.auto_payment' => SORT_DESC],
        ];
        
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        if(isset($this->id_user) && is_numeric($this->id_user)) {
            $query->andWhere([
                'subscription.id_user' => $this->id_user
            ]) ;    
        }
        
        if(isset($this->id_point_sale) && is_numeric($this->id_point_sale)) {
            $query->andWhere([
                'subscription.id_point_sale' => $this->id_point_sale
            ]) ;    
        }
        
        if(isset($this->auto_payment) && is_numeric($this->auto_payment)) {
            $query->andWhere([
                'subscription.auto_payment' => $this->auto_payment
            ]) ;    
        }
        
        if(isset($this->week_frequency) && is_numeric($this->week_frequency)) {
            $query->andWhere([
                'subscription.week_frequency' => $this->week_frequency
            ]) ;    
        }
        
        if(isset($this->day) && strlen($this->day)) {
            $query->andWhere([
                'subscription.'.$this->day => 1
            ]) ;
        }
        
        if(strlen($this->date_begin)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->date_begin);
            $query->andWhere([
                'subscription.date_begin' => date('Y-m-d',$date->format('U'))
            ]) ;
        }
        
        $query->andFilterWhere([
            'or',
            ['like', 'user.lastname', $this->username],
            ['like', 'user.name', $this->username],
            ['like', 'subscription.username', $this->username]
        ]);
        
        $query->andFilterWhere(['like', 'product.name', $this->product_name]) ;
        
        
        return $dataProvider;
    }
    
}