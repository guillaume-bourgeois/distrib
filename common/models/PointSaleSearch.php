<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models ;

use common\helpers\GlobalParam;
use common\models\PointSale ;

class PointSaleSearch extends PointSale 
{
    var $delivery ;
    var $access_type ;
    
    public function rules() 
    {
        return [
            [['restricted_access'], 'boolean'],
            [['name', 'code'], 'string', 'max' => 255],
            [['address', 'locality', 'infos_monday', 'infos_tuesday', 
                'infos_wednesday', 'infos_thursday', 'infos_friday', 
                'infos_saturday', 'infos_sunday'], 'string'],
            [['point_production', 'credit', 'delivery_monday', 'delivery_tuesday', 
                'delivery_wednesday', 'delivery_thursday', 'delivery_friday', 
                'delivery_saturday', 'delivery_sunday'], 'boolean'],
            ['id_producer', 'integer'],
            [['users', 'users_comment', 'code','name', 'delivery', 'access_type'], 'safe']
        ];
    }
    
    public function search($params) 
    {
        $optionsSearch = self::defaultOptionsSearch() ;
        
        $query = PointSale::find()
            ->with($optionsSearch['with'])
            ->innerJoinWith($optionsSearch['join_with'], true)
            ->where(['point_sale.id_producer' => GlobalParam::getCurrentProducerId()])
            ;
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['name', 'locality','point_production','credit']],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        if(isset($this->point_production) && is_numeric($this->point_production)) {
            $query->andWhere([
                'point_sale.point_production' => $this->point_production
            ]) ;    
        }
        
        if(isset($this->delivery) && strlen($this->delivery)) {
            $query->andWhere([
                'point_sale.delivery_'.$this->delivery => 1
            ]) ;    
        }
        
        if(isset($this->access_type) && strlen($this->access_type)) {
            if($this->access_type == 'open') {
                $query->andWhere(['or',
                    ['=','point_sale.code',''],
                    ['IS','point_sale.code',null],
                ]) ;
                $query->andWhere(['point_sale.restricted_access' => 0]) ;
            }
            elseif($this->access_type == 'code') {
                $query->andWhere(['!=','point_sale.code','']) ;
            }
            elseif($this->access_type == 'restricted_access') {
                $query->andWhere(['point_sale.restricted_access' => 1]) ;
            }    
        }
        
        $query->andFilterWhere(['like', 'point_sale.name', $this->name]) ;
        $query->andFilterWhere(['like', 'point_sale.locality', $this->locality]) ;
        
        return $dataProvider;
    }
    
}