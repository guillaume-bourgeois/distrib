<?php

namespace common\models;

use common\components\ActiveRecordCommon;
use Yii;

/**
 * This is the model class for table "tax_rate".
 *
 * @property integer $id
 * @property string $name
 * @property double $pourcent
 */
class TaxRate extends ActiveRecordCommon
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nom',
            'value' => 'Valeur (0.2 pour 20%)',
        ];
    }
    
    /**
     * Retourne les options de base nécessaires à la fonction de recherche.
     * 
     * @return array
     */
    public static function defaultOptionsSearch() {
        return [
            'with' => [],
            'join_with' => [],
            'orderby' => 'pourcent ASC',
            'attribute_id_producer' => ''
        ] ;
    }
}
