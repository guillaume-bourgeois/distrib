<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace common\models;

use common\helpers\GlobalParam;
use common\models\User;

class UserSearch extends User
{
        var $id_point_sale;
        var $subscribers;
        var $inactive;
        var $username;

        public function rules()
        {
                return [
                        [['no_mail', 'mail_distribution_monday', 'mail_distribution_tuesday', 'mail_distribution_wednesday', 'mail_distribution_thursday', 'mail_distribution_friday', 'mail_distribution_saturday', 'mail_distribution_sunday'], 'boolean'],
                        [['lastname', 'name', 'phone', 'address'], 'string'],
                        [['id_point_sale', 'inactive', 'subscribers'], 'integer'],
                        [['date_last_connection', 'id_point_sale', 'username'], 'safe'],
                ];
        }

        public function search($params = [])
        {
                $optionsSearch = self::defaultOptionsSearch();

                $query = User::find()
                        ->select(
                                '`user`.id, '
                                . '`user`.username,'
                                . '`user`.email, '
                                . '`user`.status, '
                                . '`user`.created_at, '
                                . '`user`.updated_at, '
                                . '`user`.lastname, '
                                . '`user`.name, '
                                . '`user`.phone, '
                                . '`user`.address, '
                                . '`user`.no_mail, '
                                . '`user`.mail_distribution_monday, '
                                . '`user`.mail_distribution_tuesday, '
                                . '`user`.mail_distribution_wednesday, '
                                . '`user`.mail_distribution_thursday, '
                                . '`user`.mail_distribution_friday, '
                                . '`user`.mail_distribution_saturday, '
                                . '`user`.mail_distribution_sunday, '
                                . '`user`.id_producer, '
                                . '`user`.date_last_connection, '
                                . '`user`.name_legal_person, '
                                . '(SELECT COUNT(*) FROM `order` WHERE `user`.id = `order`.id_user) AS count_orders');

                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'sort' => ['attributes' => ['username', 'credit', 'orders']],
                        'pagination' => [
                                'pageSize' => 20,
                        ],
                ]);

                $dataProvider->sort->attributes['username'] = [
                        'asc' => ['user.lastname' => SORT_ASC, 'user.name' => SORT_ASC],
                        'desc' => ['user.lastname' => SORT_DESC, 'user.name' => SORT_DESC],
                ];

                $dataProvider->sort->attributes['credit'] = [
                        'asc' => ['user_producer.credit' => SORT_ASC],
                        'desc' => ['user_producer.credit' => SORT_DESC],
                ];

                $this->load($params);
                if (!$this->validate()) {
                        return $dataProvider;
                }

                $query->innerJoin('user_producer', 'user.id = user_producer.id_user AND user_producer.id_producer = :id_producer AND user_producer.active = 1', [':id_producer' => GlobalParam::getCurrentProducerId()]);

                if (isset($this->id_point_sale) && $this->id_point_sale) {
                        $pointSale = PointSale::findOne(['id' => $this->id_point_sale]);
                        $query->innerJoin('user_point_sale', 'user.id = user_point_sale.id_user AND user_point_sale.id_point_sale = :id_point_sale', [':id_point_sale' => $this->id_point_sale]);
                }

                if (isset($this->subscribers) && $this->subscribers) {
                        $query->innerJoin(
                                'subscription',
                                'user.id = subscription.id_user AND subscription.id_producer = :id_producer',
                                [':id_producer' => GlobalParam::getCurrentProducerId()]
                        )->groupBy('user.id');
                }

                if (isset($this->inactive) && $this->inactive) {
                        $query->having([
                                'count_orders' => 0
                        ]);
                }

                $query->andFilterWhere([
                        'or',
                        ['like', 'user.lastname', $this->username],
                        ['like', 'user.name', $this->username],
                        ['like', 'user.name_legal_person', $this->username],
                ]);

                return $dataProvider;
        }

}