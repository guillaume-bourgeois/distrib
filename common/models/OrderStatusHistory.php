<?php

namespace common\models;

use common\components\ActiveRecordCommon;
use Yii;

/**
 * This is the model class for table "order_order_status".
 *
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_order_status
 * @property string $date
 */
class OrderStatusHistory extends ActiveRecordCommon
{
        /**
         * @inheritdoc
         */
        public static function tableName()
        {
                return 'order_status_history';
        }

        /**
         * @inheritdoc
         */
        public function rules()
        {
                return [
                        [['id_order', 'status', 'id_user', 'date', 'origin'], 'required'],
                        [['id_order', 'id_user',], 'integer'],
                        [['date'], 'safe'],
                ];
        }

        /**
         * @inheritdoc
         */
        public function attributeLabels()
        {
                return [
                        'id' => 'ID',
                        'id_order' => 'Commande',
                        'id_user' => 'Utilisateur',
                        'status' => 'Statut',
                        'origin' => 'Origine',
                        'date' => 'Date',
                ];
        }

        /*
         * Relations
         */

        public function getOrder()
        {
                return $this->hasOne(User::className(), ['id' => 'id_order']);
        }

        public function getUser()
        {
                return $this->hasOne(User::className(), ['id' => 'id_user']);
        }

        /**
         * Retourne les options de base nécessaires à la fonction de recherche.
         *
         * @return array
         */
        public static function defaultOptionsSearch()
        {
                return [
                        'with' => [],
                        'join_with' => ['order', 'orderStatus'],
                        'orderby' => 'date ASC',
                        'attribute_id_producer' => ''
                ];
        }
}
