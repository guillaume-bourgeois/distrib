<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model 
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $email;
    private $_user = false;
    public $id_producer ;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Champs obligatoire'],
            ['email', 'email'],
            // username and password are both required
            [ 'password', 'required', 'message' => 'Champs obligatoire'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['id_producer', 'integer'],
            ['id_producer', function($attribute, $params) {
                    if ($this->id_producer) {
                        $producer = Producer::findOne($this->id_producer);
                        if (!$producer) {
                            $this->addError($attribute, 'Ce producteur n\'existe pas.');
                        }
                    }
                }],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) 
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Email ou mot de passe incorrect');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() 
    {
        if ($this->validate()) {
            $this->updateLastConnection();
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() 
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'username' => 'Identifiant',
            'password' => 'Mot de passe',
            'rememberMe' => 'Se souvenir de moi',
            'email' => 'Email',
            'id_producer' => 'Producteur'
        ];
    }

    /**
     * Met à jour la date de dernière connexion de l'utilisateur.
     */
    public function updateLastConnection() 
    {
        $user = $this->getUser();
        $user->date_last_connection = date('Y-m-d H:i:s');
        $user->save();
    }

}
