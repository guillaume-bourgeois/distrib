<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use Yii;
use common\components\ActiveRecordCommon ;
use common\models\Producer;

/**
 * This is the model class for table "development_priority".
 *
 * @property integer $id_user
 * @property integer $id_development
 */
class DevelopmentPriority extends ActiveRecordCommon 
{

    const PRIORITY_HIGH     = 'high';
    const PRIORITY_NORMAL   = 'normal';
    const PRIORITY_LOW      = 'low';

    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'development_priority';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id_producer', 'id_development'], 'required'],
            [['id_producer', 'id_development'], 'integer'],
            [['priority'], 'string'],
        ];
    }
    
    /*
     * Relations
     */

    public function getProducer() 
    {
        return $this->hasOne(Producer::className(), ['id' => 'id_producer']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id_producer' => 'Producteur',
            'id_producer' => 'Développement',
            'priority' => 'Priorité'
        ];
    }
    
    /**
     * Retourne les options de base nécessaires à la fonction de recherche.
     * 
     * @return array
     */
    public static function defaultOptionsSearch() {
        return [
            'with' => [],
            'join_with' => [],
            'orderby' => '',
            'attribute_id_producer' => 'development_priority.id_producer'
        ] ;
    }

    /**
     * Retourne la priorité.
     * 
     * @return string
     */
    public function getStrPriority() 
    {
        switch ($this->priority) {
            case self::PRIORITY_LOW : return 'Basse';
                break;
            case self::PRIORITY_NORMAL : return 'Normale';
                break;
            case self::PRIORITY_HIGH : return 'Haute';
                break;
            default: return 'Non définie';
                break;
        }
    }

    /**
     * Retourne la classe CSS du bouton servant à définir la priorité.
     * 
     * @return string
     */
    public function getClassCssStyleButton() 
    {
        $styleButton = 'default';
        if ($this->priority == DevelopmentPriority::PRIORITY_LOW) {
            $styleButton = 'info';
        }
        elseif ($this->priority == DevelopmentPriority::PRIORITY_NORMAL) {
            $styleButton = 'warning';
        }
        elseif ($this->priority == DevelopmentPriority::PRIORITY_HIGH) {
            $styleButton = 'danger';
        }
        
        return $styleButton;
    }

}
