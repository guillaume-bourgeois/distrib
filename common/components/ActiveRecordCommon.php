<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace common\components;

use common\helpers\GlobalParam;

class ActiveRecordCommon extends \yii\db\ActiveRecord
{
        const SEARCH_ALL = 'all';
        const SEARCH_ONE = 'one';
        const SEARCH_COUNT = 'count';

        /**
         * Méthode générique de recherche utilisée pour tous les modèles. Elle a
         * pour but de construire la requête et de retourner le résultat.
         *
         * @param array $params
         * @param array $options
         * @return mixed
         * @throws NotFoundHttpException
         */
        public static function searchBy($params = [], $options = [])
        {
                $class = get_called_class();

                if (is_callable([$class, 'defaultOptionsSearch'])) {
                        $default_options = $class::defaultOptionsSearch();
                } else {
                        throw new \ErrorException('La méthode "defaultOptionsSearch" n\'est '
                                . 'pas définie dans la classe "' . $class . '"');
                }

                $options = array_merge($default_options, $options);

                $pk = $class::primaryKey();
                $pk = $class::tableName() . '.' . $pk[0];

                if (isset($options['attribute_id_producer']) && strlen($options['attribute_id_producer'])
                        && !isset($params[$options['attribute_id_producer']]) && !Yii::$app->user->isGuest) {
                        $params[$options['attribute_id_producer']] = GlobalParam::getCurrentProducerId();
                }

                if (!isset($options['type_search'])) {
                        $options['type_search'] = self::SEARCH_ALL;
                }

                $records = $class::find();

                // With
                if (is_array($options['with']) && count($options['with'])) {
                        $records = $records->with($options['with']);
                }

                // Join with
                if (is_array($options['join_with']) && count($options['join_with'])) {
                        $records = $records->joinWith($options['join_with']);
                }

                // Conditions
                if (isset($options['conditions'])) {
                        if (is_array($options['conditions'])) {
                                if (count($options['conditions'])) {
                                        foreach ($options['conditions'] as $condition) {
                                                $records = $records->andWhere($condition);
                                        }
                                }
                        } else {
                                if (strlen($options['conditions'])) {
                                        $records = $records->andWhere($options['conditions']);
                                }
                        }
                }

                // Params
                if (isset($options['params']) && is_array($options['params']) && count($options['params'])) {
                        $records = $records->params($options['params']);
                }

                // Paramètres
                if (is_array($params) && count($params)) {
                        foreach ($params as $key => $val) {
                                if (strpos($key, '.') === false) {
                                        unset($params[$key]);
                                        $key = $class::tableName() . '.' . $key;
                                        $params[$key] = $val;
                                }
                                $records = $records->andWhere([$key => $val]);
                        }
                }

                if (!isset($params[$pk])) {
                        // Orderby
                        if (isset($options['orderby']) && strlen($options['orderby'])) {
                                $records = $records->orderBy($options['orderby']);
                        }
                        // Limit
                        if (isset($options['limit']) && is_numeric($options['limit'])
                                && $options['limit'] > 0) {
                                $records = $records->limit($options['limit']);
                        }
                }

                if (isset($options['as_array'])) {
                        $records = $records->asArray();
                }

                if ($options['type_search'] == self::SEARCH_ALL) {
                        return $records->all();
                } elseif ($options['type_search'] == self::SEARCH_ONE) {
                        $record = $records->one();
                        if ($record) {
                                return $record;
                        }
                } elseif ($options['type_search'] == self::SEARCH_COUNT) {
                        return $records->count();
                }

                return false;
        }

        /**
         * Recherche un enregistrement.
         *
         * @param array $params
         * @param array $options
         * @return mixed
         */
        public static function searchOne($params = [], $options = [])
        {
                $options['type_search'] = self::SEARCH_ONE;
                return self::searchDispatch($params, $options);
        }

        /**
         * Recherche tous les enregistrements.
         *
         * @param array $params
         * @param array $options
         * @return mixed
         */
        public static function searchAll($params = [], $options = [])
        {
                $options['type_search'] = self::SEARCH_ALL;
                return self::searchDispatch($params, $options);
        }

        /**
         * Recherche et compte le nombre de résultats.
         *
         * @param array $params
         * @param array $options
         * @return integer
         */
        public static function searchCount($params = [], $options = [])
        {
                $options['type_search'] = self::SEARCH_COUNT;
                return self::searchDispatch($params, $options);
        }

        /**
         * Appelle la méthode 'search' de la classe appellante.
         *
         * @param array $params
         * @param array $options
         * @return mixed
         */
        public static function searchDispatch($params = [], $options = [])
        {
                $class = get_called_class();
                return $class::searchBy($params, $options);
        }

}
